from flask import url_for
from flask.ext.script import Manager, prompt_bool
from flask.ext.migrate import Migrate, MigrateCommand

from sqlalchemy_utils import database_exists, create_database, drop_database

from api import create_app
from api.config import DevConfig

app = create_app(DevConfig)
manager = Manager(app)

from api.base.db import db
migrate = Migrate(app, db)
manager.add_command('db', MigrateCommand)

@manager.command
def hello():
    print "hello"


@manager.command
def routes():
    import urllib
    output = []
    for rule in app.url_map.iter_rules():

        options = {}
        for arg in rule.arguments:
            options[arg] = "[{0}]".format(arg)

        methods = ','.join(rule.methods)
        url = url_for(rule.endpoint, **options)
        line = urllib.unquote("{:50s} {:20s} {}".format(rule.endpoint, methods, url))
        output.append(line)

    for line in sorted(output):
        print line


@manager.command
def create_db():
    if not database_exists(db.engine.url):
        print '====> Create database'
        create_database(db.engine.url)
    else:
        print '====> database exist'


@manager.command
def drop_db():
    if database_exists(db.engine.url):
        if prompt_bool("Are you sure you want to lose all your data"):
            print '====> Drop database'
            drop_database(db.engine.url)
    else:
        print '====> database not exist'


@manager.command
def create_schema():
    db.create_all()
    print '===> Create schema <==='


@manager.command
def populate_db():
    pass

if __name__ == "__main__":
    manager.run()

