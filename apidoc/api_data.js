define({ "api": [
  {
    "type": "post",
    "url": "auth/login/",
    "title": "Logging a user in",
    "name": "LoginUser",
    "group": "Auth",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>User email.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>User password.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"data\": \"Login successful\",\n  \"status\": \"OK\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 400 ERROR\n{\n  \"data\": \"Invalid Username/Password\",\n  \"status\": \"Error\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/auth/views.py",
    "groupTitle": "Auth"
  },
  {
    "type": "get",
    "url": "auth/logout/",
    "title": "Logging a user out",
    "name": "LogoutUser",
    "group": "Auth",
    "version": "1.0.0",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200\n{\n  \"data\": \"Logout successful\",\n  \"status\": \"OK\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 401\n{\n  \"data\": \"Unauthorized access\",\n  \"status\": \"ERROR\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/auth/views.py",
    "groupTitle": "Auth"
  },
  {
    "type": "post",
    "url": "certificates/",
    "title": "Creating a certificate",
    "name": "CreateCertificate",
    "group": "Certificates",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "img_file_location",
            "description": "<p>Certificate file location.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "restaurant_id",
            "description": "<p>Restaurant unique ID.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200\n{\n  \"data\": {\n        \"id\": 4,\n        \"img_file_location\": \"img_file_location\",\n        \"restaurant_id\": null\n  },\n  \"status\": \"OK\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/restaurant/certificate/views.py",
    "groupTitle": "Certificates"
  },
  {
    "type": "delete",
    "url": "certificates/:id",
    "title": "Deleting a certificate",
    "name": "DeleteSingleCertificate",
    "group": "Certificates",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Ceriticate unique ID.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200\n{\n  \"data\": \"Deleted id=2 certificate\",\n  \"status\": \"OK\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 204\n{\n  \"data\": \"There is no certificate with this id\",\n  \"status\": \"ERROR\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/restaurant/certificate/views.py",
    "groupTitle": "Certificates"
  },
  {
    "type": "get",
    "url": "certificates/",
    "title": "Getting all certificates",
    "name": "GetCertificates",
    "group": "Certificates",
    "version": "1.0.0",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200\n{\n  \"data\": [\n    {\n      \"id\": 1,\n      \"img_file_location\": \"img_file_location\",\n      \"restaurant_id\": null\n    },\n    {\n      \"id\": 2,\n      \"img_file_location\": \"img_file_location\",\n      \"restaurant_id\": null\n    }\n  \"status\": \"OK\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/restaurant/certificate/views.py",
    "groupTitle": "Certificates"
  },
  {
    "type": "get",
    "url": "certificates/:id",
    "title": "Getting a certificate",
    "name": "GetSingleCertificate",
    "group": "Certificates",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Certificate unique ID.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200\n{\n  \"data\": {\n      \"id\": 1,\n      \"img_file_location\": \"img_file_location\",\n      \"restaurant_id\": null\n    },\n  \"status\": \"OK\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 204\n{\n  \"data\": \"There is no certificate with this id\",\n  \"status\": \"ERROR\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/restaurant/certificate/views.py",
    "groupTitle": "Certificates"
  },
  {
    "type": "put",
    "url": "certificates/:id",
    "title": "Updating a certificate",
    "name": "UpdateSingleCertificate",
    "group": "Certificates",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Certiciate unique ID.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "img_file_location",
            "description": "<p>Certificate img_file_location.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "restaurant_id",
            "description": "<p>Restaurant unique ID.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200\n{\n  \"data\": {\n        \"id\": 1,\n        \"img_file_location\": \"img_file_location\",\n        \"restaurant_id\": null\n  },\n  \"status\": \"OK\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 204\n{\n  \"data\": \"There is no ceriticate with this id\",\n  \"status\": \"ERROR\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/restaurant/certificate/views.py",
    "groupTitle": "Certificates"
  },
  {
    "type": "post",
    "url": "reports/chillings/",
    "title": "Creating a chilling report",
    "name": "CreateChillingReport",
    "group": "Chilling_Reports",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "description",
            "description": "<p>Chilling Report description.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "food_name",
            "description": "<p>Chilling Report food name.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "method",
            "description": "<p>Chilling Report method.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "object_id",
            "description": "<p>Chilling Report object ID.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "object_type",
            "description": "<p>Chilling Report object type.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "temp_now",
            "description": "<p>Chilling Report current temperature.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "temp_to",
            "description": "<p>Chilling Report desired temperature.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "type",
            "description": "<p>Chilling Reprt type.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200\n{\n    \"data\":{\n        \"description\": \"text\",\n        \"food_name\": \"t3\",\n        \"id\": 1,\n        \"method\": \"t4\",\n        \"object_id\": 1,\n        \"object_type\": \"restaurant\",\n        \"temp_now\": \"t1\",\n        \"temp_to\": \"t2\",\n        \"type\": \"chilling\"\n    },\n    \"status\": \"OK\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/report/views.py",
    "groupTitle": "Chilling_Reports"
  },
  {
    "type": "delete",
    "url": "reports/chillings/:id",
    "title": "Deleting a chilling report",
    "name": "DeleteSingleChillingReport",
    "group": "Chilling_Reports",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Chilling Report unique ID.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200\n{\n  \"data\": \"Deleted id=2 chilling report\",\n  \"status\": \"OK\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 204\n{\n  \"data\": \"There is no chilling report with this id\",\n  \"status\": \"ERROR\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/report/views.py",
    "groupTitle": "Chilling_Reports"
  },
  {
    "type": "get",
    "url": "reports/chillings/",
    "title": "Getting all chilling reports",
    "name": "GetChillingReports",
    "group": "Chilling_Reports",
    "version": "1.0.0",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200\n{\n    \"data\": [\n        {\n            \"description\": \"text\",\n            \"food_name\": \"t3\",\n            \"id\": 1,\n            \"method\": \"t4\",\n            \"object_id\": 1,\n            \"object_type\": \"restaurant\",\n            \"temp_now\": \"t1\",\n            \"temp_to\": \"t2\",\n            \"type\": \"chilling\"\n        },\n        {\n            \"description\": \"text\",\n            \"food_name\": \"t3\",\n            \"id\": 2,\n            \"method\": \"t4\",\n            \"object_id\": 1,\n            \"object_type\": \"restaurant\",\n            \"temp_now\": \"t1\",\n            \"temp_to\": \"t2\",\n            \"type\": \"chilling\"\n    }\n    ],\n    \"status\": \"OK\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/report/views.py",
    "groupTitle": "Chilling_Reports"
  },
  {
    "type": "get",
    "url": "reports/chillings:id",
    "title": "Getting a chilling report",
    "name": "GetSingleChillingReport",
    "group": "Chilling_Reports",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Chilling Report unique ID.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200\n{\n  \"data\": {\n      \"description\": \"text\",\n      \"food_name\": \"t3\",\n      \"id\": 1,\n      \"method\": \"t4\",\n      \"object_id\": 1,\n      \"object_type\": \"restaurant\",\n      \"temp_now\": \"t1\",\n      \"temp_to\": \"t2\",\n      \"type\": \"chilling\"\n  },\n  \"status\": \"OK\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 204\n{\n  \"data\": \"There is no chilling report with this id\",\n  \"status\": \"ERROR\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/report/views.py",
    "groupTitle": "Chilling_Reports"
  },
  {
    "type": "put",
    "url": "reports/chillings/:id",
    "title": "Updating a chilling report",
    "name": "UpdateSingleChillingReport",
    "group": "Chilling_Reports",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Chilling Report unique ID.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "description",
            "description": "<p>Chilling Report description.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "food_name",
            "description": "<p>Chilling Report food name.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "method",
            "description": "<p>Chilling Report method.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "object_id",
            "description": "<p>Chilling Report object ID.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "object_type",
            "description": "<p>Chilling Report object type.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "temp_now",
            "description": "<p>Chilling Report current temperature.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "temp_to",
            "description": "<p>Chilling Report desired temperature.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "type",
            "description": "<p>Chilling Reprt type.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200\n{\n  \"data\": {\n      \"description\": \"text\",\n      \"food_name\": \"t3\",\n      \"id\": 1,\n      \"method\": \"t4\",\n      \"object_id\": 1,\n      \"object_type\": \"restaurant\",\n      \"temp_now\": \"t1\",\n      \"temp_to\": \"t2\",\n      \"type\": \"chilling\"\n  },\n  \"status\": \"OK\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 204\n{\n  \"data\": \"There is no chilling report with this id\",\n  \"status\": \"ERROR\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/report/views.py",
    "groupTitle": "Chilling_Reports"
  },
  {
    "type": "post",
    "url": "reports/cleanings/",
    "title": "Creating a cleaning report",
    "name": "CreateCleaningReport",
    "group": "Cleaning_Reports",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "description",
            "description": "<p>Cleaning Report description.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "field",
            "description": "<p>Cleaning Report field.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "object_id",
            "description": "<p>Cleaning Report object ID.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "object_type",
            "description": "<p>Cleaning Report object type.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "periodic_time",
            "description": "<p>Cleaning Report schedule time.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "type",
            "description": "<p>Cleaning Report type.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200\n{\n    \"data\": {\n          \"description\": \"text\",\n          \"field\": \"t1\",\n          \"id\": 3,\n          \"object_id\": 1,\n          \"object_type\": \"restaurant\",\n          \"periodic_time\": \"t2\",\n          \"type\": \"cleaning\"\n    },\n    \"status\": \"OK\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/report/views.py",
    "groupTitle": "Cleaning_Reports"
  },
  {
    "type": "delete",
    "url": "reports/cleanings/:id",
    "title": "Deleting a cleaning report",
    "name": "DeleteSingleCleaningReport",
    "group": "Cleaning_Reports",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Cleaning Report unique ID.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200\n{\n  \"data\": \"Deleted id=2 cleaning report\",\n  \"status\": \"OK\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 204\n{\n  \"data\": \"There is no cleaning report with this id\",\n  \"status\": \"ERROR\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/report/views.py",
    "groupTitle": "Cleaning_Reports"
  },
  {
    "type": "get",
    "url": "reports/cleanings/",
    "title": "Getting all cleaning reports",
    "name": "GetCleaningReports",
    "group": "Cleaning_Reports",
    "version": "1.0.0",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200\n{\n    \"data\": [\n        {\n          \"description\": \"text\",\n          \"field\": \"t1\",\n          \"id\": 3,\n          \"object_id\": 1,\n          \"object_type\": \"restaurant\",\n          \"periodic_time\": \"t2\",\n          \"type\": \"cleaning\"\n        },\n        {\n          \"description\": \"text\",\n          \"field\": \"t1\",\n          \"id\": 4,\n          \"object_id\": 1,\n          \"object_type\": \"restaurant\",\n          \"periodic_time\": \"t2\",\n          \"type\": \"cleaning\"\n        }\n    ],\n    \"status\": \"OK\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/report/views.py",
    "groupTitle": "Cleaning_Reports"
  },
  {
    "type": "get",
    "url": "reports/cleaning/:id",
    "title": "Getting a cleaning report",
    "name": "GetSingleCleaningReport",
    "group": "Cleaning_Reports",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Cleaning Report unique ID.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200\n{\n  \"data\": {\n      \"description\": \"text\",\n      \"field\": \"t1\",\n      \"id\": 3,\n      \"object_id\": 1,\n      \"object_type\": \"restaurant\",\n      \"periodic_time\": \"t2\",\n      \"type\": \"cleaning\"\n  },\n  \"status\": \"OK\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 204\n{\n  \"data\": \"There is no cleaning report with this id\",\n  \"status\": \"ERROR\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/report/views.py",
    "groupTitle": "Cleaning_Reports"
  },
  {
    "type": "put",
    "url": "foods/:id",
    "title": "Updating a cleaning report",
    "name": "UpdateSingleCleaningReport",
    "group": "Cleaning_Reports",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Cleaning Report unique ID.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "description",
            "description": "<p>Cleaning Report description.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "field",
            "description": "<p>Cleaning Report field.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "object_id",
            "description": "<p>Cleaning Report object ID.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "object_type",
            "description": "<p>Cleaning Report object type.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "periodic_time",
            "description": "<p>Cleaning Report schedule time.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "type",
            "description": "<p>Cleaning Report type.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200\n{\n  \"data\": {\n      \"description\": \"text\",\n      \"field\": \"t1\",\n      \"id\": 3,\n      \"object_id\": 1,\n      \"object_type\": \"restaurant\",\n      \"periodic_time\": \"t2\",\n      \"type\": \"cleaning\"\n  },\n  \"status\": \"OK\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 204\n{\n  \"data\": \"There is no cleaning report with this id\",\n  \"status\": \"ERROR\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/report/views.py",
    "groupTitle": "Cleaning_Reports"
  },
  {
    "type": "post",
    "url": "reports/dailies/",
    "title": "Creating a daily report",
    "name": "CreateDailyReport",
    "group": "Daily_Reports",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "description",
            "description": "<p>Daily Report description.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "temperature",
            "description": "<p>Daily Report temperature.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "object_type",
            "description": "<p>Daily Report object type.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "object_id",
            "description": "<p>Daily Report object ID.</p>"
          },
          {
            "group": "Parameter",
            "type": "Boolean",
            "optional": false,
            "field": "leftout",
            "description": "<p>Daily Report if food left-out.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "leftout_desc",
            "description": "<p>Daily Report left-out description.</p>"
          },
          {
            "group": "Parameter",
            "type": "Boolean",
            "optional": false,
            "field": "expired",
            "description": "<p>Daily Report if food expired.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "expired_desc",
            "description": "<p>Daily Report expiration description.</p>"
          },
          {
            "group": "Parameter",
            "type": "Boolean",
            "optional": false,
            "field": "dirtyclothes",
            "description": "<p>Daily Report if clothes dirty.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "dirtyclothes_desc",
            "description": "<p>Daily Report if dirt clothes description.</p>"
          },
          {
            "group": "Parameter",
            "type": "Boolean",
            "optional": false,
            "field": "waste",
            "description": "<p>Daily Report if waste was attended.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "waste_desc",
            "description": "<p>Daily Report waste description.</p>"
          },
          {
            "group": "Parameter",
            "type": "Boolean",
            "optional": false,
            "field": "floor",
            "description": "<p>Daily Report if floor was clean.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "floor_desc",
            "description": "<p>Daily Report flood description.</p>"
          },
          {
            "group": "Parameter",
            "type": "Boolean",
            "optional": false,
            "field": "board",
            "description": "<p>Daily Report if board was attended.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "board_desc",
            "description": "<p>Daily Report board description.</p>"
          },
          {
            "group": "Parameter",
            "type": "Boolean",
            "optional": false,
            "field": "utensile",
            "description": "<p>Daily Report if utensils were clean.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "utensile_desc",
            "description": "<p>Daily Report utensils desciption.</p>"
          },
          {
            "group": "Parameter",
            "type": "Boolean",
            "optional": false,
            "field": "equipment",
            "description": "<p>Daily Report if equipments were attended.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "equipment_desc",
            "description": "<p>Daily Report equipment description.</p>"
          },
          {
            "group": "Parameter",
            "type": "Boolean",
            "optional": false,
            "field": "storage",
            "description": "<p>Daily Report if storage was attended.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "storage_desc",
            "description": "<p>Daily Report storage description.</p>"
          },
          {
            "group": "Parameter",
            "type": "Boolean",
            "optional": false,
            "field": "food_prep",
            "description": "<p>Daily Report if food was prepared.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "food_prep_desc",
            "description": "<p>Daily Report food preparation description.</p>"
          },
          {
            "group": "Parameter",
            "type": "Boolean",
            "optional": false,
            "field": "matt_ok",
            "description": "<p>Daily Report if materials were OK.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "matt_ok_desc",
            "description": "<p>Daily Report material description.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200\n{\n    \"data\": {\n    \"id\": 1,\n    \"description\": \"text\",\n    \"temperature\": 10,\n    \"object_type\": \"restaurant\",\n    \"object_id\": 1,\n    \"leftout\": true,\n    \"leftout_desc\": \"leftout_desc\",\n    \"expired\": true,\n    \"expired_desc\": \"expired_desc\",\n    \"dirtyclothes\": true,\n    \"dirtyclothes_desc\": \"dirtyclothes_desc\",\n    \"waste\": true,\n    \"waste_desc\": \"waste_desc\",\n    \"floor\": true,\n    \"floor_desc\": \"floor_desc\",\n    \"board\": true,\n    \"board_desc\": \"board_desc\",\n    \"utensile\": true,\n    \"utensile_desc\": \"utensile_desc\",\n    \"equipment\": true,\n    \"equipment_desc\": \"equipment_desc\",\n    \"storage\": true,\n    \"storage_desc\": \"storage_desc\",\n    \"food_prep\": true,\n    \"food_prep_desc\": \"food_prep_desc\",\n    \"matt_ok\": true,\n    \"matt_ok_desc\": \"matt_ok_desc\"\n    },\n    \"status\": \"OK\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/report/views.py",
    "groupTitle": "Daily_Reports"
  },
  {
    "type": "delete",
    "url": "report/dailies/:id",
    "title": "Deleting a daily report",
    "name": "DeleteSingleDailyReport",
    "group": "Daily_Reports",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Daily Report unique ID.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200\n{\n  \"data\": \"Deleted id=2 daily report\",\n  \"status\": \"OK\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 204\n{\n  \"data\": \"There is no daily report with this id\",\n  \"status\": \"ERROR\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/report/views.py",
    "groupTitle": "Daily_Reports"
  },
  {
    "type": "get",
    "url": "reports/dailies/",
    "title": "Getting all daily reports",
    "name": "GetDailyReports",
    "group": "Daily_Reports",
    "version": "1.0.0",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200\n{\n    \"data\": [\n        {\n        \"id\": 1,\n        \"description\": \"text\",\n        \"temperature\": 10,\n        \"object_type\": \"restaurant\",\n        \"object_id\": 1,\n        \"leftout\": true,\n        \"leftout_desc\": \"leftout_desc\",\n        \"expired\": true,\n        \"expired_desc\": \"expired_desc\",\n        \"dirtyclothes\": true,\n        \"dirtyclothes_desc\": \"dirtyclothes_desc\",\n        \"waste\": true,\n        \"waste_desc\": \"waste_desc\",\n        \"floor\": true,\n        \"floor_desc\": \"floor_desc\",\n        \"board\": true,\n        \"board_desc\": \"board_desc\",\n        \"utensile\": true,\n        \"utensile_desc\": \"utensile_desc\",\n        \"equipment\": true,\n        \"equipment_desc\": \"equipment_desc\",\n        \"storage\": true,\n        \"storage_desc\": \"storage_desc\",\n        \"food_prep\": true,\n        \"food_prep_desc\": \"food_prep_desc\",\n        \"matt_ok\": true,\n        \"matt_ok_desc\": \"matt_ok_desc\"\n      },{\n        \"id\": 2,\n        \"description\": \"text\",\n        \"temperature\": 10,\n        \"object_type\": \"restaurant\",\n        \"object_id\": 1,\n        \"leftout\": true,\n        \"leftout_desc\": \"leftout_desc\",\n        \"expired\": true,\n        \"expired_desc\": \"expired_desc\",\n        \"dirtyclothes\": true,\n        \"dirtyclothes_desc\": \"dirtyclothes_desc\",\n        \"waste\": true,\n        \"waste_desc\": \"waste_desc\",\n        \"floor\": true,\n        \"floor_desc\": \"floor_desc\",\n        \"board\": true,\n        \"board_desc\": \"board_desc\",\n        \"utensile\": true,\n        \"utensile_desc\": \"utensile_desc\",\n        \"equipment\": true,\n        \"equipment_desc\": \"equipment_desc\",\n        \"storage\": true,\n        \"storage_desc\": \"storage_desc\",\n        \"food_prep\": true,\n        \"food_prep_desc\": \"food_prep_desc\",\n        \"matt_ok\": true,\n        \"matt_ok_desc\": \"matt_ok_desc\"\n      }\n    ],\n    \"status\": \"OK\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/report/views.py",
    "groupTitle": "Daily_Reports"
  },
  {
    "type": "get",
    "url": "reports/dailies/:id",
    "title": "Getting a daily report",
    "name": "GetSingleDailyReport",
    "group": "Daily_Reports",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Daily Report unique ID.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200\n{\n  \"data\": {\n    \"id\": 1,\n    \"description\": \"text\",\n    \"temperature\": 10,\n    \"object_type\": \"restaurant\",\n    \"object_id\": 1,\n    \"leftout\": true,\n    \"leftout_desc\": \"leftout_desc\",\n    \"expired\": true,\n    \"expired_desc\": \"expired_desc\",\n    \"dirtyclothes\": true,\n    \"dirtyclothes_desc\": \"dirtyclothes_desc\",\n    \"waste\": true,\n    \"waste_desc\": \"waste_desc\",\n    \"floor\": true,\n    \"floor_desc\": \"floor_desc\",\n    \"board\": true,\n    \"board_desc\": \"board_desc\",\n    \"utensile\": true,\n    \"utensile_desc\": \"utensile_desc\",\n    \"equipment\": true,\n    \"equipment_desc\": \"equipment_desc\",\n    \"storage\": true,\n    \"storage_desc\": \"storage_desc\",\n    \"food_prep\": true,\n    \"food_prep_desc\": \"food_prep_desc\",\n    \"matt_ok\": true,\n    \"matt_ok_desc\": \"matt_ok_desc\"\n  },\n  \"status\": \"OK\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 204\n{\n  \"data\": \"There is no daily report with this id\",\n  \"status\": \"ERROR\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/report/views.py",
    "groupTitle": "Daily_Reports"
  },
  {
    "type": "put",
    "url": "reports/dailies/:id",
    "title": "Updating a daily report",
    "name": "UpdateSingleDailyReport",
    "group": "Daily_Reports",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Daily Report unique ID.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "description",
            "description": "<p>Daily Report description.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "temperature",
            "description": "<p>Daily Report temperature.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "object_type",
            "description": "<p>Daily Report object type.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "object_id",
            "description": "<p>Daily Report object ID.</p>"
          },
          {
            "group": "Parameter",
            "type": "Boolean",
            "optional": false,
            "field": "leftout",
            "description": "<p>Daily Report if food left-out.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "leftout_desc",
            "description": "<p>Daily Report left-out description.</p>"
          },
          {
            "group": "Parameter",
            "type": "Boolean",
            "optional": false,
            "field": "expired",
            "description": "<p>Daily Report if food expired.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "expired_desc",
            "description": "<p>Daily Report expiration description.</p>"
          },
          {
            "group": "Parameter",
            "type": "Boolean",
            "optional": false,
            "field": "dirtyclothes",
            "description": "<p>Daily Report if clothes dirty.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "dirtyclothes_desc",
            "description": "<p>Daily Report if dirt clothes description.</p>"
          },
          {
            "group": "Parameter",
            "type": "Boolean",
            "optional": false,
            "field": "waste",
            "description": "<p>Daily Report if waste was attended.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "waste_desc",
            "description": "<p>Daily Report waste description.</p>"
          },
          {
            "group": "Parameter",
            "type": "Boolean",
            "optional": false,
            "field": "floor",
            "description": "<p>Daily Report if floor was clean.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "floor_desc",
            "description": "<p>Daily Report flood description.</p>"
          },
          {
            "group": "Parameter",
            "type": "Boolean",
            "optional": false,
            "field": "board",
            "description": "<p>Daily Report if board was attended.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "board_desc",
            "description": "<p>Daily Report board description.</p>"
          },
          {
            "group": "Parameter",
            "type": "Boolean",
            "optional": false,
            "field": "utensile",
            "description": "<p>Daily Report if utensils were clean.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "utensile_desc",
            "description": "<p>Daily Report utensils desciption.</p>"
          },
          {
            "group": "Parameter",
            "type": "Boolean",
            "optional": false,
            "field": "equipment",
            "description": "<p>Daily Report if equipments were attended.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "equipment_desc",
            "description": "<p>Daily Report equipment description.</p>"
          },
          {
            "group": "Parameter",
            "type": "Boolean",
            "optional": false,
            "field": "storage",
            "description": "<p>Daily Report if storage was attended.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "storage_desc",
            "description": "<p>Daily Report storage description.</p>"
          },
          {
            "group": "Parameter",
            "type": "Boolean",
            "optional": false,
            "field": "food_prep",
            "description": "<p>Daily Report if food was prepared.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "food_prep_desc",
            "description": "<p>Daily Report food preparation description.</p>"
          },
          {
            "group": "Parameter",
            "type": "Boolean",
            "optional": false,
            "field": "matt_ok",
            "description": "<p>Daily Report if materials were OK.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "matt_ok_desc",
            "description": "<p>Daily Report material description.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200\n{\n  \"data\": {\n    \"id\": 1,\n    \"description\": \"text\",\n    \"temperature\": 10,\n    \"object_type\": \"restaurant\",\n    \"object_id\": 1,\n    \"leftout\": true,\n    \"leftout_desc\": \"leftout_desc\",\n    \"expired\": true,\n    \"expired_desc\": \"expired_desc\",\n    \"dirtyclothes\": true,\n    \"dirtyclothes_desc\": \"dirtyclothes_desc\",\n    \"waste\": true,\n    \"waste_desc\": \"waste_desc\",\n    \"floor\": true,\n    \"floor_desc\": \"floor_desc\",\n    \"board\": true,\n    \"board_desc\": \"board_desc\",\n    \"utensile\": true,\n    \"utensile_desc\": \"utensile_desc\",\n    \"equipment\": true,\n    \"equipment_desc\": \"equipment_desc\",\n    \"storage\": true,\n    \"storage_desc\": \"storage_desc\",\n    \"food_prep\": true,\n    \"food_prep_desc\": \"food_prep_desc\",\n    \"matt_ok\": true,\n    \"matt_ok_desc\": \"matt_ok_desc\"\n  },\n  \"status\": \"OK\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 204\n{\n  \"data\": \"There is no daily report with this id\",\n  \"status\": \"ERROR\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/report/views.py",
    "groupTitle": "Daily_Reports"
  },
  {
    "type": "post",
    "url": "reports/defrostings",
    "title": "Creating a defrosting report",
    "name": "CreateDefrostingReport",
    "group": "Defrosting_Reports",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "description",
            "description": "<p>Defrosting Report description.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "food_name",
            "description": "<p>Defrosting Report food name.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "method",
            "description": "<p>Defrosting method.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "object_id",
            "description": "<p>Defrosting Report object ID.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "object_type",
            "description": "<p>Defrosting Report object type.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "temp_to",
            "description": "<p>Defrosting Report desired temperature.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "type",
            "description": "<p>Defrosting Report type.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200\n{\n    \"data\": {\n        \"description\": \"text\",\n        \"food_name\": \"t2\",\n        \"id\": 14,\n        \"method\": \"t3\",\n        \"object_id\": 1,\n        \"object_type\": \"restaurant\",\n        \"temp_to\": \"t1\",\n        \"type\": \"defrosting\"\n    },\n    \"status\": \"OK\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/report/views.py",
    "groupTitle": "Defrosting_Reports"
  },
  {
    "type": "delete",
    "url": "reports/defrostings/:id",
    "title": "Deleting a defrosting report",
    "name": "DeleteSingleDefrostingReport",
    "group": "Defrosting_Reports",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Defrosting Report unique ID.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200\n{\n  \"data\": \"Deleted id=2 defrosting report\",\n  \"status\": \"OK\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 204\n{\n  \"data\": \"There is no defrosting report with this id\",\n  \"status\": \"ERROR\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/report/views.py",
    "groupTitle": "Defrosting_Reports"
  },
  {
    "type": "get",
    "url": "reports/defrostings/",
    "title": "Getting all defrosting reports",
    "name": "GetDefrostingReports",
    "group": "Defrosting_Reports",
    "version": "1.0.0",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200\n{\n    \"data\": [\n        {\n              \"description\": \"text\",\n              \"food_name\": \"t2\",\n              \"id\": 14,\n              \"method\": \"t3\",\n              \"object_id\": 1,\n              \"object_type\": \"restaurant\",\n              \"temp_to\": \"t1\",\n              \"type\": \"defrosting\"\n          },\n                {\n              \"description\": \"text\",\n              \"food_name\": \"t2\",\n              \"id\": 15,\n              \"method\": \"t3\",\n              \"object_id\": 1,\n              \"object_type\": \"restaurant\",\n              \"temp_to\": \"t1\",\n              \"type\": \"defrosting\"\n          }\n    ],\n    \"status\": \"OK\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/report/views.py",
    "groupTitle": "Defrosting_Reports"
  },
  {
    "type": "get",
    "url": "reports/defrostings/:id",
    "title": "Getting a defrosting report",
    "name": "GetSingleDefrostingReport",
    "group": "Defrosting_Reports",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Defrosting Report unique ID.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200\n{\n  \"data\": {\n      \"description\": \"text\",\n      \"food_name\": \"t2\",\n      \"id\": 14,\n      \"method\": \"t3\",\n      \"object_id\": 1,\n      \"object_type\": \"restaurant\",\n      \"temp_to\": \"t1\",\n      \"type\": \"defrosting\"\n  },\n  \"status\": \"OK\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 204\n{\n  \"data\": \"There is no defrosting report with this id\",\n  \"status\": \"ERROR\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/report/views.py",
    "groupTitle": "Defrosting_Reports"
  },
  {
    "type": "put",
    "url": "reports/defrostings/:id",
    "title": "Updating a defrosting report",
    "name": "UpdateSingleDefrostingReport",
    "group": "Defrosting_Reports",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Defrosting Report unique ID.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "description",
            "description": "<p>Defrosting Report description.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "food_name",
            "description": "<p>Defrosting Report food name.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "method",
            "description": "<p>Defrosting method.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "object_id",
            "description": "<p>Defrosting Report object ID.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "object_type",
            "description": "<p>Defrosting Report object type.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "temp_to",
            "description": "<p>Defrosting Report desired temperature.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "type",
            "description": "<p>Defrosting Report type.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200\n{\n  \"data\": {\n      \"description\": \"text\",\n      \"food_name\": \"t2\",\n      \"id\": 14,\n      \"method\": \"t3\",\n      \"object_id\": 1,\n      \"object_type\": \"restaurant\",\n      \"temp_to\": \"t1\",\n      \"type\": \"defrosting\"\n  },\n  \"status\": \"OK\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 204\n{\n  \"data\": \"There is no defrosting report with this id\",\n  \"status\": \"ERROR\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/report/views.py",
    "groupTitle": "Defrosting_Reports"
  },
  {
    "type": "post",
    "url": "foods/",
    "title": "Creating a food",
    "name": "CreateFood",
    "group": "Foods",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Food name.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "restaurant_id",
            "description": "<p>Restaurant unique ID.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200\n{\n    \"data\": {\n        \"id\": 7,\n        \"name\": \"food_name\",\n        \"restaurant_id\": 1\n      },\n    \"status\": \"OK\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/food/views.py",
    "groupTitle": "Foods"
  },
  {
    "type": "delete",
    "url": "foods/:id",
    "title": "Deleting a food",
    "name": "DeleteSingleFood",
    "group": "Foods",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Food unique ID.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200\n{\n  \"data\": \"Deleted id=2 food\",\n  \"status\": \"OK\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 204\n{\n  \"data\": \"There is no food with this id\",\n  \"status\": \"ERROR\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/food/views.py",
    "groupTitle": "Foods"
  },
  {
    "type": "get",
    "url": "foods/",
    "title": "Getting all foods",
    "name": "GetFoods",
    "group": "Foods",
    "version": "1.0.0",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200\n{\n    \"data\": [\n        {\n          \"id\": 2,\n          \"name\": \"food_name\",\n          \"restaurant_id\": 1\n        },\n        {\n          \"id\": 3,\n          \"name\": \"food_name\",\n          \"restaurant_id\": 1\n        }\n    ],\n    \"status\": \"OK\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/food/views.py",
    "groupTitle": "Foods"
  },
  {
    "type": "get",
    "url": "foods/:id",
    "title": "Getting a food",
    "name": "GetSingleFood",
    "group": "Foods",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Food unique ID.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200\n{\n  \"data\": {\n    \"id\": 2,\n    \"name\": \"food_name\",\n    \"restaurant_id\": 1\n  },\n  \"status\": \"OK\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 204\n{\n  \"data\": \"There is no food with this id\",\n  \"status\": \"ERROR\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/food/views.py",
    "groupTitle": "Foods"
  },
  {
    "type": "put",
    "url": "foods/:id",
    "title": "Updating a food",
    "name": "UpdateSingleFood",
    "group": "Foods",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Food unique ID.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Food name.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "restaurant_id",
            "description": "<p>Restaurant unique ID.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200\n{\n  \"data\": {\n    \"id\": 2,\n    \"name\": \"food_name\",\n    \"restaurant_id\": 1\n  },\n  \"status\": \"OK\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 204\n{\n  \"data\": \"There is no food with this id\",\n  \"status\": \"ERROR\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/food/views.py",
    "groupTitle": "Foods"
  },
  {
    "type": "post",
    "url": "reports/heatings/",
    "title": "Creating a heating report",
    "name": "CreateHeaintReport",
    "group": "Heating_Reports",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "description",
            "description": "<p>Heating Report description.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "temperature",
            "description": "<p>Heating Report temperature.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "object_type",
            "description": "<p>Heating Report object type.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "object_id",
            "description": "<p>Heating Report object ID.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "temp_now",
            "description": "<p>Heating Report current temperature.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "temp_to",
            "description": "<p>Heating Report desired temperature.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "food_name",
            "description": "<p>Heating Report food name.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200\n{\n    \"data\": {\n        \"id\": 1,\n        \"description\": \"text\",\n        \"temperature\": 10,\n        \"object_type\": \"restaurant\",\n        \"object_id\": 1,\n        \"temp_now\": \"t1\",\n        \"temp_to\": \"t2\",\n        \"food_name\": \"t3\"\n    },\n    \"status\": \"OK\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/report/views.py",
    "groupTitle": "Heating_Reports"
  },
  {
    "type": "delete",
    "url": "reports/heatings/:id",
    "title": "Deleting a heating report",
    "name": "DeleteSingleHeatingReport",
    "group": "Heating_Reports",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Heating Report unique ID.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200\n{\n  \"data\": \"Deleted id=2 heating report\",\n  \"status\": \"OK\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 204\n{\n  \"data\": \"There is no heating report with this id\",\n  \"status\": \"ERROR\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/report/views.py",
    "groupTitle": "Heating_Reports"
  },
  {
    "type": "get",
    "url": "reports/heatings/",
    "title": "Getting all heating reports",
    "name": "GetHeatingReports",
    "group": "Heating_Reports",
    "version": "1.0.0",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200\n{\n    \"data\": [\n        {\n        \"id\": 1,\n        \"description\": \"text\",\n        \"temperature\": 10,\n        \"object_type\": \"restaurant\",\n        \"object_id\": 1,\n        \"temp_now\": \"t1\",\n        \"temp_to\": \"t2\",\n        \"food_name\": \"t3\"\n        },{\n        \"id\": 2,\n        \"description\": \"text\",\n        \"temperature\": 10,\n        \"object_type\": \"restaurant\",\n        \"object_id\": 1,\n        \"temp_now\": \"t1\",\n        \"temp_to\": \"t2\",\n        \"food_name\": \"t3\"\n        }\n    ],\n    \"status\": \"OK\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/report/views.py",
    "groupTitle": "Heating_Reports"
  },
  {
    "type": "get",
    "url": "reports/heatings/:id",
    "title": "Getting a heating report",
    "name": "GetSingleHeatingReport",
    "group": "Heating_Reports",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Heating Report unique ID.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200\n{\n  \"data\": {\n        \"id\": 1,\n        \"description\": \"text\",\n        \"temperature\": 10,\n        \"object_type\": \"restaurant\",\n        \"object_id\": 1,\n        \"temp_now\": \"t1\",\n        \"temp_to\": \"t2\",\n        \"food_name\": \"t3\"\n  },\n  \"status\": \"OK\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 204\n{\n  \"data\": \"There is no heating report with this id\",\n  \"status\": \"ERROR\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/report/views.py",
    "groupTitle": "Heating_Reports"
  },
  {
    "type": "put",
    "url": "reports/heatings/:id",
    "title": "Updating a heating report",
    "name": "UpdateSingleHeatingReport",
    "group": "Heating_Reports",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Heating Report unique ID.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "description",
            "description": "<p>Heating Report description.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "temperature",
            "description": "<p>Heating Report temperature.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "object_type",
            "description": "<p>Heating Report object type.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "object_id",
            "description": "<p>Heating Report object ID.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "temp_now",
            "description": "<p>Heating Report current temperature.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "temp_to",
            "description": "<p>Heating Report desired temperature.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "food_name",
            "description": "<p>Heating Report food name.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200\n{\n  \"data\": {\n        \"id\": 1,\n        \"description\": \"text\",\n        \"temperature\": 10,\n        \"object_type\": \"restaurant\",\n        \"object_id\": 1,\n        \"temp_now\": \"t1\",\n        \"temp_to\": \"t2\",\n        \"food_name\": \"t3\"\n  },\n  \"status\": \"OK\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 204\n{\n  \"data\": \"There is no heating report with this id\",\n  \"status\": \"ERROR\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/report/views.py",
    "groupTitle": "Heating_Reports"
  },
  {
    "type": "post",
    "url": "instruments/",
    "title": "Creating an instrument",
    "name": "CreateInstrument",
    "group": "Instruments",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Instrument name.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "restaurant_id",
            "description": "<p>Restaurant unique ID.</p>"
          },
          {
            "group": "Parameter",
            "type": "Boolean",
            "optional": false,
            "field": "is_working",
            "defaultValue": "True",
            "description": "<p>If instrument is working.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "barcode_id",
            "defaultValue": "0",
            "description": "<p>Barcode of the instrument.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Example:",
          "content": "HTTP/1.1 200\n{\n  \"data\": {\n    \"barcode_id\": 12,\n    \"id\": 4,\n    \"is_working\": true,\n    \"name\": \"food_name\",\n    \"restaurant_id\": 1\n  },\n  \"status\": \"OK\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/instrument/views.py",
    "groupTitle": "Instruments"
  },
  {
    "type": "delete",
    "url": "foods/:id",
    "title": "Deleting an instrument",
    "name": "DeleteSingleInstrument",
    "group": "Instruments",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Instrument unique ID.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200\n{\n  \"data\": \"Deleted id=2 instrument\",\n  \"status\": \"OK\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 204\n{\n  \"data\": \"There is no instrument with this id\",\n  \"status\": \"ERROR\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/instrument/views.py",
    "groupTitle": "Instruments"
  },
  {
    "type": "get",
    "url": "instruments/",
    "title": "Getting all instruments",
    "name": "GetInstrument",
    "group": "Instruments",
    "version": "1.0.0",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200\n{\n  \"data\": [\n    {\n      \"barcode_id\": 12,\n      \"id\": 1,\n      \"is_working\": true,\n      \"name\": \"food_name\",\n      \"restaurant_id\": 1\n    },\n    {\n      \"barcode_id\": 12,\n      \"id\": 2,\n      \"is_working\": true,\n      \"name\": \"food_name\",\n      \"restaurant_id\": 1\n    }\n  ],\n  \"status\": \"OK\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/instrument/views.py",
    "groupTitle": "Instruments"
  },
  {
    "type": "get",
    "url": "instruments/:id",
    "title": "Getting an instrument",
    "name": "GetSingleInstrument",
    "group": "Instruments",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Instrument unique ID.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200\n{\n  \"data\": {\n    \"barcode_id\": 12,\n    \"id\": 1,\n    \"is_working\": true,\n    \"name\": \"food_name\",\n    \"restaurant_id\": 1\n  },\n  \"status\": \"OK\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 204\n{\n  \"data\": \"There is no instrument with this id\",\n  \"status\": \"ERROR\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/instrument/views.py",
    "groupTitle": "Instruments"
  },
  {
    "type": "put",
    "url": "instruments/:id",
    "title": "Updating an instrument",
    "name": "UpdateSingleInstrument",
    "group": "Instruments",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Instrument unique ID.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Instrument name.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "restaurant_id",
            "description": "<p>Restaurant unique ID.</p>"
          },
          {
            "group": "Parameter",
            "type": "Boolean",
            "optional": false,
            "field": "is_working",
            "defaultValue": "True",
            "description": "<p>If instrument is working.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "barcode_id",
            "defaultValue": "0",
            "description": "<p>Barcode of the instrument.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200\n{\n  \"data\": {\n    \"barcode_id\": 12,\n    \"id\": 7,\n    \"is_working\": true,\n    \"name\": \"food_name\",\n    \"restaurant_id\": 1\n  },\n  \"status\": \"OK\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 204\n{\n  \"data\": \"There is no instrument with this id\",\n  \"status\": \"ERROR\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/instrument/views.py",
    "groupTitle": "Instruments"
  },
  {
    "type": "post",
    "url": "invoices/",
    "title": "Creating an invoice",
    "name": "CreateInvoice",
    "group": "Invoices",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "supplier_id",
            "description": "<p>Supplier unique ID.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200\n{\n    \"data\": {\n        \"id\": 7,\n        \"supplier_id\": 1\n      },\n    \"status\": \"OK\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/supplier/invoice/views.py",
    "groupTitle": "Invoices"
  },
  {
    "type": "delete",
    "url": "incoices/:id",
    "title": "Deleting an invoice",
    "name": "DeleteSingleInvoice",
    "group": "Invoices",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Invoice unique ID.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200\n{\n  \"data\": \"Deleted id=2 invoice\",\n  \"status\": \"OK\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 204\n{\n  \"data\": \"There is no invoice with this id\",\n  \"status\": \"ERROR\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/supplier/invoice/views.py",
    "groupTitle": "Invoices"
  },
  {
    "type": "get",
    "url": "invoices/",
    "title": "Getting all invoices",
    "name": "GetInvoices",
    "group": "Invoices",
    "version": "1.0.0",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200\n{\n    \"data\": [\n        {\n          \"id\": 2,\n          \"supplier_id\": 1\n        },\n        {\n          \"id\": 3,\n          \"supplier_id\": 1\n        }\n    ],\n    \"status\": \"OK\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/supplier/invoice/views.py",
    "groupTitle": "Invoices"
  },
  {
    "type": "get",
    "url": "invoices/:id",
    "title": "Getting an invoice",
    "name": "GetSingleInvoice",
    "group": "Invoices",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Invoice unique ID.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200\n{\n  \"data\": {\n    \"id\": 2,\n    \"supplier_id\": 1\n  },\n  \"status\": \"OK\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 204\n{\n  \"data\": \"There is no invoice with this id\",\n  \"status\": \"ERROR\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/supplier/invoice/views.py",
    "groupTitle": "Invoices"
  },
  {
    "type": "put",
    "url": "invoices/:id",
    "title": "Updating an invoice",
    "name": "UpdateSingleInvoice",
    "group": "Invoices",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Invoice unique ID.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "supplier_id",
            "description": "<p>Supplier unique ID.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200\n{\n  \"data\": {\n    \"id\": 2,\n    \"supplier_id\": 1\n  },\n  \"status\": \"OK\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 204\n{\n  \"data\": \"There is no invoice with this id\",\n  \"status\": \"ERROR\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/supplier/invoice/views.py",
    "groupTitle": "Invoices"
  },
  {
    "type": "post",
    "url": "materials/",
    "title": "Creating a material",
    "name": "CreateMaterial",
    "group": "Materials",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Material name.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "restaurant_id",
            "description": "<p>Restaurant unique ID.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Example:",
          "content": "HTTP/1.1 200\n{\n  \"data\": {\n    \"id\": 4,\n    \"name\": \"material_name\",\n    \"restaurant_id\": 1\n  },\n  \"status\": \"OK\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/material/views.py",
    "groupTitle": "Materials"
  },
  {
    "type": "delete",
    "url": "materials/:id",
    "title": "Deleting a material",
    "name": "DeleteSingleMaterial",
    "group": "Materials",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Material unique ID.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200\n{\n  \"data\": \"Deleted id=2 material\",\n  \"status\": \"OK\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 204\n{\n  \"data\": \"There is no material with this id\",\n  \"status\": \"ERROR\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/material/views.py",
    "groupTitle": "Materials"
  },
  {
    "type": "get",
    "url": "materials/",
    "title": "Getting all materials",
    "name": "GetMaterial",
    "group": "Materials",
    "version": "1.0.0",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200\n{\n  \"data\": [\n    {\n      \"id\": 1,\n      \"name\": \"material_name\",\n      \"restaurant_id\": 1\n    },\n    {\n      \"id\": 2,\n      \"name\": \"material_name\",\n      \"restaurant_id\": 1\n    }\n  ],\n  \"status\": \"OK\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/material/views.py",
    "groupTitle": "Materials"
  },
  {
    "type": "get",
    "url": "materials/:id",
    "title": "Getting a material",
    "name": "GetSingleMaterial",
    "group": "Materials",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Material unique ID.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200\n{\n  \"data\": {\n    \"id\": 1,\n    \"name\": \"material_name\",\n    \"restaurant_id\": 1\n  },\n  \"status\": \"OK\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 204\n{\n  \"data\": \"There is no material with this id\",\n  \"status\": \"ERROR\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/material/views.py",
    "groupTitle": "Materials"
  },
  {
    "type": "put",
    "url": "materials/:id",
    "title": "Updating a material",
    "name": "UpdateSingleMaterial",
    "group": "Materials",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Material unique ID.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Material name.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "restaurant_id",
            "description": "<p>Restaurant unique ID.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200\n{\n  \"data\": {\n    \"id\": 2,\n    \"name\": \"material_name\",\n    \"restaurant_id\": 1\n  },\n  \"status\": \"OK\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 204\n{\n  \"data\": \"There is no material with this id\",\n  \"status\": \"ERROR\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/material/views.py",
    "groupTitle": "Materials"
  },
  {
    "type": "post",
    "url": "notifications/",
    "title": "Creating a notification",
    "name": "CreateNotification",
    "group": "Notifications",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Notification message.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "user_id",
            "description": "<p>User unique ID.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Example:",
          "content": "HTTP/1.1 200\n{\n  \"data\": {\n    \"id\": 1,\n    \"message\": \"message_text\",\n    \"user_id\": 1\n  },\n  \"status\": \"OK\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/notification/views.py",
    "groupTitle": "Notifications"
  },
  {
    "type": "delete",
    "url": "notifications/:id",
    "title": "Deleting a notification",
    "name": "DeleteSingleNotification",
    "group": "Notifications",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Notification unique ID.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200\n{\n  \"data\": \"Deleted id=2 notification\",\n  \"status\": \"OK\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 204\n{\n  \"data\": \"There is no notification with this id\",\n  \"status\": \"ERROR\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/notification/views.py",
    "groupTitle": "Notifications"
  },
  {
    "type": "get",
    "url": "notifications/",
    "title": "Getting all notifications",
    "name": "GetNotification",
    "group": "Notifications",
    "version": "1.0.0",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200\n{\n  \"data\": [\n    {\n      \"id\": 1,\n        \"message\": \"message_text\",\n        \"user_id\": 1\n    },\n    {\n      \"id\": 2,\n        \"message\": \"message_text\",\n        \"user_id\": 1\n    }\n  ],\n  \"status\": \"OK\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/notification/views.py",
    "groupTitle": "Notifications"
  },
  {
    "type": "get",
    "url": "notifications/:id",
    "title": "Getting a notification",
    "name": "GetSingleNotification",
    "group": "Notifications",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Notification unique ID.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200\n{\n  \"data\": {\n    \"id\": 1,\n    \"message\": \"message_text\",\n    \"user_id\": 1\n  },\n  \"status\": \"OK\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 204\n{\n  \"data\": \"There is no notification with this id\",\n  \"status\": \"ERROR\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/notification/views.py",
    "groupTitle": "Notifications"
  },
  {
    "type": "put",
    "url": "notifications/:id",
    "title": "Updating a notification",
    "name": "UpdateSingleNotification",
    "group": "Notifications",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Notification unique ID.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Notification message.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "user_id",
            "description": "<p>User unique ID.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200\n{\n  \"data\": {\n    \"id\": 2,\n    \"message\": \"message_text\",\n    \"user_id\": 1\n  },\n  \"status\": \"OK\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 204\n{\n  \"data\": \"There is no notification with this id\",\n  \"status\": \"ERROR\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/notification/views.py",
    "groupTitle": "Notifications"
  },
  {
    "type": "delete",
    "url": "restaurants/:id",
    "title": "Deleting a restaurant",
    "name": "DeleteSingleRestaurant",
    "group": "Restaurants",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Restaurant unique ID.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200\n{\n  \"data\": \"Deleted id=2 restaurant\",\n  \"status\": \"OK\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 204\n{\n  \"data\": \"There is no restaurant with this id\",\n  \"status\": \"ERROR\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/restaurant/views.py",
    "groupTitle": "Restaurants"
  },
  {
    "type": "get",
    "url": "restaurants/",
    "title": "Getting all restaurants",
    "name": "GetRestaurants",
    "group": "Restaurants",
    "version": "1.0.0",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200\n{\n  \"data\": [\n    {\n      \"id\": 2\n    },\n    {\n      \"id\": 3\n    }\n  ],\n  \"status\": \"OK\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/restaurant/views.py",
    "groupTitle": "Restaurants"
  },
  {
    "type": "get",
    "url": "restaurants/:id",
    "title": "Getting a restaurant",
    "name": "GetSingleRestaurant",
    "group": "Restaurants",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Restaurant unique ID.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200\n{\n  \"data\": {\n        \"id\": 2\n    },\n  \"status\": \"OK\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 204\n{\n  \"data\": \"There is no restaurant with this id\",\n  \"status\": \"ERROR\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/restaurant/views.py",
    "groupTitle": "Restaurants"
  },
  {
    "type": "post",
    "url": "services/",
    "title": "Creating a service",
    "name": "CreateService",
    "group": "Services",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Service name.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "service_type",
            "description": "<p>Service type.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "restaurant_id",
            "description": "<p>Restaurant unique ID.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200\n{\n    \"data\": {\n          \"id\": 2,\n          \"name\": \"service_name\",\n          \"service_type\": \"personal\",\n          \"restaurant_id\": 1\n      },\n    \"status\": \"OK\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/service/views.py",
    "groupTitle": "Services"
  },
  {
    "type": "delete",
    "url": "services/:id",
    "title": "Deleting a service",
    "name": "DeleteSingleService",
    "group": "Services",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Service unique ID.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200\n{\n  \"data\": \"Deleted id=2 service\",\n  \"status\": \"OK\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 204\n{\n  \"data\": \"There is no service with this id\",\n  \"status\": \"ERROR\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/service/views.py",
    "groupTitle": "Services"
  },
  {
    "type": "get",
    "url": "services/",
    "title": "Getting all services",
    "name": "GetServices",
    "group": "Services",
    "version": "1.0.0",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200\n{\n    \"data\": [\n        {\n          \"id\": 2,\n          \"name\": \"service_name\",\n          \"service_type\": \"personal\",\n          \"restaurant_id\": 1\n        },\n        {\n          \"id\": 3,\n          \"name\": \"service_name\",\n          \"service_type\": \"personal\",\n          \"restaurant_id\": 1\n        }\n    ],\n    \"status\": \"OK\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/service/views.py",
    "groupTitle": "Services"
  },
  {
    "type": "get",
    "url": "services/:id",
    "title": "Getting a service",
    "name": "GetSingleService",
    "group": "Services",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Service unique ID.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200\n{\n  \"data\": {\n    \"id\": 2,\n    \"name\": \"service_name\",\n    \"service_type\": \"personal\",\n    \"restaurant_id\": 1\n  },\n  \"status\": \"OK\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 204\n{\n  \"data\": \"There is no service with this id\",\n  \"status\": \"ERROR\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/service/views.py",
    "groupTitle": "Services"
  },
  {
    "type": "put",
    "url": "services/:id",
    "title": "Updating a service",
    "name": "UpdateSingleService",
    "group": "Services",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Service unique ID.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Service name.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "service_type",
            "description": "<p>Service type.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "restaurant_id",
            "description": "<p>Restaurant unique ID.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200\n{\n  \"data\": {\n    \"id\": 2,\n    \"name\": \"service_name\",\n    \"service_type\": \"personal\",\n    \"restaurant_id\": 1\n  },\n  \"status\": \"OK\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 204\n{\n  \"data\": \"There is no service with this id\",\n  \"status\": \"ERROR\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/service/views.py",
    "groupTitle": "Services"
  },
  {
    "type": "post",
    "url": "reports/staffs/",
    "title": "Creating a staff",
    "name": "CreateStaff",
    "group": "Staffs",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "description",
            "description": "<p>Staff description.</p>"
          },
          {
            "group": "Parameter",
            "type": "Boolean",
            "optional": false,
            "field": "appearance",
            "description": "<p>Staff appearance.</p>"
          },
          {
            "group": "Parameter",
            "type": "Boolean",
            "optional": false,
            "field": "health",
            "description": "<p>Staff is healthy.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "object_id",
            "description": "<p>Staff object id.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "object_typr",
            "description": "<p>Staff object type.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "type",
            "description": "<p>Staff type.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200\n{\n    \"data\": {\n        \"appearance\": false,\n        \"description\": \"salam\",\n        \"health\": true,\n        \"id\": 31,\n        \"object_id\": 1,\n        \"object_type\": \"staff\",\n        \"type\": \"staff\"\n    },\n    \"status\": \"OK\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/report/views.py",
    "groupTitle": "Staffs"
  },
  {
    "type": "delete",
    "url": "reports/staffs/:id",
    "title": "Deleting a staff",
    "name": "DeleteSingleStaff",
    "group": "Staffs",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Staff unique ID.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200\n{\n  \"data\": \"Deleted id=2 staff\",\n  \"status\": \"OK\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 204\n{\n  \"data\": \"There is no staff with this id\",\n  \"status\": \"ERROR\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/report/views.py",
    "groupTitle": "Staffs"
  },
  {
    "type": "get",
    "url": "reports/staffs/:id",
    "title": "Getting a staff",
    "name": "GetSingleStaff",
    "group": "Staffs",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Staff unique ID.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200\n{\n  \"data\": [\n    {\n      \"appearance\": false,\n      \"description\": \"salam\",\n      \"health\": true,\n      \"id\": 18,\n      \"object_id\": 1,\n      \"object_type\": \"staff\",\n      \"type\": \"staff\"\n    }\n  ],\n  \"status\": \"OK\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 204\n{\n  \"data\": \"There is no staff with this id\",\n  \"status\": \"ERROR\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/report/views.py",
    "groupTitle": "Staffs"
  },
  {
    "type": "get",
    "url": "reports/staffs/",
    "title": "Getting all staff",
    "name": "GetStaffs",
    "group": "Staffs",
    "version": "1.0.0",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200\n{\n  \"data\": [\n    {\n      \"appearance\": false,\n      \"description\": \"salam\",\n      \"health\": true,\n      \"id\": 18,\n      \"object_id\": 1,\n      \"object_type\": \"staff\",\n      \"type\": \"staff\"\n    },\n    {\n      \"appearance\": false,\n      \"description\": \"salam\",\n      \"health\": true,\n      \"id\": 19,\n      \"object_id\": 1,\n      \"object_type\": \"staff\",\n      \"type\": \"staff\"\n    }\n  ],\n  \"status\": \"OK\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/report/views.py",
    "groupTitle": "Staffs"
  },
  {
    "type": "put",
    "url": "reports/staffs/:id",
    "title": "Updating a staff",
    "name": "UpdateSingleStaff",
    "group": "Staffs",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Staff unique ID.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "description",
            "description": "<p>Staff description.</p>"
          },
          {
            "group": "Parameter",
            "type": "Boolean",
            "optional": false,
            "field": "appearance",
            "description": "<p>Staff appearance.</p>"
          },
          {
            "group": "Parameter",
            "type": "Boolean",
            "optional": false,
            "field": "health",
            "description": "<p>Staff is healthy.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "object_id",
            "description": "<p>Staff object id.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "object_typr",
            "description": "<p>Staff object type.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "type",
            "description": "<p>Staff type.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200\n{\n  \"data\": {\n      \"appearance\": false,\n      \"description\": \"salam\",\n      \"health\": true,\n      \"id\": 18,\n      \"object_id\": 1,\n      \"object_type\": \"staff\",\n      \"type\": \"staff\"\n    },\n  \"status\": \"OK\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 204\n{\n  \"data\": \"There is no staff with this id\",\n  \"status\": \"ERROR\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/report/views.py",
    "groupTitle": "Staffs"
  },
  {
    "type": "post",
    "url": "suppliers/",
    "title": "Creating a supplier",
    "name": "CreateSupplier",
    "group": "Suppliers",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Supplier name.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "address",
            "description": "<p>Supplier Address.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "contact_phone",
            "description": "<p>Supplier contact phone.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200\n{\n    \"data\": {\n        \"id\": 7,\n        \"name\": \"supplier_name\",\n        \"address\": null,\n        \"contact_phone\": \"09121234532\"\n      },\n    \"status\": \"OK\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/supplier/views.py",
    "groupTitle": "Suppliers"
  },
  {
    "type": "post",
    "url": "reports/suppliers/",
    "title": "Creating a supplier",
    "name": "CreateSupplier",
    "group": "Suppliers",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Supplier name.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "address",
            "description": "<p>Supplier address.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "contact_phone",
            "description": "<p>Supplier contact phone.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200\n{\n    \"data\": {\n        \"id\": 1,\n        \"name\": \"supplier_name\",\n        \"address\": \"supplier_address\",\n        \"contact_phone\": \"0217676767\"\n      },\n    \"status\": \"OK\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/report/views.py",
    "groupTitle": "Suppliers"
  },
  {
    "type": "delete",
    "url": "supplier/:id",
    "title": "Deleting a supplier",
    "name": "DeleteSingleSupplier",
    "group": "Suppliers",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Supplier unique ID.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200\n{\n  \"data\": \"Deleted id=2 supplier\",\n  \"status\": \"OK\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 204\n{\n  \"data\": \"There is no supplier with this id\",\n  \"status\": \"ERROR\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/supplier/views.py",
    "groupTitle": "Suppliers"
  },
  {
    "type": "delete",
    "url": "reports/suppliers/:id",
    "title": "Deleting a food",
    "name": "DeleteSingleSupplier",
    "group": "Suppliers",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Supplier unique ID.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200\n{\n  \"data\": \"Deleted id=2 supplier\",\n  \"status\": \"OK\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 204\n{\n  \"data\": \"There is no supplier with this id\",\n  \"status\": \"ERROR\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/report/views.py",
    "groupTitle": "Suppliers"
  },
  {
    "type": "get",
    "url": "reports/suppliers/:id",
    "title": "Getting a supplier",
    "name": "GetSingleSupplier",
    "group": "Suppliers",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Supplier unique ID.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200\n{\n  \"data\": {\n        \"id\": 1,\n        \"name\": \"supplier_name\",\n        \"address\": \"supplier_address\",\n        \"contact_phone\": \"0217676767\"\n        },\n  \"status\": \"OK\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 204\n{\n  \"data\": \"There is no supplier with this id\",\n  \"status\": \"ERROR\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/report/views.py",
    "groupTitle": "Suppliers"
  },
  {
    "type": "get",
    "url": "suppliers/:id",
    "title": "Getting a supplier",
    "name": "GetSingleSupplier",
    "group": "Suppliers",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Supplier unique ID.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200\n{\n  \"data\": {\n        \"id\": 7,\n        \"name\": \"supplier_name\",\n        \"address\": null,\n        \"contact_phone\": \"09121234532\"\n  },\n  \"status\": \"OK\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 204\n{\n  \"data\": \"There is no supplier with this id\",\n  \"status\": \"ERROR\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/supplier/views.py",
    "groupTitle": "Suppliers"
  },
  {
    "type": "get",
    "url": "suppliers/",
    "title": "Getting all suppliers",
    "name": "GetSuppliers",
    "group": "Suppliers",
    "version": "1.0.0",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200\n{\n    \"data\": [\n        {\n          \"id\": 7,\n          \"name\": \"supplier_name\",\n          \"address\": null,\n          \"contact_phone\": \"09121234532\"\n        },\n        {\n          \"id\": 8,\n          \"name\": \"supplier_name\",\n          \"address\": null,\n          \"contact_phone\": \"09121234532\"\n        }\n    ],\n    \"status\": \"OK\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/supplier/views.py",
    "groupTitle": "Suppliers"
  },
  {
    "type": "get",
    "url": "reports/suppliers/",
    "title": "Getting all suppliers",
    "name": "GetSuppliers",
    "group": "Suppliers",
    "version": "1.0.0",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200\n{\n    \"data\": [\n        {\n            \"id\": 1,\n            \"name\": \"supplier_name\",\n            \"address\": \"supplier_address\",\n            \"contact_phone\": \"0217676767\"\n        },\n        {\n            \"id\": 2,\n            \"name\": \"supplier_name\",\n            \"address\": \"supplier_address\",\n            \"contact_phone\": \"0217676767\"\n        }\n    ],\n    \"status\": \"OK\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/report/views.py",
    "groupTitle": "Suppliers"
  },
  {
    "type": "put",
    "url": "suppliers/:id",
    "title": "Updating a supplier",
    "name": "UpdateSingleSupplier",
    "group": "Suppliers",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Supplier unique ID.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Supplier name.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "address",
            "description": "<p>Supplier Address.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "contact_phone",
            "description": "<p>Supplier contact phone.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200\n{\n  \"data\": {\n            \"id\": 2,\n            \"name\": \"supplier_name\",\n            \"address\": null,\n            \"contact_phone\": \"09121234532\"\n  },\n  \"status\": \"OK\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 204\n{\n  \"data\": \"There is no supplier with this id\",\n  \"status\": \"ERROR\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/supplier/views.py",
    "groupTitle": "Suppliers"
  },
  {
    "type": "put",
    "url": "reports/suppliers/:id",
    "title": "Updating a supplier",
    "name": "UpdateSingleSupplier",
    "group": "Suppliers",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Supplier unique ID.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Supplier name.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "address",
            "description": "<p>Supplier address.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "contact_phone",
            "description": "<p>Supplier contact phone.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200\n{\n  \"data\": {\n        \"id\": 1,\n        \"name\": \"supplier_name\",\n        \"address\": \"supplier_address\",\n        \"contact_phone\": \"0217676767\"\n        },\n  \"status\": \"OK\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 204\n{\n  \"data\": \"There is no supplier with this id\",\n  \"status\": \"ERROR\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/report/views.py",
    "groupTitle": "Suppliers"
  },
  {
    "type": "post",
    "url": "tasks/",
    "title": "Creating a task",
    "name": "CreateTask",
    "group": "Tasks",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "time_schedule",
            "description": "<p>Task timeschedule.</p>"
          },
          {
            "group": "Parameter",
            "type": "number",
            "optional": false,
            "field": "restaurant_id",
            "description": "<p>User restaurant ID.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Example:",
          "content": "HTTP/1.1 200\n{\n  \"data\": {\n    \"id\": 1,\n    \"time_schedule\": null,\n    \"restaurant_id\": null\n  },\n  \"status\": \"OK\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/task/views.py",
    "groupTitle": "Tasks"
  },
  {
    "type": "delete",
    "url": "tasks/:id",
    "title": "Deleting a task",
    "name": "DeleteSingleTask",
    "group": "Tasks",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Task unique ID.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200\n{\n  \"data\": \"Deleted id=2 task\",\n  \"status\": \"OK\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 204\n{\n  \"data\": \"There is no task with this id\",\n  \"status\": \"ERROR\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/task/views.py",
    "groupTitle": "Tasks"
  },
  {
    "type": "get",
    "url": "tasks/:id",
    "title": "Getting a task",
    "name": "GetSingleTask",
    "group": "Tasks",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Task unique ID.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200\n{\n  \"data\": {\n    \"id\": 2,\n    \"time_schedule\": null,\n    \"restaurant_id\": 1\n  },\n  \"status\": \"OK\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 204\n{\n  \"data\": \"There is no task with this id\",\n  \"status\": \"ERROR\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/task/views.py",
    "groupTitle": "Tasks"
  },
  {
    "type": "get",
    "url": "tasks/",
    "title": "Getting all tasks",
    "name": "GetTasks",
    "group": "Tasks",
    "version": "1.0.0",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200\n{\n    \"data\": [\n        {\n          \"id\": 2,\n          \"time_schedule\": null,\n          \"restaurant_id\": 1\n        },\n        {\n          \"id\": 3,\n          \"time_schedule\": null,\n          \"restaurant_id\": 1\n        }\n    ],\n    \"status\": \"OK\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/task/views.py",
    "groupTitle": "Tasks"
  },
  {
    "type": "put",
    "url": "tasks/:id",
    "title": "Updating a task",
    "name": "UpdateSingleTask",
    "group": "Tasks",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Task unique ID.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "time_schedule",
            "description": "<p>Task time schedule.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "restaurant_id",
            "description": "<p>Restaurant unique ID.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200\n{\n  \"data\": {\n    \"id\": 2,\n    \"time_schedule\": null,\n    \"restaurant_id\": 1\n  },\n  \"status\": \"OK\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 204\n{\n  \"data\": \"There is no task with this id\",\n  \"status\": \"ERROR\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/task/views.py",
    "groupTitle": "Tasks"
  },
  {
    "type": "post",
    "url": "reports/temperatures/",
    "title": "Creating a temperature",
    "name": "CreateTemperature",
    "group": "Temperatures",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "description",
            "description": "<p>Temperature description.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "temperature",
            "description": "<p>Temperature value.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "object_type",
            "description": "<p>Temperature object type.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "object_id",
            "description": "<p>Temperature object ID.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200\n{\n    \"data\": {\n          \"id\": 7,\n          \"description\": \"salam\",\n          \"temperature\": 10,\n          \"object_type\": \"instrument\",\n          \"object_id\": 1\n      },\n    \"status\": \"OK\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/report/views.py",
    "groupTitle": "Temperatures"
  },
  {
    "type": "delete",
    "url": "reports/temperatures/:id",
    "title": "Deleting a temperature",
    "name": "DeleteSingleTemperature",
    "group": "Temperatures",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Temperature unique ID.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200\n{\n  \"data\": \"Deleted id=2 temperature\",\n  \"status\": \"OK\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 204\n{\n  \"data\": \"There is no temperature report with this id\",\n  \"status\": \"ERROR\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/report/views.py",
    "groupTitle": "Temperatures"
  },
  {
    "type": "get",
    "url": "reports/temperatures:id",
    "title": "Getting a temperature",
    "name": "GetSingleTemperature",
    "group": "Temperatures",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Temperature unique ID.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200\n{\n  \"data\": {\n      \"description\": \"salam\",\n      \"id\": 24,\n      \"object_id\": 1,\n      \"object_type\": \"instrument\",\n      \"temperature\": 10,\n      \"type\": \"temperature\"1\n  },\n  \"status\": \"OK\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 204\n{\n  \"data\": \"There is no temperature report with this id\",\n  \"status\": \"ERROR\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/report/views.py",
    "groupTitle": "Temperatures"
  },
  {
    "type": "get",
    "url": "reports/temperatures",
    "title": "Getting all temperatures",
    "name": "GetTemperature",
    "group": "Temperatures",
    "version": "1.0.0",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200\n{\n  \"data\": [\n    {\n      \"description\": \"salam\",\n      \"id\": 24,\n      \"object_id\": 1,\n      \"object_type\": \"instrument\",\n      \"temperature\": 10,\n      \"type\": \"temperature\"\n    },\n    {\n      \"description\": \"salam\",\n      \"id\": 25,\n      \"object_id\": 1,\n      \"object_type\": \"instrument\",\n      \"temperature\": 10,\n      \"type\": \"temperature\"\n    }\n  ],\n  \"status\": \"OK\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/report/views.py",
    "groupTitle": "Temperatures"
  },
  {
    "type": "put",
    "url": "reports/temperatures/:id",
    "title": "Updating a temperature",
    "name": "UpdateSingleTemperature",
    "group": "Temperatures",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Temperature unique ID.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "description",
            "description": "<p>Temperature description.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "temperature",
            "description": "<p>Temperature value.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "object_type",
            "description": "<p>Temperature object type.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "object_id",
            "description": "<p>Temperature object ID.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200\n{\n  \"data\": {\n      \"description\": \"salam\",\n      \"id\": 24,\n      \"object_id\": 1,\n      \"object_type\": \"instrument\",\n      \"temperature\": 10,\n      \"type\": \"temperature\"1\n  },\n  \"status\": \"OK\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 204\n{\n  \"data\": \"There is no temperature with this id\",\n  \"status\": \"ERROR\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/report/views.py",
    "groupTitle": "Temperatures"
  },
  {
    "type": "post",
    "url": "users/",
    "title": "Creating a user",
    "name": "CreateUser",
    "group": "Users",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "size": "..32",
            "optional": false,
            "field": "name",
            "description": "<p>User name.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "size": "..16",
            "optional": false,
            "field": "phone",
            "description": "<p>User phone number.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "size": "..64",
            "optional": false,
            "field": "email",
            "description": "<p>User email.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "size": "..64",
            "optional": false,
            "field": "password",
            "description": "<p>User password.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "my_restaurant_id",
            "description": "<p>User's restaurant ID.</p>"
          },
          {
            "group": "Parameter",
            "type": "number",
            "optional": false,
            "field": "restaurant_id",
            "description": "<p>User restaurant ID.</p>"
          },
          {
            "group": "Parameter",
            "type": "Boolean",
            "optional": false,
            "field": "is_working",
            "defaultValue": "False",
            "description": "<p>User restaurant status.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "size": "..20",
            "optional": false,
            "field": "type",
            "description": "<p>User type.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Example:",
          "content": "HTTP/1.1 200\n{\n  \"data\": {\n    \"id\": 1,\n    \"is_working\": true,\n    \"my_restaurant\": null,\n    \"my_restaurant_id\": null,\n    \"name\": \"name_test\",\n    \"phone\": \"phone_test\",\n    \"t1\": false,\n    \"t2\": false,\n    \"t3\": true,\n    \"t4\": false,\n    \"t5\": false,\n    \"t6\": true,\n    \"t7\": false,\n    \"t8\": true,\n    \"type\": \"staff\"\n  },\n  \"status\": \"OK\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/user/views.py",
    "groupTitle": "Users"
  },
  {
    "type": "delete",
    "url": "users/:id",
    "title": "Deleting a user",
    "name": "DeleteSingleUser",
    "group": "Users",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>User unique ID.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200\n{\n  \"data\": \"Deleted id=2 user\",\n  \"status\": \"OK\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 204\n{\n  \"data\": \"There is no user with this id\",\n  \"status\": \"ERROR\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/user/views.py",
    "groupTitle": "Users"
  },
  {
    "type": "get",
    "url": "users/:id",
    "title": "Getting a user",
    "name": "GetSingleUser",
    "group": "Users",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>User unique ID.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200\n{\n  \"data\": {\n    \"id\": 4,\n    \"is_working\": true,\n    \"my_restaurant\": null,\n    \"my_restaurant_id\": null,\n    \"name\": \"name_test\",\n    \"phone\": \"phone_test\",\n    \"t1\": false,\n    \"t2\": false,\n    \"t3\": true,\n    \"t4\": false,\n    \"t5\": false,\n    \"t6\": true,\n    \"t7\": false,\n    \"t8\": true,\n    \"type\": \"staff\"\n  },\n  \"status\": \"OK\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 204\n{\n  \"data\": \"There is no user with this id\",\n  \"status\": \"ERROR\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/user/views.py",
    "groupTitle": "Users"
  },
  {
    "type": "get",
    "url": "users/",
    "title": "Getting all users",
    "name": "GetUser",
    "group": "Users",
    "version": "1.0.0",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200\n{\n  \"data\": [\n    {\n      \"id\": 1,\n      \"is_working\": true,\n      \"my_restaurant\": null,\n      \"my_restaurant_id\": null,\n      \"name\": \"name_test\",\n      \"phone\": \"phone_test\",\n      \"t1\": false,\n      \"t2\": false,\n      \"t3\": true,\n      \"t4\": false,\n      \"t5\": false,\n      \"t6\": true,\n      \"t7\": false,\n      \"t8\": true,\n      \"type\": \"staff\"\n    },\n    {\n      \"id\": 2,\n      \"is_working\": true,\n      \"my_restaurant\": null,\n      \"my_restaurant_id\": null,\n      \"name\": \"name_test\",\n      \"phone\": \"phone_test\",\n      \"t1\": false,\n      \"t2\": false,\n      \"t3\": true,\n      \"t4\": false,\n      \"t5\": false,\n      \"t6\": true,\n      \"t7\": false,\n      \"t8\": true,\n      \"type\": \"staff\"\n    }\n  ],\n  \"status\": \"OK\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/user/views.py",
    "groupTitle": "Users"
  },
  {
    "type": "put",
    "url": "users/:id",
    "title": "Updating a user",
    "name": "UpdateSingleUser",
    "group": "Users",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Notification unique ID.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "size": "..32",
            "optional": false,
            "field": "name",
            "description": "<p>User name.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "size": "..16",
            "optional": false,
            "field": "phone",
            "description": "<p>User phone number.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "size": "..64",
            "optional": false,
            "field": "email",
            "description": "<p>User email.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "size": "..64",
            "optional": false,
            "field": "password",
            "description": "<p>User password.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "my_restaurant_id",
            "description": "<p>User's restaurant ID.</p>"
          },
          {
            "group": "Parameter",
            "type": "number",
            "optional": false,
            "field": "restaurant_id",
            "description": "<p>User restaurant ID.</p>"
          },
          {
            "group": "Parameter",
            "type": "Boolean",
            "optional": false,
            "field": "is_working",
            "defaultValue": "False",
            "description": "<p>User restaurant status.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "size": "..20",
            "optional": false,
            "field": "type",
            "description": "<p>User type.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200\n{\n  \"data\": {\n    \"id\": 4,\n    \"is_working\": true,\n    \"my_restaurant\": null,\n    \"my_restaurant_id\": null,\n    \"name\": \"name_test\",\n    \"phone\": \"phone_test\",\n    \"t1\": false,\n    \"t2\": false,\n    \"t3\": true,\n    \"t4\": false,\n    \"t5\": false,\n    \"t6\": true,\n    \"t7\": false,\n    \"t8\": true,\n    \"type\": \"staff\"\n  },\n  \"status\": \"OK\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 204\n{\n  \"data\": \"There is no user with this id\",\n  \"status\": \"ERROR\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/modules/user/views.py",
    "groupTitle": "Users"
  }
] });
