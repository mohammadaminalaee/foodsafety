$(window).load(function() {

  //The Calender
  $("#calendar").datepicker();

  //Date range picker
   $('#daterange').daterangepicker();

   // reply service man
   $('select.btn').change(function(){
   	if( $(this).val() == "custom" ){
   		$(this).parent('.btn-group').find('.texarearow').show();
   	}else{
   		$(this).parent('.btn-group').find('.texarearow').hide();
   	}
   })

});