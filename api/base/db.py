from datetime import datetime
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy_utils import Timestamp, get_columns, has_changes
from sqlalchemy.dialects.postgresql import JSONB
from sqlalchemy import func

class DB(SQLAlchemy):
    _instance = None

    def __new__(cls, *args, **kwargs):
        if not cls._instance:
            cls._instance = super(DB, cls).__new__(cls, *args, **kwargs)

        return cls._instance


db = DB()


class FSModel(object):
    @classmethod
    def get(cls, id=None, query=None, args=None):
        if not query:
            query = db.session.query(cls)

        if id:
            query = query.filter(getattr(cls, 'id') == id)

        for key, val in args.items():
            mode = 'eq'
            min_key = key.split('_min_')
            max_key = key.split('_max_')
            in_key  = key.split('_in_')

            if len(min_key) == 2:
                key = min_key[1]
                mode = 'bt'
            elif len(max_key) == 2:
                key = max_key[1]
                mode = 'st'
            elif len(in_key) == 2:
                key = in_key[1]
                mode = 'in'

            if hasattr(cls, key):
                attr = getattr(cls, key)
                if mode == 'eq':
                    query = query.filter(attr == val)
                elif mode == 'bt':
                    query = query.filter(attr >= val)
                elif mode == 'st':
                    query = query.filter(attr <= val)
                elif mode == 'in':
                    query = query.filter(attr.contains(val))

        result = query.all()
        if len(result) == 1:
            return result[0]
        return result

    @classmethod
    def add(cls, obj):
        db.session.add(obj)
        db.session.commit()

    @classmethod
    def remove(cls, obj):
        db.session.delete(obj)
        db.session.commit()

    @classmethod  # TODO => update because wh have jsonb
    def update(cls, obj):
        obj.updated = datetime.now()
        obj.query.update({c.key: getattr(obj, c.key) for c in get_columns(obj) if has_changes(obj, c.key)
                           or isinstance(getattr(obj, c.key), JSONB)}, synchronize_session=False)
        db.session.commit()
