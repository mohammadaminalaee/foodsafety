class WtfFormData(object):
    my_json = None

    def __init__(self, json):
        self.my_json = json

    def __iter__(self):
        return iter(self.my_json)

    def __len__(self):
        return len(self.my_json)

    def __contains__(self, name):
        return (name in self.my_json)

    def getlist(self, name):
        return [self.my_json.get(name)]