from flask import request
from flask_login import LoginManager

login_manager = LoginManager()

from ..modules.user.models import User


@login_manager.user_loader
def load_user(user_id):
    user = User.get(id=user_id, args=request.args)

    return user if user else None