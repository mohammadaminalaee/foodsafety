from flask import make_response, json, jsonify
from .db import db


# TODO => if obj to loop
def obj_to_dict(objs, includes=None):
    if not isinstance(objs, list):
        di = objs.v_map
        if includes:
            di.update({key: key for key in includes})
        # print ')()'*40, 'einjam', objs.id

        return {val: getattr(objs, key) for key, val in di.items() if hasattr(objs, key)}

    res = []
    for obj in objs:
        di = obj.v_map
        if includes:
            di.update({key: key for key in includes})

        res.append({val: getattr(obj, key) for key, val in di.items() if hasattr(obj, key)})

    return res


# utility functions but Base Function
def make_rest(status, data, code, include=None, headers=None):
    if isinstance(data, db.Model) or \
            (isinstance(data, list) and len(data) > 0 and isinstance(data[0], db.Model)):
        data = obj_to_dict(data, include.split(',') if include else None)

    resp = {'status': status, 'data': data}
    # resp.headers.extend(headers or {})
    return jsonify(resp)