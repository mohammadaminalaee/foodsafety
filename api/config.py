class BaseConfig(object):
    DEBUG = False
    # SERVER_NAME = '127.0.0.1:8080'  # Full path => TODO at deploy time get null
    SECRET_KEY = 'testkey'
    HOST = '127.0.0.1'
    PORT = 8080
    SESSION_COOKIE_NAME = 'temp'
    SESSION_COOKIE_SECURE = True


    MODULES = [
            ('admin module - control app', 'dashboard'),
            ('auth module', 'auth'),
            ('food module', 'food'),
            ('instrument module', 'instrument'),
            ('material module', 'material'),
            ('notification module', 'notification'),
            ('report module', 'report'),
            ('restaurant.certificate module', 'restaurant.certificate'),
            ('restaurant module', 'restaurant'),
            ('service module', 'service'),
            ('supplier.invoice module', 'supplier.invoice'),
            ('supplier module', 'supplier'),
            ('task module', 'task'),
            ('user module', 'user'),
        ]
    STATIC_FOLDER = 'static'
    TEMPLATE_FOLDER = 'templates'

    #SQLALCHEMY_DATABASE_URI = 'postgresql+psycopg2://yaserabbasi:testtest@127.0.0.1:5432/fsm' # Localhost
    SQLALCHEMY_DATABASE_URI = 'postgresql+psycopg2://postgres:london2016@127.0.0.1:5432/fsm' # Server

    SQLALCHEMY_POOL_SIZE = 10


class DevConfig(BaseConfig):
    DEBUG = True


class DepConfig(BaseConfig):
    DEBUG = False
