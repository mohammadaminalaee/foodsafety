from flask import Blueprint
mod = Blueprint('task', __name__, url_prefix='/api/v1/tasks')
from .views import *
