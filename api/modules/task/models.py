from ...base.db import db, Timestamp, FSModel


# cron job
class Task(db.Model, Timestamp, FSModel):
    id = db.Column(db.Integer, primary_key=True)
    time_schedule = db.Column(db.DateTime)
    restaurant_id = db.Column(db.Integer, db.ForeignKey('restaurant.id'))

    @property
    def v_map(self):
        return {
            'id': 'id',
            'time_schedule': 'time_schedule',
            'restaurant_id': 'restaurant_id'
        }