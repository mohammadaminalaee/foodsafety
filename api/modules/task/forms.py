from wtforms_alchemy import ModelForm
from .models import Task


class TaskForm(ModelForm):
    class Meta:
        model = Task