from flask import request
from flask.ext.login import login_required
from ...base.rest import make_rest
from ...base.wtform import WtfFormData
from .forms import Task, TaskForm
from . import mod


@mod.route('/', methods=['POST'])
@login_required
def create_task():
    """
    @api {post} tasks/ Creating a task
    @apiName CreateTask
    @apiGroup Tasks
    @apiVersion 1.0.0

    @apiParam {String} time_schedule Task timeschedule.
    @apiParam {number} restaurant_id User restaurant ID.

    @apiSuccessExample {json} Success-Example:
    HTTP/1.1 200
    {
      "data": {
        "id": 1,
        "time_schedule": null,
        "restaurant_id": null
      },
      "status": "OK"
    }
    """
    form = TaskForm(data=request.json)
    if form.validate():
        task = Task()
        form.populate_obj(task)
        Task.add(task)

        return make_rest('OK', task, 200)

    return make_rest('ERROR', form.errors, 400)

@mod.route('/', methods=['GET'])
@login_required
def get_tasks():
    """
    @api {get} tasks/ Getting all tasks
    @apiName GetTasks
    @apiGroup Tasks
    @apiVersion 1.0.0

    @apiSuccessExample {json} Success-Response:
    HTTP/1.1 200
    {
        "data": [
            {
              "id": 2,
              "time_schedule": null,
              "restaurant_id": 1
            },
            {
              "id": 3,
              "time_schedule": null,
              "restaurant_id": 1
            }
        ],
        "status": "OK"
    }
    """
    tasks = Task.get(args=request.args)
    return make_rest('OK', tasks, 200, include=request.args.get('include', None))


@mod.route('/<id>/', methods=['GET'])
@login_required
def get_task(id):
    """
    @api {get} tasks/:id Getting a task
    @apiName GetSingleTask
    @apiGroup Tasks
    @apiVersion 1.0.0

    @apiParam {Number} id Task unique ID.

    @apiSuccessExample {json} Success-Response:
    HTTP/1.1 200
    {
      "data": {
        "id": 2,
        "time_schedule": null,
        "restaurant_id": 1
      },
      "status": "OK"
    }

    @apiErrorExample {json} Error-Response:
    HTTP/1.1 204
    {
      "data": "There is no task with this id",
      "status": "ERROR"
    }
    """
    task = Task.get(id=int(id), args=request.args)
    if not task:
        return make_rest('ERROR', 'There is no task with this id', 204)

    return make_rest('OK', task, 200, include=request.args.get('include', None))
    
@mod.route('/<id>/', methods=['PUT'])
@login_required
def update_task(id):
    """
    @api {put} tasks/:id Updating a task
    @apiName UpdateSingleTask
    @apiGroup Tasks
    @apiVersion 1.0.0

    @apiParam {Number} id Task unique ID.
    @apiParam {String} time_schedule Task time schedule.
    @apiParam {Number} restaurant_id Restaurant unique ID.

    @apiSuccessExample {json} Success-Response:
    HTTP/1.1 200
    {
      "data": {
        "id": 2,
        "time_schedule": null,
        "restaurant_id": 1
      },
      "status": "OK"
    }

    @apiErrorExample {json} Error-Response:
    HTTP/1.1 204
    {
      "data": "There is no task with this id",
      "status": "ERROR"
    }
    """
    task = Task.get(id=int(id), args=request.args)
    if not task:
        return make_rest('ERROR', 'There is no task report with this id', 204)

    form = TaskForm(WtfFormData(request.json), obj=task)
    if form.validate():
        form.populate_obj(task)
        Task.update(task)

        return make_rest('OK', task, 200)

    return make_rest('ERROR', form.errors, 400)

@mod.route('/<id>/', methods=['DELETE'])
@login_required
def delete_task(id):
    """
    @api {delete} tasks/:id Deleting a task
    @apiName DeleteSingleTask
    @apiGroup Tasks
    @apiVersion 1.0.0

    @apiParam {Number} id Task unique ID.

    @apiSuccessExample {json} Success-Response:
    HTTP/1.1 200
    {
      "data": "Deleted id=2 task",
      "status": "OK"
    }

    @apiErrorExample {json} Error-Response:
    HTTP/1.1 204
    {
      "data": "There is no task with this id",
      "status": "ERROR"
    }
    """
    task = Task.get(id=int(id), args=request.args)
    if task:
        Task.remove(task)
        return make_rest('OK', 'Deleted id={0} task report'.format(id), 200)

    return make_rest('ERROR', 'There is no task report with this id', 204)
