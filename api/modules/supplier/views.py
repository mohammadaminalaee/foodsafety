from flask import request
from flask.ext.login import login_required
from ...base.rest import make_rest
from ...base.wtform import WtfFormData
from .forms import Supplier, SupplierForm
from . import mod


@mod.route('/', methods=['POST'])
@login_required
def create_supplier():
    """
    @api {post} suppliers/ Creating a supplier
    @apiName CreateSupplier
    @apiGroup Suppliers
    @apiVersion 1.0.0

    @apiParam {String} name Supplier name.
    @apiParam {String} address Supplier Address.
    @apiParam {String} contact_phone Supplier contact phone.

    @apiSuccessExample {json} Success-Response:
    HTTP/1.1 200
    {
        "data": {
            "id": 7,
            "name": "supplier_name",
            "address": null,
            "contact_phone": "09121234532"
          },
        "status": "OK"
    }
    """
    form = SupplierForm(data=request.json)
    if form.validate():
        supplier = Supplier()
        form.populate_obj(supplier)
        Supplier.add(supplier)

        return make_rest('OK', supplier, 200)

    return make_rest('ERROR', form.errors, 400)

@mod.route('/', methods=['GET'])
@login_required
def get_suppliers():
    """
    @api {get} suppliers/ Getting all suppliers
    @apiName GetSuppliers
    @apiGroup Suppliers
    @apiVersion 1.0.0

    @apiSuccessExample {json} Success-Response:
    HTTP/1.1 200
    {
        "data": [
            {
              "id": 7,
              "name": "supplier_name",
              "address": null,
              "contact_phone": "09121234532"
            },
            {
              "id": 8,
              "name": "supplier_name",
              "address": null,
              "contact_phone": "09121234532"
            }
        ],
        "status": "OK"
    }
    """
    suppliers = Supplier.get(args=request.args)
    return make_rest('OK', suppliers, 200, include=request.args.get('include', None))


@mod.route('/<id>/', methods=['GET'])
@login_required
def get_supplier(id):
    """
    @api {get} suppliers/:id Getting a supplier
    @apiName GetSingleSupplier
    @apiGroup Suppliers
    @apiVersion 1.0.0

    @apiParam {Number} id Supplier unique ID.

    @apiSuccessExample {json} Success-Response:
    HTTP/1.1 200
    {
      "data": {
            "id": 7,
            "name": "supplier_name",
            "address": null,
            "contact_phone": "09121234532"
      },
      "status": "OK"
    }

    @apiErrorExample {json} Error-Response:
    HTTP/1.1 204
    {
      "data": "There is no supplier with this id",
      "status": "ERROR"
    }
    """
    supplier = Supplier.get(id=int(id), args=request.args)
    if not supplier:
        return make_rest('ERROR', None, 204)

    return make_rest('OK', supplier, 200, include=request.args.get('include', None))
    
@mod.route('/<id>/', methods=['PUT'])
@login_required
def update_supplier(id):
    """
    @api {put} suppliers/:id Updating a supplier
    @apiName UpdateSingleSupplier
    @apiGroup Suppliers
    @apiVersion 1.0.0

    @apiParam {Number} id Supplier unique ID.
    @apiParam {String} name Supplier name.
    @apiParam {String} address Supplier Address.
    @apiParam {String} contact_phone Supplier contact phone.

    @apiSuccessExample {json} Success-Response:
    HTTP/1.1 200
    {
      "data": {
                "id": 2,
                "name": "supplier_name",
                "address": null,
                "contact_phone": "09121234532"
      },
      "status": "OK"
    }

    @apiErrorExample {json} Error-Response:
    HTTP/1.1 204
    {
      "data": "There is no supplier with this id",
      "status": "ERROR"
    }
    """
    supplier = Supplier.get(id=int(id), args=request.args)
    if not supplier:
        return make_rest('ERROR', 'There is no supplier report with this id', 204)

    form = SupplierForm(WtfFormData(request.json), obj=supplier)
    if form.validate():
        form.populate_obj(supplier)
        Supplier.update(supplier)

        return make_rest('OK', supplier, 200)

    return make_rest('ERROR', form.errors, 400)

@mod.route('/<id>/', methods=['DELETE'])
@login_required
def delete_supplier(id):
    """
    @api {delete} supplier/:id Deleting a supplier
    @apiName DeleteSingleSupplier
    @apiGroup Suppliers
    @apiVersion 1.0.0

    @apiParam {Number} id Supplier unique ID.

    @apiSuccessExample {json} Success-Response:
    HTTP/1.1 200
    {
      "data": "Deleted id=2 supplier",
      "status": "OK"
    }

    @apiErrorExample {json} Error-Response:
    HTTP/1.1 204
    {
      "data": "There is no supplier with this id",
      "status": "ERROR"
    }
    """
    supplier = Supplier.get(id=int(id), args=request.args)
    if supplier:
        Supplier.remove(supplier)
        return make_rest('OK', 'Deleted id={0} supplier report'.format(id), 200)

    return make_rest('ERROR', 'There is no supplier report with this id', 204)
