from flask import Blueprint
mod = Blueprint('supplier', __name__, url_prefix='/api/v1/suppliers')
from .views import *
