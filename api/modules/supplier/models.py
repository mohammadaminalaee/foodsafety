from ...base.db import db, Timestamp, FSModel

from .invoice import Invoice


class Supplier(db.Model, Timestamp, FSModel):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(32))
    address = db.Column(db.String)
    contact_phone = db.Column(db.String(16))

    invoices = db.relationship('Invoice', backref='supplier')

    @property
    def v_map(self):
        return {
            'id': 'id',
            'name': 'name',
            'address': 'address',
            'contact_phone': 'contact_phone'
        }