from wtforms_alchemy import ModelForm
from .models import Invoice


class InvoiceForm(ModelForm):
    class Meta:
        model = Invoice