from flask import Blueprint
mod = Blueprint('invoice', __name__, url_prefix='/api/v1/invoices')
from .views import *
