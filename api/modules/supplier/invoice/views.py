from flask import request
from flask.ext.login import login_required
from ....base.rest import make_rest
from ....base.wtform import WtfFormData
from .forms import Invoice, InvoiceForm
from . import mod


@mod.route('/', methods=['POST'])
@login_required
def create_invoice():
    """
    @api {post} invoices/ Creating an invoice
    @apiName CreateInvoice
    @apiGroup Invoices
    @apiVersion 1.0.0

    @apiParam {Number} supplier_id Supplier unique ID.

    @apiSuccessExample {json} Success-Response:
    HTTP/1.1 200
    {
        "data": {
            "id": 7,
            "supplier_id": 1
          },
        "status": "OK"
    }
    """
    form = InvoiceForm(data=request.json)
    if form.validate():
        invoice = Invoice()
        form.populate_obj(invoice)
        Invoice.add(invoice)

        return make_rest('OK', invoice, 200)

    return make_rest('ERROR', form.errors, 400)

@mod.route('/', methods=['GET'])
@login_required
def get_invoices():
    """
    @api {get} invoices/ Getting all invoices
    @apiName GetInvoices
    @apiGroup Invoices
    @apiVersion 1.0.0

    @apiSuccessExample {json} Success-Response:
    HTTP/1.1 200
    {
        "data": [
            {
              "id": 2,
              "supplier_id": 1
            },
            {
              "id": 3,
              "supplier_id": 1
            }
        ],
        "status": "OK"
    }
    """
    invoices = Invoice.get(args=request.args)
    return make_rest('OK', invoices, 200, include=request.args.get('include', None))


@mod.route('/<id>/', methods=['GET'])
@login_required
def get_invoice(id):
    """
    @api {get} invoices/:id Getting an invoice
    @apiName GetSingleInvoice
    @apiGroup Invoices
    @apiVersion 1.0.0

    @apiParam {Number} id Invoice unique ID.

    @apiSuccessExample {json} Success-Response:
    HTTP/1.1 200
    {
      "data": {
        "id": 2,
        "supplier_id": 1
      },
      "status": "OK"
    }

    @apiErrorExample {json} Error-Response:
    HTTP/1.1 204
    {
      "data": "There is no invoice with this id",
      "status": "ERROR"
    }
    """
    invoice = Invoice.get(id=int(id), args=request.args)
    if not invoice:
        return make_rest('ERROR', None, 204)

    return make_rest('OK', invoice, 200, include=request.args.get('include', None))
    
@mod.route('/<id>/', methods=['PUT'])
@login_required
def update_invoice(id):
    """
    @api {put} invoices/:id Updating an invoice
    @apiName UpdateSingleInvoice
    @apiGroup Invoices
    @apiVersion 1.0.0

    @apiParam {Number} id Invoice unique ID.
    @apiParam {Number} supplier_id Supplier unique ID.

    @apiSuccessExample {json} Success-Response:
    HTTP/1.1 200
    {
      "data": {
        "id": 2,
        "supplier_id": 1
      },
      "status": "OK"
    }

    @apiErrorExample {json} Error-Response:
    HTTP/1.1 204
    {
      "data": "There is no invoice with this id",
      "status": "ERROR"
    }
    """
    invoice = Invoice.get(id=int(id), args=request.args)
    if not invoice:
        return make_rest('ERROR', 'There is no invoice report with this id', 204)

    form = InvoiceForm(WtfFormData(request.json), obj=invoice)
    if form.validate():
        form.populate_obj(invoice)
        Invoice.update(invoice)

        return make_rest('OK', invoice, 200)

    return make_rest('ERROR', form.errors, 400)

@mod.route('/<id>/', methods=['DELETE'])
@login_required
def delete_invoice(id):
    """
    @api {delete} incoices/:id Deleting an invoice
    @apiName DeleteSingleInvoice
    @apiGroup Invoices
    @apiVersion 1.0.0

    @apiParam {Number} id Invoice unique ID.

    @apiSuccessExample {json} Success-Response:
    HTTP/1.1 200
    {
      "data": "Deleted id=2 invoice",
      "status": "OK"
    }

    @apiErrorExample {json} Error-Response:
    HTTP/1.1 204
    {
      "data": "There is no invoice with this id",
      "status": "ERROR"
    }
    """
    invoice = Invoice.get(id=int(id), args=request.args)
    if invoice:
        Invoice.remove(invoice)
        return make_rest('OK', 'Deleted id={0} invoice report'.format(id), 200)

    return make_rest('ERROR', 'There is no invoice report with this id', 204)
