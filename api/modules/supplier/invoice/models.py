from ....base.db import db, Timestamp, FSModel


class Invoice(db.Model, Timestamp, FSModel):
    id = db.Column(db.Integer, primary_key=True)
    # image_name
    supplier_id = db.Column(db.Integer, db.ForeignKey('supplier.id'))

    @property
    def v_map(self):
        return {
            'id': 'id',
            'supplier_id': 'supplier_id'
        }