from wtforms_alchemy import ModelForm
from .models import Supplier


class SupplierForm(ModelForm):
    class Meta:
        model = Supplier