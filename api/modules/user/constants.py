class Constants(object):
    _instance = None

    ADMIN = 'admin'
    OWNER = 'owner'
    MANAGER = 'manager'
    STAFF = 'staff'

    user_types = [ADMIN, OWNER, MANAGER, STAFF]

    def __new__(cls, *args, **kwargs):
        if not cls._instance:
            cls._instance = super(Constants, cls).__new__(cls, *args, **kwargs)

        return cls._instance