from sqlalchemy.dialects.postgres import ARRAY, JSONB

from ...base.db import db, Timestamp, FSModel
from ..restaurant.models import Restaurant
from ..notification.models import Notification


class User(db.Model, Timestamp, FSModel):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(32))
    phone = db.Column(db.String(16))

    email = db.Column(db.String(64))
    password = db.Column(db.String(64))  # TODO tobe md5 and set on form auth

    my_restaurant_id = db.Column(db.Integer, db.ForeignKey('restaurant.id'))
    restaurant_id = db.Column(db.Integer, db.ForeignKey('restaurant.id'))

    my_restaurant = db.relationship('Restaurant', foreign_keys=my_restaurant_id, backref='owner')
    restaurant = db.relationship('Restaurant', foreign_keys=restaurant_id, backref='staffs')

    notifications = db.relationship('Notification', backref='user')

    is_working = db.Column(db.Boolean, default=False)

    data = db.Column(JSONB())

    type = db.Column(db.String(20))

    def is_authenticated(self):
        return True

    def is_active(self):
        return True

    def is_anonymous(self):
        return False

    def get_id(self):
        return unicode(self.id)

    def get_param(self, var_name):
        return self.data.get(var_name, None)

    def set_param(self, var_name, var_val):
        if not self.data:
            self.data = {}
        self.data[var_name] = var_val

    __mapper_args__ = {
        'polymorphic_on': type,
        'polymorphic_identity': 'user'
    }

    @property
    def v_map(self):
        return {
            'id': 'id',
            'name': 'name',
            'phone': 'phone',
            'my_restaurant_id': 'my_restaurant_id',
            'my_restaurant': 'my_restaurant',
            'is_working': 'is_working',
            'type': 'type'
        }


class AdminUser(User):
    __mapper_args__ = {
        'polymorphic_identity': 'admin'
    }


class OwnerUser(User):
    __mapper_args__ = {
        'polymorphic_identity': 'owner'
    }


class ManagerUser(User):
    __mapper_args__ = {
        'polymorphic_identity': 'manager'
    }


class StaffUser(User):
    @property
    def t1(self):
        return self.get_param('t1')

    @t1.setter
    def t1(self, val):
        if not isinstance(val, bool):
            print 'ERROR:=> t1 not instance of bool and set False'
            self.set_param('t1', False)
        else:
            self.set_param('t1', val)

    @property
    def t2(self):
        return self.get_param('t2')

    @t2.setter
    def t2(self, val):
        if not isinstance(val, bool):
            print 'ERROR:=> t2 not instance of bool and set False'
            self.set_param('t2', False)
        else:
            self.set_param('t2', val)

    @property
    def t3(self):
        return self.get_param('t3')

    @t3.setter
    def t3(self, val):
        if not isinstance(val, bool):
            print 'ERROR:=> t3 not instance of bool and set False'
            self.set_param('t3', False)
        else:
            self.set_param('t3', val)

    @property
    def t4(self):
        return self.get_param('t4')

    @t4.setter
    def t4(self, val):
        if not isinstance(val, bool):
            print 'ERROR:=> t4 not instance of bool and set False'
            self.set_param('t4', False)
        else:
            self.set_param('t4', val)

    @property
    def t5(self):
        return self.get_param('t5')

    @t5.setter
    def t5(self, val):
        if not isinstance(val, bool):
            print 'ERROR:=> t5 not instance of bool and set False'
            self.set_param('t5', False)
        else:
            self.set_param('t5', val)

    @property
    def t6(self):
        return self.get_param('t6')

    @t6.setter
    def t6(self, val):
        if not isinstance(val, bool):
            print 'ERROR:=> t6 not instance of bool and set False'
            self.set_param('t6', False)
        else:
            self.set_param('t6', val)

    @property
    def t7(self):
        return self.get_param('t7')

    @t7.setter
    def t7(self, val):
        if not isinstance(val, bool):
            print 'ERROR:=> t7 not instance of bool and set False'
            self.set_param('t7', False)
        else:
            self.set_param('t7', val)

    @property
    def t8(self):
        return self.get_param('t8')

    @t8.setter
    def t8(self, val):
        if not isinstance(val, bool):
            print 'ERROR:=> t8 not instance of bool and set False'
            self.set_param('t8', False)
        else:
            self.set_param('t8', val)

    __mapper_args__ = {
        'polymorphic_identity': 'staff'
    }

    @property
    def v_map(self):
        temp = super(StaffUser, self).v_map
        temp['t1'] = 't1'
        temp['t2'] = 't2'
        temp['t3'] = 't3'
        temp['t4'] = 't4'
        temp['t5'] = 't5'
        temp['t6'] = 't6'
        temp['t7'] = 't7'
        temp['t8'] = 't8'

        return temp