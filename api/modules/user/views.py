from flask import render_template, redirect, url_for, request, flash
from flask.ext.login import login_required
from ...base.rest import make_rest
from ...base.wtform import WtfFormData
from .forms import RegistrationUserForm, RegistrationAdminForm, \
    RegistrationManagerForm, RegistrationOwnerForm, RegistrationStaffForm
from .models import User, ManagerUser, OwnerUser, StaffUser, AdminUser
from . import mod

# @mod.route('/', methods=['GET'])
# def index():
#     return render_template('user/register.html', extensions=None)


def get_correct_form_user(data):
    type = data.get('type', 'user')
    if type == 'admin':
        return RegistrationAdminForm(data=data), AdminUser()
    elif type == 'owner':
        return RegistrationOwnerForm(data=data), OwnerUser()
    elif type == 'manager':
        return RegistrationManagerForm(data=data), ManagerUser()
    elif type == 'staff':
        return RegistrationStaffForm(data=data), StaffUser()
    else:
        return RegistrationUserForm(data=data), User()


def get_correct_form_class(type='user'):
    if type == 'admin':
        return RegistrationAdminForm
    elif type == 'owner':
        return RegistrationOwnerForm
    elif type == 'manager':
        return RegistrationManagerForm
    elif type == 'staff':
        return RegistrationStaffForm
    else:
        return RegistrationUserForm


@mod.route('/', methods=['POST'])
@login_required
def register():
    """
    @api {post} users/ Creating a user
    @apiName CreateUser
    @apiGroup Users
    @apiVersion 1.0.0

    @apiParam {String{..32}} name User name.
    @apiParam {Number{..16}} phone User phone number.
    @apiParam {String{..64}} email User email.
    @apiParam {String{..64}} password User password.
    @apiParam {Number} my_restaurant_id User's restaurant ID.
    @apiParam {number} restaurant_id User restaurant ID.
    @apiParam {Boolean} is_working=False User restaurant status.
    @apiParam {String{..20}} type User type.  


    @apiSuccessExample {json} Success-Example:
    HTTP/1.1 200
    {
      "data": {
        "id": 1,
        "is_working": true,
        "my_restaurant": null,
        "my_restaurant_id": null,
        "name": "name_test",
        "phone": "phone_test",
        "t1": false,
        "t2": false,
        "t3": true,
        "t4": false,
        "t5": false,
        "t6": true,
        "t7": false,
        "t8": true,
        "type": "staff"
      },
      "status": "OK"
    }
    """
    form, user_obj = get_correct_form_user(data=request.json)
    if form.validate():
        user = user_obj
        form.populate_obj(user)
        user_obj.add(user)
        return make_rest('OK', user, 200)

    return make_rest('ERROR', form.errors, 400)
        # user = User(form.username.data, form.email.data,
        #             form.password.data)
        # db_session.add(user)
        # TODO => send email and email verfication
    #     flash('Thanks for registering')
    #     return redirect(url_for('login'))
    # return render_template('register.html', form=form)


@mod.route('/', methods=['GET'])
@login_required
def get_users():
    """
    @api {get} users/ Getting all users
    @apiName GetUser
    @apiGroup Users
    @apiVersion 1.0.0

    @apiSuccessExample {json} Success-Response:
    HTTP/1.1 200
    {
      "data": [
        {
          "id": 1,
          "is_working": true,
          "my_restaurant": null,
          "my_restaurant_id": null,
          "name": "name_test",
          "phone": "phone_test",
          "t1": false,
          "t2": false,
          "t3": true,
          "t4": false,
          "t5": false,
          "t6": true,
          "t7": false,
          "t8": true,
          "type": "staff"
        },
        {
          "id": 2,
          "is_working": true,
          "my_restaurant": null,
          "my_restaurant_id": null,
          "name": "name_test",
          "phone": "phone_test",
          "t1": false,
          "t2": false,
          "t3": true,
          "t4": false,
          "t5": false,
          "t6": true,
          "t7": false,
          "t8": true,
          "type": "staff"
        }
      ],
      "status": "OK"
    }
    """
    users = User.get(args=request.args)
    return make_rest('OK', users, 200, include=request.args.get('include', None))


@mod.route('/<id>/', methods=['GET'])
@login_required
def get_user(id):
    """
    @api {get} users/:id Getting a user
    @apiName GetSingleUser
    @apiGroup Users
    @apiVersion 1.0.0

    @apiParam {Number} id User unique ID.

    @apiSuccessExample {json} Success-Response:
    HTTP/1.1 200
    {
      "data": {
        "id": 4,
        "is_working": true,
        "my_restaurant": null,
        "my_restaurant_id": null,
        "name": "name_test",
        "phone": "phone_test",
        "t1": false,
        "t2": false,
        "t3": true,
        "t4": false,
        "t5": false,
        "t6": true,
        "t7": false,
        "t8": true,
        "type": "staff"
      },
      "status": "OK"
    }

    @apiErrorExample {json} Error-Response:
    HTTP/1.1 204
    {
      "data": "There is no user with this id",
      "status": "ERROR"
    }
    """
    user = User.get(id=int(id), args=request.args)
    if not user:
        return make_rest('ERROR', 'There is no user with this id', 204)

    return make_rest('OK', user, 200, include=request.args.get('include', None))
    
@mod.route('/<id>/', methods=['PUT'])
@login_required
def update_user(id):
    """
    @api {put} users/:id Updating a user
    @apiName UpdateSingleUser
    @apiGroup Users
    @apiVersion 1.0.0

    @apiParam {Number} id Notification unique ID.
    @apiParam {String{..32}} name User name.
    @apiParam {Number{..16}} phone User phone number.
    @apiParam {String{..64}} email User email.
    @apiParam {String{..64}} password User password.
    @apiParam {Number} my_restaurant_id User's restaurant ID.
    @apiParam {number} restaurant_id User restaurant ID.
    @apiParam {Boolean} is_working=False User restaurant status.
    @apiParam {String{..20}} type User type.

    @apiSuccessExample {json} Success-Response:
    HTTP/1.1 200
    {
      "data": {
        "id": 4,
        "is_working": true,
        "my_restaurant": null,
        "my_restaurant_id": null,
        "name": "name_test",
        "phone": "phone_test",
        "t1": false,
        "t2": false,
        "t3": true,
        "t4": false,
        "t5": false,
        "t6": true,
        "t7": false,
        "t8": true,
        "type": "staff"
      },
      "status": "OK"
    }

    @apiErrorExample {json} Error-Response:
    HTTP/1.1 204
    {
      "data": "There is no user with this id",
      "status": "ERROR"
    }
    """
    user = User.get(id=int(id), args=request.args)
    if not user:
        return make_rest('ERROR', 'There is no user with this id', 204)

    # TODO => change user will be some data before null and set new needed data
    form = get_correct_form_class(request.json.get('type', 'user'))(WtfFormData(request.json), obj=user)
    if form.validate():
        form.populate_obj(user)
        user.update(user)

        return make_rest('OK', user, 200)

    return make_rest('ERROR', form.errors, 400)

@mod.route('/<id>/', methods=['DELETE'])
@login_required
def delete_user(id):
    """
    @api {delete} users/:id Deleting a user
    @apiName DeleteSingleUser
    @apiGroup Users
    @apiVersion 1.0.0

    @apiParam {Number} id User unique ID.

    @apiSuccessExample {json} Success-Response:
    HTTP/1.1 200
    {
      "data": "Deleted id=2 user",
      "status": "OK"
    }

    @apiErrorExample {json} Error-Response:
    HTTP/1.1 204
    {
      "data": "There is no user with this id",
      "status": "ERROR"
    }
    """
    user = User.get(id=int(id), args=request.args)
    if user:
        User.remove(user)
        return make_rest('OK', 'Deleted id={0} user'.format(id), 200)

    return make_rest('ERROR', 'There is no user with this id', 204)
