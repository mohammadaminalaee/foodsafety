from flask import Blueprint
mod = Blueprint('users', __name__, url_prefix='/api/v1/users')
from .views import *
