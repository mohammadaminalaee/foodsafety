from wtforms_alchemy import ModelForm
from .models import User, AdminUser, ManagerUser, OwnerUser, StaffUser
from wtforms import BooleanField

class RegistrationUserForm(ModelForm):
    class Meta:
        model = User
        exclude = ['data']


class RegistrationAdminForm(ModelForm):
    class Meta:
        model = AdminUser
        exclude = ['data']


class RegistrationOwnerForm(ModelForm):
    class Meta:
        model = OwnerUser
        exclude = ['data']


class RegistrationManagerForm(ModelForm):
    class Meta:
        model = ManagerUser
        exclude = ['data']


class RegistrationStaffForm(ModelForm):
    class Meta:
        model = StaffUser
        exclude = ['data']

    t1 = BooleanField()
    t2 = BooleanField()
    t3 = BooleanField()
    t4 = BooleanField()
    t5 = BooleanField()
    t6 = BooleanField()
    t7 = BooleanField()
    t8 = BooleanField()