from wtforms_alchemy import ModelForm
from .models import Service


class ServiceForm(ModelForm):
    class Meta:
        model = Service