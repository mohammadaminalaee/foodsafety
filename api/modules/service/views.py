from flask import request
from flask.ext.login import login_required
from ...base.rest import make_rest
from ...base.wtform import WtfFormData
from .forms import Service, ServiceForm
from . import mod


@mod.route('/', methods=['POST'])
@login_required
def create_service():
    """
    @api {post} services/ Creating a service
    @apiName CreateService
    @apiGroup Services
    @apiVersion 1.0.0

    @apiParam {String} name Service name.
    @apiParam {String} service_type Service type.
    @apiParam {Number} restaurant_id Restaurant unique ID.

    @apiSuccessExample {json} Success-Response:
    HTTP/1.1 200
    {
        "data": {
              "id": 2,
              "name": "service_name",
              "service_type": "personal",
              "restaurant_id": 1
          },
        "status": "OK"
    }
    """
    form = ServiceForm(data=request.json)
    if form.validate():
        service = Service()
        form.populate_obj(service)
        Service.add(service)

        return make_rest('OK', service, 200)

    return make_rest('ERROR', form.errors, 400)

@mod.route('/', methods=['GET'])
@login_required
def get_services():
    """
    @api {get} services/ Getting all services
    @apiName GetServices
    @apiGroup Services
    @apiVersion 1.0.0

    @apiSuccessExample {json} Success-Response:
    HTTP/1.1 200
    {
        "data": [
            {
              "id": 2,
              "name": "service_name",
              "service_type": "personal",
              "restaurant_id": 1
            },
            {
              "id": 3,
              "name": "service_name",
              "service_type": "personal",
              "restaurant_id": 1
            }
        ],
        "status": "OK"
    }
    """
    services = Service.get(args=request.args)
    return make_rest('OK', services, 200, include=request.args.get('include', None))


@mod.route('/<id>/', methods=['GET'])
@login_required
def get_service(id):
    """
    @api {get} services/:id Getting a service
    @apiName GetSingleService
    @apiGroup Services
    @apiVersion 1.0.0

    @apiParam {Number} id Service unique ID.

    @apiSuccessExample {json} Success-Response:
    HTTP/1.1 200
    {
      "data": {
        "id": 2,
        "name": "service_name",
        "service_type": "personal",
        "restaurant_id": 1
      },
      "status": "OK"
    }

    @apiErrorExample {json} Error-Response:
    HTTP/1.1 204
    {
      "data": "There is no service with this id",
      "status": "ERROR"
    }
    """
    service = Service.get(id=int(id), args=request.args)
    if not service:
        return make_rest('ERROR', None, 204)

    return make_rest('OK', service, 200, include=request.args.get('include', None))
    
@mod.route('/<id>/', methods=['PUT'])
@login_required
def update_service(id):
    """
    @api {put} services/:id Updating a service
    @apiName UpdateSingleService
    @apiGroup Services
    @apiVersion 1.0.0

    @apiParam {Number} id Service unique ID.
    @apiParam {String} name Service name.
    @apiParam {String} service_type Service type.
    @apiParam {Number} restaurant_id Restaurant unique ID.

    @apiSuccessExample {json} Success-Response:
    HTTP/1.1 200
    {
      "data": {
        "id": 2,
        "name": "service_name",
        "service_type": "personal",
        "restaurant_id": 1
      },
      "status": "OK"
    }

    @apiErrorExample {json} Error-Response:
    HTTP/1.1 204
    {
      "data": "There is no service with this id",
      "status": "ERROR"
    }
    """
    service = Service.get(id=int(id), args=request.args)
    if not service:
        return make_rest('ERROR', 'There is no service report with this id', 204)

    form = ServiceForm(WtfFormData(request.json), obj=service)
    if form.validate():
        form.populate_obj(service)
        Service.update(service)

        return make_rest('OK', service, 200)

    return make_rest('ERROR', form.errors, 400)

@mod.route('/<id>/', methods=['DELETE'])
@login_required
def delete_service(id):
    """
    @api {delete} services/:id Deleting a service
    @apiName DeleteSingleService
    @apiGroup Services
    @apiVersion 1.0.0

    @apiParam {Number} id Service unique ID.

    @apiSuccessExample {json} Success-Response:
    HTTP/1.1 200
    {
      "data": "Deleted id=2 service",
      "status": "OK"
    }

    @apiErrorExample {json} Error-Response:
    HTTP/1.1 204
    {
      "data": "There is no service with this id",
      "status": "ERROR"
    }
    """
    service = Service.get(id=int(id), args=request.args)
    if service:
        Service.remove(service)
        return make_rest('OK', 'Deleted id={0} service report'.format(id), 200)

    return make_rest('ERROR', 'There is no service report with this id', 204)
