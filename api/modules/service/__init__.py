from flask import Blueprint
mod = Blueprint('service', __name__, url_prefix='/api/v1/services')
from .views import *
