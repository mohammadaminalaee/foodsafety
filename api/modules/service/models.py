from sqlalchemy.dialects.postgres import ENUM

from ...base.db import db, Timestamp, FSModel
from .constants import Constants

service_type_enum = ENUM(*Constants.service_types, name='service_type_enum')


class Service(db.Model, Timestamp, FSModel):
    id = db.Column(db.Integer, primary_key=True)

    name = db.Column(db.String)
    service_type = db.Column(service_type_enum)

    restaurant_id = db.Column(db.Integer, db.ForeignKey('restaurant.id'))

    @property
    def v_map(self):
        return {
            'id': 'id',
            'name': 'name',
            'service_type': 'service_type',
            'restaurant_id': 'restaurant_id'
        }