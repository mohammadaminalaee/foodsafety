from flask import Blueprint
mod = Blueprint('restaurant', __name__, url_prefix='/api/v1/restaurants')
from .views import *

