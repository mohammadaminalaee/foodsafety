from ...base.db import db, Timestamp, FSModel

restaurant_supplier_table = db.Table('restaurant_supplier', db.metadata,
                                     db.Column('restaurant_id', db.Integer, db.ForeignKey('restaurant.id')),
                                     db.Column('supplier_id', db.Integer, db.ForeignKey('supplier.id'))
                                     )

from .certificate.models import Certificate
from ..task.models import Task
from ..service.models import Service
from ..supplier.models import Supplier
from ..instrument.models import Instrument
from ..material.models import Material
from ..food.models import Food


class Restaurant(db.Model, Timestamp, FSModel):
    id = db.Column(db.Integer, primary_key=True)
    # backref => owner
    # backref => staffs

    certificates = db.relationship('Certificate', backref='restaurant')
    instruments = db.relationship('Instrument', backref='restaurant')
    materials = db.relationship('Material', backref='restaurant')
    foods = db.relationship('Food', backref='restaurant')
    tasks = db.relationship('Task', backref='restaurant')

    services = db.relationship('Service', backref='restaurant')
    suppliers = db.relationship('Supplier', secondary=restaurant_supplier_table, backref='restaurants')

    @property
    def v_map(self):
        return {
            'id': 'id',
            'owner_id': 'owner.id'
        }