from wtforms_alchemy import ModelForm
from .models import Restaurant


class RestaurantForm(ModelForm):
    class Meta:
        model = Restaurant