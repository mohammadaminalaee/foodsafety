from flask import request
from flask.ext.login import login_required
from ...base.rest import make_rest
from ...base.wtform import WtfFormData
from .forms import Restaurant, RestaurantForm
from . import mod


@mod.route('/', methods=['POST'])
@login_required
def create_restaurant():
    form = RestaurantForm(data=request.json)
    if form.validate():
        restaurant = Restaurant()
        form.populate_obj(restaurant)
        Restaurant.add(restaurant)

        return make_rest('OK', restaurant, 200)

    return make_rest('ERROR', form.errors, 400)

@mod.route('/', methods=['GET'])
@login_required
def get_restaurants():
    """
    @api {get} restaurants/ Getting all restaurants
    @apiName GetRestaurants
    @apiGroup Restaurants
    @apiVersion 1.0.0

    @apiSuccessExample {json} Success-Response:
    HTTP/1.1 200
    {
      "data": [
        {
          "id": 2
        },
        {
          "id": 3
        }
      ],
      "status": "OK"
    }
    """
    restaurants = Restaurant.get(args=request.args)
    return make_rest('OK', restaurants, 200, include=request.args.get('include', None))

@mod.route('/<id>/', methods=['GET'])
@login_required
def get_restaurant(id):
    """
    @api {get} restaurants/:id Getting a restaurant
    @apiName GetSingleRestaurant
    @apiGroup Restaurants
    @apiVersion 1.0.0

    @apiParam {Number} id Restaurant unique ID.

    @apiSuccessExample {json} Success-Response:
    HTTP/1.1 200
    {
      "data": {
            "id": 2
        },
      "status": "OK"
    }

    @apiErrorExample {json} Error-Response:
    HTTP/1.1 204
    {
      "data": "There is no restaurant with this id",
      "status": "ERROR"
    }
    """
    restaurant = Restaurant.get(id=int(id), args=request.args)
    if not restaurant:
        return make_rest('ERROR', None, 204)

    return make_rest('OK', restaurant, 200, include=request.args.get('include', None))

@mod.route('/<id>/', methods=['PUT'])
@login_required
def update_restaurant(id):
    restaurant = Restaurant.get(id=int(id), args=request.args)
    if not restaurant:
        return make_rest('ERROR', 'There is no restaurant report with this id', 204)

    form = RestaurantForm(WtfFormData(request.json), obj=restaurant)
    if form.validate():
        form.populate_obj(restaurant)
        Restaurant.update(restaurant)

        return make_rest('OK', restaurant, 200)

    return make_rest('ERROR', form.errors, 400)

@mod.route('/<id>/', methods=['DELETE'])
@login_required
def delete_restaurant(id):
    """
    @api {delete} restaurants/:id Deleting a restaurant
    @apiName DeleteSingleRestaurant
    @apiGroup Restaurants
    @apiVersion 1.0.0

    @apiParam {Number} id Restaurant unique ID.

    @apiSuccessExample {json} Success-Response:
    HTTP/1.1 200
    {
      "data": "Deleted id=2 restaurant",
      "status": "OK"
    }

    @apiErrorExample {json} Error-Response:
    HTTP/1.1 204
    {
      "data": "There is no restaurant with this id",
      "status": "ERROR"
    }
    """
    restaurant = Restaurant.get(id=int(id), args=request.args)
    if restaurant:
        Restaurant.remove(restaurant)
        return make_rest('OK', 'Deleted id={0} restaurant report'.format(id), 200)
    return make_rest('ERROR', 'There is no restaurant report with this id', 204)
