from wtforms_alchemy import ModelForm
from .models import Certificate


class CertificateForm(ModelForm):
    class Meta:
        model = Certificate