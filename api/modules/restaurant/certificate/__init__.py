from flask import Blueprint
mod = Blueprint('certificate', __name__, url_prefix='/api/v1/certificates')
from .views import *
