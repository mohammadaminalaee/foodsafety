from flask import request
from flask.ext.login import login_required
from ....base.rest import make_rest
from ....base.wtform import WtfFormData
from .forms import Certificate, CertificateForm
from . import mod


@mod.route('/', methods=['POST'])
@login_required
def create_certificate():
    """
    @api {post} certificates/ Creating a certificate
    @apiName CreateCertificate
    @apiGroup Certificates
    @apiVersion 1.0.0

    @apiParam {String} img_file_location Certificate file location.
    @apiParam {Number} restaurant_id Restaurant unique ID.

    @apiSuccessExample {json} Success-Response:
    HTTP/1.1 200
    {
      "data": {
            "id": 4,
            "img_file_location": "img_file_location",
            "restaurant_id": null
      },
      "status": "OK"
    }
    """
    form = CertificateForm(data=request.json)
    if form.validate():
        certificate = Certificate()
        form.populate_obj(certificate)
        Certificate.add(certificate)

        return make_rest('OK', certificate, 200)

    return make_rest('ERROR', form.errors, 400)

@mod.route('/', methods=['GET'])
@login_required
def get_certificates():
    """
    @api {get} certificates/ Getting all certificates
    @apiName GetCertificates
    @apiGroup Certificates
    @apiVersion 1.0.0

    @apiSuccessExample {json} Success-Response:
    HTTP/1.1 200
    {
      "data": [
        {
          "id": 1,
          "img_file_location": "img_file_location",
          "restaurant_id": null
        },
        {
          "id": 2,
          "img_file_location": "img_file_location",
          "restaurant_id": null
        }
      "status": "OK"
    }
    """
    certificates = Certificate.get(args=request.args)
    return make_rest('OK', certificates, 200, include=request.args.get('include', None))


@mod.route('/<id>/', methods=['GET'])
@login_required
def get_certificate(id):
    """
    @api {get} certificates/:id Getting a certificate
    @apiName GetSingleCertificate
    @apiGroup Certificates
    @apiVersion 1.0.0

    @apiParam {Number} id Certificate unique ID.

    @apiSuccessExample {json} Success-Response:
    HTTP/1.1 200
    {
      "data": {
          "id": 1,
          "img_file_location": "img_file_location",
          "restaurant_id": null
        },
      "status": "OK"
    }

    @apiErrorExample {json} Error-Response:
    HTTP/1.1 204
    {
      "data": "There is no certificate with this id",
      "status": "ERROR"
    }
    """
    certificate = Certificate.get(id=int(id), args=request.args)
    if not certificate:
        return make_rest('ERROR', 'There is no certificate with this id', 204)

    return make_rest('OK', certificate, 200, include=request.args.get('include', None))

@mod.route('/<id>/', methods=['PUT'])
@login_required
def update_certificate(id):
    """
    @api {put} certificates/:id Updating a certificate
    @apiName UpdateSingleCertificate
    @apiGroup Certificates
    @apiVersion 1.0.0

    @apiParam {Number} id Certiciate unique ID.
    @apiParam {String} img_file_location Certificate img_file_location.
    @apiParam {Number} restaurant_id Restaurant unique ID.

    @apiSuccessExample {json} Success-Response:
    HTTP/1.1 200
    {
      "data": {
            "id": 1,
            "img_file_location": "img_file_location",
            "restaurant_id": null
      },
      "status": "OK"
    }

    @apiErrorExample {json} Error-Response:
    HTTP/1.1 204
    {
      "data": "There is no ceriticate with this id",
      "status": "ERROR"
    }
    """
    certificate = Certificate.get(id=int(id), args=request.args)
    if not certificate:
        return make_rest('ERROR', 'There is no certificate report with this id', 204)

    form = CertificateForm(WtfFormData(request.json), obj=certificate)
    if form.validate():
        form.populate_obj(certificate)
        Certificate.update(certificate)

        return make_rest('OK', certificate, 200)

    return make_rest('ERROR', form.errors, 400)

@mod.route('/<id>/', methods=['DELETE'])
@login_required
def delete_certificate(id):
    """
    @api {delete} certificates/:id Deleting a certificate
    @apiName DeleteSingleCertificate
    @apiGroup Certificates
    @apiVersion 1.0.0

    @apiParam {Number} id Ceriticate unique ID.

    @apiSuccessExample {json} Success-Response:
    HTTP/1.1 200
    {
      "data": "Deleted id=2 certificate",
      "status": "OK"
    }

    @apiErrorExample {json} Error-Response:
    HTTP/1.1 204
    {
      "data": "There is no certificate with this id",
      "status": "ERROR"
    }
    """
    certificate = Certificate.get(id=int(id), args=request.args)
    if certificate:
        Certificate.remove(certificate)
        return make_rest('OK', 'Deleted id={0} certificate report'.format(id), 200)
    return make_rest('ERROR', 'There is no certificate report with this id', 204)
