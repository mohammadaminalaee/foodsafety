from ....base.db import db, Timestamp, FSModel


class Certificate(db.Model, Timestamp, FSModel):
    id = db.Column(db.Integer, primary_key=True)
    img_file_location = db.Column(db.String)
    restaurant_id = db.Column(db.Integer, db.ForeignKey('restaurant.id'))

    @property
    def v_map(self):
        return {
            'id': 'id',
            'img_file_location': 'img_file_location',
            'restaurant_id': 'restaurant_id'
        }