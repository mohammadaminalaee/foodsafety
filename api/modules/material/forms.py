from wtforms_alchemy import ModelForm
from .models import Material


class MaterialForm(ModelForm):
    class Meta:
        model = Material