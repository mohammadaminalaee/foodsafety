from flask import request
from flask.ext.login import login_required
from ...base.rest import make_rest
from ...base.wtform import WtfFormData
from .forms import Material, MaterialForm
from . import mod


@mod.route('/', methods=['POST'])
@login_required
def create_material():
    """
    @api {post} materials/ Creating a material
    @apiName CreateMaterial
    @apiGroup Materials
    @apiVersion 1.0.0

    @apiParam {String} name Material name.
    @apiParam {Number} restaurant_id Restaurant unique ID.

    @apiSuccessExample {json} Success-Example:
    HTTP/1.1 200
    {
      "data": {
        "id": 4,
        "name": "material_name",
        "restaurant_id": 1
      },
      "status": "OK"
    }
    """
    form = MaterialForm(data=request.json)
    if form.validate():
        material = Material()
        form.populate_obj(material)
        Material.add(material)

        return make_rest('OK', material, 200)

    return make_rest('ERROR', form.errors, 400)

@mod.route('/', methods=['GET'])
@login_required
def get_materials():
    """
    @api {get} materials/ Getting all materials
    @apiName GetMaterial
    @apiGroup Materials
    @apiVersion 1.0.0

    @apiSuccessExample {json} Success-Response:
    HTTP/1.1 200
    {
      "data": [
        {
          "id": 1,
          "name": "material_name",
          "restaurant_id": 1
        },
        {
          "id": 2,
          "name": "material_name",
          "restaurant_id": 1
        }
      ],
      "status": "OK"
    }
    """
    materials = Material.get(args=request.args)
    return make_rest('OK', materials, 200, include=request.args.get('include', None))


@mod.route('/<id>/', methods=['GET'])
@login_required
def get_material(id):
    """
    @api {get} materials/:id Getting a material
    @apiName GetSingleMaterial
    @apiGroup Materials
    @apiVersion 1.0.0

    @apiParam {Number} id Material unique ID.

    @apiSuccessExample {json} Success-Response:
    HTTP/1.1 200
    {
      "data": {
        "id": 1,
        "name": "material_name",
        "restaurant_id": 1
      },
      "status": "OK"
    }

    @apiErrorExample {json} Error-Response:
    HTTP/1.1 204
    {
      "data": "There is no material with this id",
      "status": "ERROR"
    }
    """
    material = Material.get(id=int(id), args=request.args)
    if not material:
        return make_rest('ERROR', 'There is no material with this id', 204)

    return make_rest('OK', material, 200, include=request.args.get('include', None))
    
@mod.route('/<id>/', methods=['PUT'])
@login_required
def update_material(id):
    """
    @api {put} materials/:id Updating a material
    @apiName UpdateSingleMaterial
    @apiGroup Materials
    @apiVersion 1.0.0

    @apiParam {Number} id Material unique ID.
    @apiParam {String} name Material name.
    @apiParam {Number} restaurant_id Restaurant unique ID.

    @apiSuccessExample {json} Success-Response:
    HTTP/1.1 200
    {
      "data": {
        "id": 2,
        "name": "material_name",
        "restaurant_id": 1
      },
      "status": "OK"
    }

    @apiErrorExample {json} Error-Response:
    HTTP/1.1 204
    {
      "data": "There is no material with this id",
      "status": "ERROR"
    }
    """
    material = Material.get(id=int(id), args=request.args)
    if not material:
        return make_rest('ERROR', 'There is no material with this id', 204)

    form = MaterialForm(WtfFormData(request.json), obj=material)
    if form.validate():
        form.populate_obj(material)
        Material.update(material)

        return make_rest('OK', material, 200)

    return make_rest('ERROR', form.errors, 400)
    
@mod.route('/<id>/', methods=['DELETE'])
@login_required
def delete_material(id):
    """
    @api {delete} materials/:id Deleting a material
    @apiName DeleteSingleMaterial
    @apiGroup Materials
    @apiVersion 1.0.0

    @apiParam {Number} id Material unique ID.

    @apiSuccessExample {json} Success-Response:
    HTTP/1.1 200
    {
      "data": "Deleted id=2 material",
      "status": "OK"
    }

    @apiErrorExample {json} Error-Response:
    HTTP/1.1 204
    {
      "data": "There is no material with this id",
      "status": "ERROR"
    }
    """
    material = Material.get(id=int(id), args=request.args)
    if material:
        Material.remove(material)
        return make_rest('OK', 'Deleted id={0} material'.format(id), 200)
    
    return make_rest('ERROR', 'There is not any material with this id', 204)
