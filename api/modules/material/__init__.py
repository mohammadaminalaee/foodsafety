from flask import Blueprint
mod = Blueprint('material', __name__, url_prefix='/api/v1/materials')
from .views import *
