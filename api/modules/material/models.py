from ...base.db import db, Timestamp, FSModel


class Material(db.Model, Timestamp, FSModel):
    id = db.Column(db.Integer, primary_key=True)

    # saboon, hole, hame chiz be joz khordani ha
    name = db.Column(db.String)
    restaurant_id = db.Column(db.Integer, db.ForeignKey('restaurant.id'))

    @property
    def v_map(self):
        return {
            'id': 'id',
            'name': 'name',
            'restaurant_id': 'restaurant_id'
        }