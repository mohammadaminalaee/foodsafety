from flask import request
from flask.ext.login import login_required
from ...base.rest import make_rest
from ...base.wtform import WtfFormData
from .forms import Notification, NotificationForm
from . import mod


@mod.route('/', methods=['POST'])
@login_required
def create_notification():
    """
    @api {post} notifications/ Creating a notification
    @apiName CreateNotification
    @apiGroup Notifications
    @apiVersion 1.0.0

    @apiParam {String} message Notification message.
    @apiParam {Number} user_id User unique ID.

    @apiSuccessExample {json} Success-Example:
    HTTP/1.1 200
    {
      "data": {
        "id": 1,
        "message": "message_text",
        "user_id": 1
      },
      "status": "OK"
    }
    """
    form = NotificationForm(data=request.json)
    if form.validate():
        notification = Notification()
        form.populate_obj(notification)
        Notification.add(notification)

        return make_rest('OK', notification, 200)

    return make_rest('ERROR', form.errors, 400)

@mod.route('/', methods=['GET'])
@login_required
def get_notifications():
    """
    @api {get} notifications/ Getting all notifications
    @apiName GetNotification
    @apiGroup Notifications
    @apiVersion 1.0.0

    @apiSuccessExample {json} Success-Response:
    HTTP/1.1 200
    {
      "data": [
        {
          "id": 1,
            "message": "message_text",
            "user_id": 1
        },
        {
          "id": 2,
            "message": "message_text",
            "user_id": 1
        }
      ],
      "status": "OK"
    }
    """
    notifications = Notification.get(args=request.args)
    return make_rest('OK', notifications, 200, include=request.args.get('include', None))

@mod.route('/<id>/', methods=['GET'])
@login_required
def get_notification(id):
    """
    @api {get} notifications/:id Getting a notification
    @apiName GetSingleNotification
    @apiGroup Notifications
    @apiVersion 1.0.0

    @apiParam {Number} id Notification unique ID.

    @apiSuccessExample {json} Success-Response:
    HTTP/1.1 200
    {
      "data": {
        "id": 1,
        "message": "message_text",
        "user_id": 1
      },
      "status": "OK"
    }

    @apiErrorExample {json} Error-Response:
    HTTP/1.1 204
    {
      "data": "There is no notification with this id",
      "status": "ERROR"
    }
    """
    notification = Notification.get(id=int(id), args=request.args)
    if request.method == 'GET':
        if not notification:
            return make_rest('ERROR', 'There is no notification with this id', 204)

        return make_rest('OK', notification, 200, include=request.args.get('include', None))

@mod.route('/<id>/', methods=['PUT'])
@login_required
def update_notification(id):
    """
    @api {put} notifications/:id Updating a notification
    @apiName UpdateSingleNotification
    @apiGroup Notifications
    @apiVersion 1.0.0

    @apiParam {Number} id Notification unique ID.
    @apiParam {String} message Notification message.
    @apiParam {Number} user_id User unique ID.

    @apiSuccessExample {json} Success-Response:
    HTTP/1.1 200
    {
      "data": {
        "id": 2,
        "message": "message_text",
        "user_id": 1
      },
      "status": "OK"
    }

    @apiErrorExample {json} Error-Response:
    HTTP/1.1 204
    {
      "data": "There is no notification with this id",
      "status": "ERROR"
    }
    """
    notification = Notification.get(id=int(id), args=request.args)
    if not notification:
        return make_rest('ERROR', 'There is no notification with this id', 204)

    form = NotificationForm(WtfFormData(request.json), obj=notification)
    if form.validate():
        form.populate_obj(notification)
        Notification.update(notification)

        return make_rest('OK', notification, 200)

    return make_rest('ERROR', form.errors, 400)

@mod.route('/<id>/', methods=['DELETE'])
@login_required
def delete_notification(id):
    """
    @api {delete} notifications/:id Deleting a notification
    @apiName DeleteSingleNotification
    @apiGroup Notifications
    @apiVersion 1.0.0

    @apiParam {Number} id Notification unique ID.

    @apiSuccessExample {json} Success-Response:
    HTTP/1.1 200
    {
      "data": "Deleted id=2 notification",
      "status": "OK"
    }

    @apiErrorExample {json} Error-Response:
    HTTP/1.1 204
    {
      "data": "There is no notification with this id",
      "status": "ERROR"
    }
    """
    notification = Notification.get(id=int(id), args=request.args)
    if notification:
        Notification.remove(notification)
        return make_rest('OK', 'Deleted id={0} notification'.format(id), 200)

    return make_rest('ERROR', 'There is no notification with this id', 204)
