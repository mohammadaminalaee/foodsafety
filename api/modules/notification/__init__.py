from flask import Blueprint
mod = Blueprint('notification', __name__, url_prefix='/api/v1/notifications')
from .views import *
