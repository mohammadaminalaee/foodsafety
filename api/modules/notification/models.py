from ...base.db import db, Timestamp, FSModel


class Notification(db.Model, Timestamp, FSModel):
    id = db.Column(db.Integer, primary_key=True)

    message = db.Column(db.String)

    #backref => user
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))

    @property
    def v_map(self):
        return {
            'id': 'id',
            'message': 'message',
            'user_id': 'user_id'
        }