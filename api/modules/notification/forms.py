from wtforms_alchemy import ModelForm
from .models import Notification


class NotificationForm(ModelForm):
    class Meta:
        model = Notification