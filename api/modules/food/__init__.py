from flask import Blueprint
mod = Blueprint('food', __name__, url_prefix='/api/v1/foods')
from .views import *
