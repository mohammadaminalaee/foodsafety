from wtforms_alchemy import ModelForm
# from wtforms import Form
from .models import Food


class FoodForm(ModelForm):
    class Meta:
        model = Food