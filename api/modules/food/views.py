from flask import request
from flask.ext.login import login_required
from ...base.rest import make_rest
from ...base.wtform import WtfFormData
# from .models import Food
from .forms import FoodForm, Food
from . import mod


@mod.route('/', methods=['POST'])
@login_required
def create_food():
    """
    @api {post} foods/ Creating a food
    @apiName CreateFood
    @apiGroup Foods
    @apiVersion 1.0.0

    @apiParam {String} name Food name.
    @apiParam {Number} restaurant_id Restaurant unique ID.

    @apiSuccessExample {json} Success-Response:
    HTTP/1.1 200
    {
        "data": {
            "id": 7,
            "name": "food_name",
            "restaurant_id": 1
          },
        "status": "OK"
    }
    """
    form = FoodForm(data=request.json)
    if form.validate():
        food = Food()
        form.populate_obj(food)
        Food.add(food)
        return make_rest('OK', food, 200)
    return make_rest('ERROR', form.errors, 400)

@mod.route('/', methods=['GET'])
@login_required
def get_foods():
    """
    @api {get} foods/ Getting all foods
    @apiName GetFoods
    @apiGroup Foods
    @apiVersion 1.0.0

    @apiSuccessExample {json} Success-Response:
    HTTP/1.1 200
    {
        "data": [
            {
              "id": 2,
              "name": "food_name",
              "restaurant_id": 1
            },
            {
              "id": 3,
              "name": "food_name",
              "restaurant_id": 1
            }
        ],
        "status": "OK"
    }
    """
    foods = Food.get(args=request.args)
    return make_rest('OK', foods, 200, include=request.args.get('include', None))


@mod.route('/<id>/', methods=['GET'])
@login_required
def get_food(id):
    """
    @api {get} foods/:id Getting a food
    @apiName GetSingleFood
    @apiGroup Foods
    @apiVersion 1.0.0

    @apiParam {Number} id Food unique ID.

    @apiSuccessExample {json} Success-Response:
    HTTP/1.1 200
    {
      "data": {
        "id": 2,
        "name": "food_name",
        "restaurant_id": 1
      },
      "status": "OK"
    }

    @apiErrorExample {json} Error-Response:
    HTTP/1.1 204
    {
      "data": "There is no food with this id",
      "status": "ERROR"
    }
    """
    food = Food.get(id=int(id), args=request.args)
    if not food:
        return make_rest('ERROR', None, 204)

    return make_rest('OK', food, 200, include=request.args.get('include', None))
    
@mod.route('/<id>/', methods=['PUT'])
def update_food(id):
    """
    @api {put} foods/:id Updating a food
    @apiName UpdateSingleFood
    @apiGroup Foods
    @apiVersion 1.0.0

    @apiParam {Number} id Food unique ID.
    @apiParam {String} name Food name.
    @apiParam {Number} restaurant_id Restaurant unique ID.

    @apiSuccessExample {json} Success-Response:
    HTTP/1.1 200
    {
      "data": {
        "id": 2,
        "name": "food_name",
        "restaurant_id": 1
      },
      "status": "OK"
    }

    @apiErrorExample {json} Error-Response:
    HTTP/1.1 204
    {
      "data": "There is no food with this id",
      "status": "ERROR"
    }
    """
    food = Food.get(id=int(id), args=request.args)
    if not food:
        return make_rest('ERROR', 'There is no food with this id', 204)

    form = FoodForm(WtfFormData(request.json), obj=food)
    if form.validate():
        form.populate_obj(food)
        Food.update(food)

        return make_rest('OK', food, 200)

    return make_rest('ERROR', form.errors, 400)

@mod.route('/<id>/', methods=['DELETE'])
def delete_food(id):
    """
    @api {delete} foods/:id Deleting a food
    @apiName DeleteSingleFood
    @apiGroup Foods
    @apiVersion 1.0.0

    @apiParam {Number} id Food unique ID.

    @apiSuccessExample {json} Success-Response:
    HTTP/1.1 200
    {
      "data": "Deleted id=2 food",
      "status": "OK"
    }

    @apiErrorExample {json} Error-Response:
    HTTP/1.1 204
    {
      "data": "There is no food with this id",
      "status": "ERROR"
    }
    """
    food = Food.get(id=int(id), args=request.args)
    if food:
        Food.remove(food)
        return make_rest('OK', 'Deleted id={0} food'.format(id), 200)
    
    return make_rest('ERROR', 'There is no food with this id', 204)
