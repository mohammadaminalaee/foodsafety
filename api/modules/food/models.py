from ...base.db import db, Timestamp, FSModel


class Food(db.Model, Timestamp, FSModel):
    id = db.Column(db.Integer, primary_key=True)

    # gosht, morgh, ...
    name = db.Column(db.String)
    # backref => restaurant
    restaurant_id = db.Column(db.Integer, db.ForeignKey('restaurant.id'))

    @property
    def v_map(self):
        return {
            'id': 'id',
            'name': 'name',
            'restaurant_id': 'restaurant_id'
        }