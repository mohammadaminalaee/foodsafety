from flask import render_template, redirect, url_for, request, flash
from flask.ext.login import login_required
from . import mod


@mod.route('/', methods=['GET'])
def home():
    return render_template('dashboard/home.html')
