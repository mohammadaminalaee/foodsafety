from flask import Blueprint
mod = Blueprint('instrument', __name__, url_prefix='/api/v1/instruments')
from .views import *
