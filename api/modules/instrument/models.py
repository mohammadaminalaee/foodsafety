# from sqlalchemy.dialects.postgres import ENUM

from ...base.db import db, Timestamp, FSModel

# from .constants import Constants
# instrument_type_enum = ENUM(*Constants.instrument_types, name='instrument_type_enum')


class Instrument(db.Model, Timestamp, FSModel):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String)
    is_working = db.Column(db.Boolean, default=True)

    barcode_id = db.Column(db.Integer, default=0)
    restaurant_id = db.Column(db.Integer, db.ForeignKey('restaurant.id'))

    @property
    def v_map(self):
        return {
            'id': 'id',
            'name': 'name',
            'is_working': 'is_working',
            'barcode_id': 'barcode_id',
            'restaurant_id': 'restaurant_id'
        }