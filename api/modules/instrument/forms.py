from wtforms_alchemy import ModelForm
from .models import Instrument


class InstrumentForm(ModelForm):
    class Meta:
        model = Instrument