from flask import request
from flask.ext.login import login_required
from ...base.rest import make_rest
from ...base.wtform import WtfFormData
from .forms import Instrument, InstrumentForm
from . import mod


@mod.route('/', methods=['POST'])
@login_required
def create_instrument():
    """
    @api {post} instruments/ Creating an instrument
    @apiName CreateInstrument
    @apiGroup Instruments
    @apiVersion 1.0.0

    @apiParam {String} name Instrument name.
    @apiParam {Number} restaurant_id Restaurant unique ID.
    @apiParam {Boolean} is_working=True If instrument is working.
    @apiParam {Number} barcode_id=0 Barcode of the instrument.

    @apiSuccessExample {json} Success-Example:
    HTTP/1.1 200
    {
      "data": {
        "barcode_id": 12,
        "id": 4,
        "is_working": true,
        "name": "food_name",
        "restaurant_id": 1
      },
      "status": "OK"
    }
    """
    form = InstrumentForm(data=request.json)
    if form.validate():
        instrument = Instrument()
        form.populate_obj(instrument)
        Instrument.add(instrument)
        return make_rest('OK', instrument, 200)

    return make_rest('ERROR', form.errors, 400)

@mod.route('/', methods=['GET'])
@login_required
def get_instruments():
    """
    @api {get} instruments/ Getting all instruments
    @apiName GetInstrument
    @apiGroup Instruments
    @apiVersion 1.0.0

    @apiSuccessExample {json} Success-Response:
    HTTP/1.1 200
    {
      "data": [
        {
          "barcode_id": 12,
          "id": 1,
          "is_working": true,
          "name": "food_name",
          "restaurant_id": 1
        },
        {
          "barcode_id": 12,
          "id": 2,
          "is_working": true,
          "name": "food_name",
          "restaurant_id": 1
        }
      ],
      "status": "OK"
    }
    """
    instruments = Instrument.get(args=request.args)
    return make_rest('OK', instruments, 200, include=request.args.get('include', None))


@mod.route('/<id>/', methods=['GET'])
@login_required
def get_instrument(id):
    """
    @api {get} instruments/:id Getting an instrument
    @apiName GetSingleInstrument
    @apiGroup Instruments
    @apiVersion 1.0.0

    @apiParam {Number} id Instrument unique ID.

    @apiSuccessExample {json} Success-Response:
    HTTP/1.1 200
    {
      "data": {
        "barcode_id": 12,
        "id": 1,
        "is_working": true,
        "name": "food_name",
        "restaurant_id": 1
      },
      "status": "OK"
    }

    @apiErrorExample {json} Error-Response:
    HTTP/1.1 204
    {
      "data": "There is no instrument with this id",
      "status": "ERROR"
    }
    """
    instrument = Instrument.get(id=int(id), args=request.args)
    if not instrument:
        return make_rest('ERROR', 'There is no instrument with this id', 204)

    return make_rest('OK', instrument, 200, include=request.args.get('include', None))
    
@mod.route('/<id>/', methods=['PUT'])
@login_required
def update_instrument(id):
    """
    @api {put} instruments/:id Updating an instrument
    @apiName UpdateSingleInstrument
    @apiGroup Instruments
    @apiVersion 1.0.0

    @apiParam {Number} id Instrument unique ID.
    @apiParam {String} name Instrument name.
    @apiParam {Number} restaurant_id Restaurant unique ID.
    @apiParam {Boolean} is_working=True If instrument is working.
    @apiParam {Number} barcode_id=0 Barcode of the instrument.

    @apiSuccessExample {json} Success-Response:
    HTTP/1.1 200
    {
      "data": {
        "barcode_id": 12,
        "id": 7,
        "is_working": true,
        "name": "food_name",
        "restaurant_id": 1
      },
      "status": "OK"
    }

    @apiErrorExample {json} Error-Response:
    HTTP/1.1 204
    {
      "data": "There is no instrument with this id",
      "status": "ERROR"
    }
    """
    instrument = Instrument.get(id=int(id), args=request.args)
    if not instrument:
        return make_rest('ERROR', 'There is no instrument with this id', 204)

    form = InstrumentForm(WtfFormData(request.json), obj=instrument)
    if form.validate():
        form.populate_obj(instrument)
        Instrument.update(instrument)

        return make_rest('OK', instrument, 200)

    return make_rest('ERROR', form.errors, 400)

@mod.route('/<id>/', methods=['DELETE'])
@login_required
def delete_instrument(id):
    """
    @api {delete} foods/:id Deleting an instrument
    @apiName DeleteSingleInstrument
    @apiGroup Instruments
    @apiVersion 1.0.0

    @apiParam {Number} id Instrument unique ID.

    @apiSuccessExample {json} Success-Response:
    HTTP/1.1 200
    {
      "data": "Deleted id=2 instrument",
      "status": "OK"
    }

    @apiErrorExample {json} Error-Response:
    HTTP/1.1 204
    {
      "data": "There is no instrument with this id",
      "status": "ERROR"
    }
    """
    instrument = Instrument.get(id=int(id), args=request.args)
    if instrument:
        Instrument.remove(instrument)
        return make_rest('OK', 'Deleted id={0} instrument'.format(id), 200)
    
    return make_rest('ERROR', 'There is no instrument with this id', 204)
