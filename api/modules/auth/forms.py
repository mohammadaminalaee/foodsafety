from wtforms import Form, StringField, PasswordField, validators as v


class LoginForm(Form):
    email = StringField('Email', [v.Length(min=4, max=25), v.Email(message='Please enter a valid email')])
    password = PasswordField('Password', [v.DataRequired('Password field is required')])