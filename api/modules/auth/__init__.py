from flask import Blueprint
mod = Blueprint('auth', __name__, url_prefix='/api/v1/auth')
from .views import *