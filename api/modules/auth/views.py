from flask import render_template, redirect, url_for, request, flash, Request
from flask.ext.login import login_user, logout_user, login_required
from ...base.rest import make_rest
from ..user.models import User
from .forms import LoginForm
from . import mod


@mod.route('/login/', methods=['POST'])
def login():
    """
    @api {post} auth/login/ Logging a user in
    @apiName LoginUser
    @apiGroup Auth
    @apiVersion 1.0.0

    @apiParam {String} email User email.
    @apiParam {String} password User password.

    @apiSuccessExample {json} Success-Response:
        HTTP/1.1 200 OK
        {
          "data": "Login successful",
          "status": "OK"
        }

    @apiErrorExample {json} Error-Response:
        HTTP/1.1 400 ERROR
        {
          "data": "Invalid Username/Password",
          "status": "Error"
        }
    """
    form = LoginForm(request.form)
    if form.validate():
        query = User.query.filter(User.email == form.email.data)
        user = User.get(query=query, args=request.args)

        if user and user.password == form.password.data:
            login_user(user, remember=True)
            return make_rest(status='OK', data='Login successful', code=200)
        return make_rest(status='Error', data='Invalid Username/Password', code=400)
    return make_rest(status='Error', data=form.errors, code=400)  #, 'csrf_token': form.csrf_token})


@mod.route('/logout/', methods=['GET'])
@login_required
def logout():
    """
    @api {get} auth/logout/ Logging a user out
    @apiName LogoutUser
    @apiGroup Auth
    @apiVersion 1.0.0

    @apiSuccessExample {json} Success-Response:
    HTTP/1.1 200
    {
      "data": "Logout successful",
      "status": "OK"
    }

    @apiErrorExample {json} Error-Response:
    HTTP/1.1 401
    {
      "data": "Unauthorized access",
      "status": "ERROR"
    }
    """
    logout_user()
    return make_rest(status='OK', data='Logout successful', code=200)
