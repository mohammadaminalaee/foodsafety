from sqlalchemy.dialects.postgresql import JSONB
from sqlalchemy_utils import generic_relationship

from ...base.db import db, Timestamp, FSModel


class Report(db.Model, Timestamp, FSModel):
    id = db.Column(db.Integer, primary_key=True)
    data = db.Column(JSONB())

    object_type = db.Column(db.Unicode(255))
    object_id = db.Column(db.Integer)

    # TODO => Has problem
    # object = generic_relationship(object_type, object_id)

    description = db.Column(db.String)

    type = db.Column(db.String(20))

    def get_param(self, var_name):
        return self.data.get(var_name, None)

    def set_param(self, var_name, var_val):
        if not self.data:
            self.data = {}

        self.data[var_name] = var_val

    __mapper_args__ = {
        'polymorphic_on': type,
        'polymorphic_identity': 'report'
    }

    @property
    def v_map(self):
        return {
            'id': 'id',
            'object_type': 'object_type',
            'object_id': 'object_id',
            'description': 'description',
            'type': 'type'
        }


class TemperatureReport(Report):
    @property
    def temperature(self):
        return self.get_param('temperature')

    @temperature.setter
    def temperature(self, val):
        self.set_param('temperature', val)

    @property
    def instrument(self):
        return self.object

    @instrument.setter
    def instrument(self, val):
        self.object = val

    __mapper_args__ = {
        'polymorphic_identity': 'temperature'
    }

    @property
    def v_map(self):
        temp = super(TemperatureReport, self).v_map
        temp['temperature'] = 'temperature'

        return temp


class StaffReport(Report):
    @property
    def health(self):
        return self.get_param('health')

    @health.setter
    def health(self, val):
        if not isinstance(val, bool):
            print 'ERROR:=> health not instance of bool and set False'
            self.set_param('health', False)
        else:
            self.set_param('health', val)

    @property
    def appearance(self):
        return self.get_param('appearance')

    @appearance.setter
    def appearance(self, val):
        if not isinstance(val, bool):
            print 'ERROR:=> appearance not instance of bool and set False'
            self.set_param('appearance', False)
        else:
            self.set_param('appearance', val)

    @property
    def staff(self):
        return self.object

    @staff.setter
    def staff(self, val):
        self.object = val

    __mapper_args__ = {
        'polymorphic_identity': 'staff'
    }

    @property
    def v_map(self):
        temp = super(StaffReport, self).v_map
        temp['health'] = 'health'
        temp['appearance'] = 'appearance'

        return temp


class SupplierReport(Report):
    @property
    def q1(self):
        return self.get_param('q1')

    @q1.setter
    def q1(self, val):
        if not isinstance(val, bool):
            print 'ERROR:=> q1 not instance of bool and set False'
            self.set_param('q1', False)
        else:
            self.set_param('q1', val)

    @property
    def q2(self):
        return self.get_param('q2')

    @q2.setter
    def q2(self, val):
        if not isinstance(val, bool):
            print 'ERROR:=> q2 not instance of bool and set False'
            self.set_param('q2', False)
        else:
            self.set_param('q2', val)

    @property
    def q3(self):
        return self.get_param('q3')

    @q3.setter
    def q3(self, val):
        if not isinstance(val, bool):
            print 'ERROR:=> q3 not instance of bool and set False'
            self.set_param('q3', False)
        else:
            self.set_param('q3', val)

    @property
    def q4(self):
        return self.get_param('q4')

    @q4.setter
    def q4(self, val):
        if not isinstance(val, bool):
            print 'ERROR:=> q4 not instance of bool and set False'
            self.set_param('q4', False)
        else:
            self.set_param('q4', val)

    @property
    def supplier(self):
        return self.object

    @supplier.setter
    def supplier(self, val):
        self.object = val

    __mapper_args__ = {
        'polymorphic_identity': 'supplier'
    }

    @property
    def v_map(self):
        temp = super(SupplierReport, self).v_map
        temp['q1'] = 'q1'
        temp['q2'] = 'q2'
        temp['q3'] = 'q3'
        temp['q4'] = 'q4'

        return temp


class DailyReport(Report):
    @property
    def leftout(self):
        return self.get_param('leftout')

    @leftout.setter
    def leftout(self, val):
        if not isinstance(val, bool):
            print 'ERROR:=> leftout not instance of bool and set False'
            self.set_param('leftout', False)
        else:
            self.set_param('leftout', val)

    @property
    def leftout_desc(self):
        return self.get_param('leftout_desc')

    @leftout_desc.setter
    def leftout_desc(self, val):
        self.set_param('leftout_desc', val)

    @property
    def expired(self):
        return self.get_param('expired')

    @expired.setter
    def expired(self, val):
        if not isinstance(val, bool):
            print 'ERROR:=> expired not instance of bool and set False'
            self.set_param('expired', False)
        else:
            self.set_param('expired', val)

    @property
    def expired_desc(self):
        return self.get_param('expired_desc')

    @expired_desc.setter
    def expired_desc(self, val):
        self.set_param('expired_desc', val)

    @property
    def dirtyclothes(self):
        return self.get_param('dirtyclothes')

    @dirtyclothes.setter
    def dirtyclothes(self, val):
        if not isinstance(val, bool):
            print 'ERROR:=> dirtyclothes not instance of bool and set False'
            self.set_param('dirtyclothes', False)
        else:
            self.set_param('dirtyclothes', val)

    @property
    def dirtyclothes_desc(self):
        return self.get_param('dirtyclothes_desc')

    @dirtyclothes_desc.setter
    def dirtyclothes_desc(self, val):
        self.set_param('dirtyclothes_desc', val)

    @property
    def waste(self):
        return self.get_param('waste')

    @waste.setter
    def waste(self, val):
        if not isinstance(val, bool):
            print 'ERROR:=> waste not instance of bool and set False'
            self.set_param('waste', False)
        else:
            self.set_param('waste', val)

    @property
    def waste_desc(self):
        return self.get_param('waste_desc')

    @waste_desc.setter
    def waste_desc(self, val):
        self.set_param('waste_desc', val)
    @property
    def floor(self):
        return self.get_param('floor')

    @floor.setter
    def floor(self, val):
        if not isinstance(val, bool):
            print 'ERROR:=> floor not instance of bool and set False'
            self.set_param('floor', False)
        else:
            self.set_param('floor', val)

    @property
    def floor_desc(self):
        return self.get_param('floor_desc')

    @floor_desc.setter
    def floor_desc(self, val):
        self.set_param('floor_desc', val)

    @property
    def board(self):
        return self.get_param('board')

    @board.setter
    def board(self, val):
        if not isinstance(val, bool):
            print 'ERROR:=> board not instance of bool and set False'
            self.set_param('board', False)
        else:
            self.set_param('board', val)

    @property
    def board_desc(self):
        return self.get_param('board_desc')

    @board_desc.setter
    def board_desc(self, val):
        self.set_param('board_desc', val)

    @property
    def utensile(self):
        return self.get_param('utensile')

    @utensile.setter
    def utensile(self, val):
        if not isinstance(val, bool):
            print 'ERROR:=> utensile not instance of bool and set False'
            self.set_param('utensile', False)
        else:
            self.set_param('utensile', val)

    @property
    def utensile_desc(self):
        return self.get_param('utensile_desc')

    @utensile_desc.setter
    def utensile_desc(self, val):
        self.set_param('utensile_desc', val)

    @property
    def equipment(self):
        return self.get_param('equipment')

    @equipment.setter
    def equipment(self, val):
        if not isinstance(val, bool):
            print 'ERROR:=> equipment not instance of bool and set False'
            self.set_param('equipment', False)
        else:
            self.set_param('equipment', val)

    @property
    def equipment_desc(self):
        return self.get_param('equipment_desc')

    @equipment_desc.setter
    def equipment_desc(self, val):
        self.set_param('equipment_desc', val)

    @property
    def storage(self):
        return self.get_param('storage')

    @storage.setter
    def storage(self, val):
        if not isinstance(val, bool):
            print 'ERROR:=> storage not instance of bool and set False'
            self.set_param('storage', False)
        else:
            self.set_param('storage', val)

    @property
    def storage_desc(self):
        return self.get_param('storage_desc')

    @storage_desc.setter
    def storage_desc(self, val):
        self.set_param('storage_desc', val)

    @property
    def food_prep(self):
        return self.get_param('food_prep')

    @food_prep.setter
    def food_prep(self, val):
        if not isinstance(val, bool):
            print 'ERROR:=> food_prep not instance of bool and set False'
            self.set_param('food_prep', False)
        else:
            self.set_param('food_prep', val)

    @property
    def food_prep_desc(self):
        return self.get_param('food_prep_desc')

    @food_prep_desc.setter
    def food_prep_desc(self, val):
        self.set_param('food_prep_desc', val)

    @property
    def matt_ok(self):
        return self.get_param('matt_ok')

    @matt_ok.setter
    def matt_ok(self, val):
        if not isinstance(val, bool):
            print 'ERROR:=> matt_ok not instance of bool and set False'
            self.set_param('matt_ok', False)
        else:
            self.set_param('matt_ok', val)

    @property
    def matt_ok_desc(self):
        return self.get_param('matt_ok_desc')

    @matt_ok_desc.setter
    def matt_ok_desc(self, val):
        self.set_param('matt_ok_desc', val)

    @property
    def restaurant(self):
        return self.object

    @restaurant.setter
    def restaurant(self, val):
        self.object = val

    __mapper_args__ = {
        'polymorphic_identity': 'daily'
    }

    @property
    def v_map(self):
        temp = super(DailyReport, self).v_map

        temp['leftout'] = 'leftout'
        temp['leftout_desc'] = 'leftout_desc'
        temp['expired'] = 'expired'
        temp['expired_desc'] = 'expired_desc'
        temp['dirtyclothes'] = 'dirtyclothes'
        temp['dirtyclothes_desc'] = 'dirtyclothes_desc'
        temp['waste'] = 'waste'
        temp['waste_desc'] = 'waste_desc'
        temp['floor'] = 'floor'
        temp['floor_desc'] = 'floor_desc'
        temp['board'] = 'board'
        temp['board_desc'] = 'board_desc'
        temp['utensile'] = 'utensile'
        temp['utensile_desc'] = 'utensile_desc'
        temp['equipment'] = 'equipment'
        temp['equipment_desc'] = 'equipment_desc'
        temp['storage'] = 'storage'
        temp['storage_desc'] = 'storage_desc'
        temp['food_prep'] = 'food_prep'
        temp['food_prep_desc'] = 'food_prep_desc'
        temp['matt_ok'] = 'matt_ok'
        temp['matt_ok_desc'] = 'matt_ok_desc'

        return temp


class HeatingReport(Report):
    @property
    def temp_now(self):
        return self.get_param('temp_now')

    @temp_now.setter
    def temp_now(self, val):
        self.set_param('temp_now', val)

    @property
    def temp_to(self):
        return self.get_param('temp_to')

    @temp_to.setter
    def temp_to(self, val):
        self.set_param('temp_to', val)

    @property
    def food_name(self):
        return self.get_param('food_name')

    @food_name.setter
    def food_name(self, val):
        self.set_param('food_name', val)

    @property
    def restaurant(self):
        return self.object

    @restaurant.setter
    def restaurant(self, val):
        self.object = val

    __mapper_args__ = {
        'polymorphic_identity': 'heating'
    }

    @property
    def v_map(self):
        temp = super(HeatingReport, self).v_map

        temp['temp_now'] = 'temp_now'
        temp['temp_to'] = 'temp_to'
        temp['food_name'] = 'food_name'

        return temp


class ChillingReport(Report):
    @property
    def temp_now(self):
        return self.get_param('temp_now')

    @temp_now.setter
    def temp_now(self, val):
        self.set_param('temp_now', val)

    @property
    def temp_to(self):
        return self.get_param('temp_to')

    @temp_to.setter
    def temp_to(self, val):
        self.set_param('temp_to', val)

    @property
    def food_name(self):
        return self.get_param('food_name')

    @food_name.setter
    def food_name(self, val):
        self.set_param('food_name', val)

    @property
    def method(self):
        return self.get_param('method')

    @method.setter
    def method(self, val):
        self.set_param('method', val)

    @property
    def restaurant(self):
        return self.object

    @restaurant.setter
    def restaurant(self, val):
        self.object = val

    __mapper_args__ = {
        'polymorphic_identity': 'chilling'
    }

    @property
    def v_map(self):
        temp = super(ChillingReport, self).v_map

        temp['temp_now'] = 'temp_now'
        temp['temp_to'] = 'temp_to'
        temp['food_name'] = 'food_name'
        temp['method'] = 'method'

        return temp


class DefrostingReport(Report):
    @property
    def temp_to(self):
        return self.get_param('temp_to')

    @temp_to.setter
    def temp_to(self, val):
        self.set_param('temp_to', val)

    @property
    def food_name(self):
        return self.get_param('food_name')

    @food_name.setter
    def food_name(self, val):
        self.set_param('food_name', val)

    @property
    def method(self):
        return self.get_param('method')

    @method.setter
    def method(self, val):
        self.set_param('method', val)

    @property
    def restaurant(self):
        return self.object

    @restaurant.setter
    def restaurant(self, val):
        self.object = val

    __mapper_args__ = {
        'polymorphic_identity': 'defrosting'
    }

    @property
    def v_map(self):
        temp = super(DefrostingReport, self).v_map

        temp['temp_to'] = 'temp_to'
        temp['food_name'] = 'food_name'
        temp['method'] = 'method'

        return temp


class CleaningReport(Report):
    @property
    def field(self):
        return self.get_param('field')

    @field.setter
    def field(self, val):
        self.set_param('field', val)

    @property
    def periodic_time(self):
        return self.get_param('periodic_time')

    @periodic_time.setter
    def periodic_time(self, val):
        self.set_param('periodic_time', val)

    @property
    def restaurant(self):
        return self.object

    @restaurant.setter
    def restaurant(self, val):
        self.object = val

    __mapper_args__ = {
        'polymorphic_identity': 'cleaning'
    }

    @property
    def v_map(self):
        temp = super(CleaningReport, self).v_map

        temp['field'] = 'field'
        temp['periodic_time'] = 'periodic_time'

        return temp
