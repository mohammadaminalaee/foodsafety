from flask import request
from flask.ext.login import login_required
from ...base.rest import make_rest
from ...base.wtform import WtfFormData
from .forms import \
    TemperatureReport, TemperatureReportForm, \
    StaffReport, StaffReportForm,\
    SupplierReport, SupplierReportForm,\
    DailyReport, DailyReportForm, \
    HeatingReport, HeatingReportForm, \
    ChillingReport, ChillingReportForm, \
    DefrostingReport, DefrostingReportForm, \
    CleaningReport, CleaningReportForm
from . import mod

@mod.route('/temperatures/', methods=['POST'])
@login_required
def create_temperature():
    """
    @api {post} reports/temperatures/ Creating a temperature
    @apiName CreateTemperature
    @apiGroup Temperatures
    @apiVersion 1.0.0

    @apiParam {String} description Temperature description.
    @apiParam {Number} temperature Temperature value.
    @apiParam {String} object_type Temperature object type.
    @apiParam {Number} object_id Temperature object ID.

    @apiSuccessExample {json} Success-Response:
    HTTP/1.1 200
    {
        "data": {
              "id": 7,
              "description": "salam",
              "temperature": 10,
              "object_type": "instrument",
              "object_id": 1
          },
        "status": "OK"
    }
    """
    form = TemperatureReportForm(data=request.json)
    if form.validate():
        temperature = TemperatureReport()
        form.populate_obj(temperature)
        TemperatureReport.add(temperature)

        return make_rest('OK', temperature, 200)

    return make_rest('ERROR', form.errors, 400)

@mod.route('/temperatures/', methods=['GET'])
@login_required
def get_temperatures():
    """
    @api {get} reports/temperatures Getting all temperatures
    @apiName GetTemperature
    @apiGroup Temperatures
    @apiVersion 1.0.0

    @apiSuccessExample {json} Success-Response:
    HTTP/1.1 200
    {
      "data": [
        {
          "description": "salam",
          "id": 24,
          "object_id": 1,
          "object_type": "instrument",
          "temperature": 10,
          "type": "temperature"
        },
        {
          "description": "salam",
          "id": 25,
          "object_id": 1,
          "object_type": "instrument",
          "temperature": 10,
          "type": "temperature"
        }
      ],
      "status": "OK"
    }
    """
    temperatures = TemperatureReport.get(args=request.args)
    return make_rest('OK', temperatures, 200, include=request.args.get('include', None))

@mod.route('/temperatures/<id>/', methods=['GET'])
@login_required
def get_temperature(id):
    """
    @api {get} reports/temperatures:id Getting a temperature
    @apiName GetSingleTemperature
    @apiGroup Temperatures
    @apiVersion 1.0.0

    @apiParam {Number} id Temperature unique ID.

    @apiSuccessExample {json} Success-Response:
    HTTP/1.1 200
    {
      "data": {
          "description": "salam",
          "id": 24,
          "object_id": 1,
          "object_type": "instrument",
          "temperature": 10,
          "type": "temperature"1
      },
      "status": "OK"
    }

    @apiErrorExample {json} Error-Response:
    HTTP/1.1 204
    {
      "data": "There is no temperature report with this id",
      "status": "ERROR"
    }
    """
    temperature = TemperatureReport.get(id=int(id), args=request.args)
    if not temperature:
        return make_rest('ERROR', 'There is no temperature report with this id', 204)

    return make_rest('OK', temperature, 200, include=request.args.get('include', None))

@mod.route('/temperatures/<id>/', methods=['PUT'])
@login_required
def update_temperature(id):
    """
    @api {put} reports/temperatures/:id Updating a temperature
    @apiName UpdateSingleTemperature
    @apiGroup Temperatures
    @apiVersion 1.0.0

    @apiParam {Number} id Temperature unique ID.
    @apiParam {String} description Temperature description.
    @apiParam {Number} temperature Temperature value.
    @apiParam {String} object_type Temperature object type.
    @apiParam {Number} object_id Temperature object ID.

    @apiSuccessExample {json} Success-Response:
    HTTP/1.1 200
    {
      "data": {
          "description": "salam",
          "id": 24,
          "object_id": 1,
          "object_type": "instrument",
          "temperature": 10,
          "type": "temperature"1
      },
      "status": "OK"
    }

    @apiErrorExample {json} Error-Response:
    HTTP/1.1 204
    {
      "data": "There is no temperature with this id",
      "status": "ERROR"
    }
    """
    temperature = TemperatureReport.get(id=int(id), args=request.args)
    if not temperature:
        return make_rest('ERROR', 'There is no temperature report with this id', 204)

    form = TemperatureReportForm(WtfFormData(request.json), obj=temperature)
    if form.validate():
        form.populate_obj(temperature)
        TemperatureReport.update(temperature)

        return make_rest('OK', temperature, 200)

    return make_rest('ERROR', form.errors, 400)

@mod.route('/temperatures/<id>/', methods=['DELETE'])
@login_required
def delete_temperature(id):
    """
    @api {delete} reports/temperatures/:id Deleting a temperature
    @apiName DeleteSingleTemperature
    @apiGroup Temperatures
    @apiVersion 1.0.0

    @apiParam {Number} id Temperature unique ID.

    @apiSuccessExample {json} Success-Response:
    HTTP/1.1 200
    {
      "data": "Deleted id=2 temperature",
      "status": "OK"
    }

    @apiErrorExample {json} Error-Response:
    HTTP/1.1 204
    {
      "data": "There is no temperature report with this id",
      "status": "ERROR"
    }
    """
    temperature = TemperatureReport.get(id=int(id), args=request.args)
    if temperature:
        TemperatureReport.remove(temperature)
        return make_rest('OK', 'Deleted id={0} temperature report'.format(id), 200)
    
    return make_rest('ERROR', 'There is no temperature report with this id', 204)

@mod.route('/staffs/', methods=['POST'])
@login_required
def create_staff():
    """
    @api {post} reports/staffs/ Creating a staff
    @apiName CreateStaff
    @apiGroup Staffs
    @apiVersion 1.0.0

    @apiParam {String} description Staff description.
    @apiParam {Boolean} appearance Staff appearance.
    @apiParam {Boolean} health Staff is healthy.
    @apiParam {Number} object_id Staff object id.
    @apiParam {String} object_typr Staff object type.
    @apiParam {String} type Staff type. 

    @apiSuccessExample {json} Success-Response:
    HTTP/1.1 200
    {
        "data": {
            "appearance": false,
            "description": "salam",
            "health": true,
            "id": 31,
            "object_id": 1,
            "object_type": "staff",
            "type": "staff"
        },
        "status": "OK"
    }
    """
    form = StaffReportForm(data=request.json)
    if form.validate():
        staff = StaffReport()
        form.populate_obj(staff)
        StaffReport.add(staff)

        return make_rest('OK', staff, 200)

    return make_rest('ERROR', form.errors, 400)

@mod.route('/staffs/', methods=['GET'])
@login_required
def get_staffs():
    """
    @api {get} reports/staffs/ Getting all staff
    @apiName GetStaffs
    @apiGroup Staffs
    @apiVersion 1.0.0

    @apiSuccessExample {json} Success-Response:
    HTTP/1.1 200
    {
      "data": [
        {
          "appearance": false,
          "description": "salam",
          "health": true,
          "id": 18,
          "object_id": 1,
          "object_type": "staff",
          "type": "staff"
        },
        {
          "appearance": false,
          "description": "salam",
          "health": true,
          "id": 19,
          "object_id": 1,
          "object_type": "staff",
          "type": "staff"
        }
      ],
      "status": "OK"
    }
    """
    staffs = StaffReport.get(args=request.args)
    return make_rest('OK', staffs, 200, include=request.args.get('include', None))

@mod.route('/staffs/<id>/', methods=['GET'])
@login_required
def get_staff(id):
    """
    @api {get} reports/staffs/:id Getting a staff
    @apiName GetSingleStaff
    @apiGroup Staffs
    @apiVersion 1.0.0

    @apiParam {Number} id Staff unique ID.

    @apiSuccessExample {json} Success-Response:
    HTTP/1.1 200
    {
      "data": [
        {
          "appearance": false,
          "description": "salam",
          "health": true,
          "id": 18,
          "object_id": 1,
          "object_type": "staff",
          "type": "staff"
        }
      ],
      "status": "OK"
    }

    @apiErrorExample {json} Error-Response:
    HTTP/1.1 204
    {
      "data": "There is no staff with this id",
      "status": "ERROR"
    }
    """
    staff = StaffReport.get(id=int(id), args=request.args)
    if not staff:
        return make_rest('ERROR', 'There is no staff with this id', 204)

    return make_rest('OK', staff, 200, include=request.args.get('include', None))

@mod.route('/staffs/<id>/', methods=['PUT'])
@login_required
def update_staff(id):
    """
    @api {put} reports/staffs/:id Updating a staff
    @apiName UpdateSingleStaff
    @apiGroup Staffs
    @apiVersion 1.0.0

    @apiParam {Number} id Staff unique ID.
    @apiParam {String} description Staff description.
    @apiParam {Boolean} appearance Staff appearance.
    @apiParam {Boolean} health Staff is healthy.
    @apiParam {Number} object_id Staff object id.
    @apiParam {String} object_typr Staff object type.
    @apiParam {String} type Staff type. 

    @apiSuccessExample {json} Success-Response:
    HTTP/1.1 200
    {
      "data": {
          "appearance": false,
          "description": "salam",
          "health": true,
          "id": 18,
          "object_id": 1,
          "object_type": "staff",
          "type": "staff"
        },
      "status": "OK"
    }

    @apiErrorExample {json} Error-Response:
    HTTP/1.1 204
    {
      "data": "There is no staff with this id",
      "status": "ERROR"
    }
    """
    staff = StaffReport.get(id=int(id), args=request.args)
    if not staff:
        return make_rest('ERROR', 'There is no staff report with this id', 204)

    form = StaffReportForm(WtfFormData(request.json), obj=staff)
    if form.validate():
        form.populate_obj(staff)
        StaffReport.update(staff)

        return make_rest('OK', staff, 200)

    return make_rest('ERROR', form.errors, 400)

@mod.route('/staffs/<id>/', methods=['DELETE'])
@login_required
def delete_staff(id):
    """
    @api {delete} reports/staffs/:id Deleting a staff
    @apiName DeleteSingleStaff
    @apiGroup Staffs
    @apiVersion 1.0.0

    @apiParam {Number} id Staff unique ID.

    @apiSuccessExample {json} Success-Response:
    HTTP/1.1 200
    {
      "data": "Deleted id=2 staff",
      "status": "OK"
    }

    @apiErrorExample {json} Error-Response:
    HTTP/1.1 204
    {
      "data": "There is no staff with this id",
      "status": "ERROR"
    }
    """
    staff = StaffReport.get(id=int(id), args=request.args)
    if staff:
        StaffReport.remove(staff)
        return make_rest('OK', 'Deleted id={0} staff report'.format(id), 200)
    
    return make_rest('ERROR', 'There is no staff report with this id', 204)

@mod.route('/suppliers/', methods=['POST'])
@login_required
def create_supplier():
    """
    @api {post} reports/suppliers/ Creating a supplier
    @apiName CreateSupplier
    @apiGroup Suppliers
    @apiVersion 1.0.0

    @apiParam {String} name Supplier name.
    @apiParam {String} address Supplier address.
    @apiParam {String} contact_phone Supplier contact phone.


    @apiSuccessExample {json} Success-Response:
    HTTP/1.1 200
    {
        "data": {
            "id": 1,
            "name": "supplier_name",
            "address": "supplier_address",
            "contact_phone": "0217676767"
          },
        "status": "OK"
    }
    """
    form = SupplierReportForm(data=request.json)
    if form.validate():
        supplier = SupplierReport()
        form.populate_obj(supplier)
        SupplierReport.add(supplier)

        return make_rest('OK', supplier, 200)

    return make_rest('ERROR', form.errors, 400)

@mod.route('/suppliers/', methods=['GET'])
@login_required
def get_suppliers():
    """
    @api {get} reports/suppliers/ Getting all suppliers
    @apiName GetSuppliers
    @apiGroup Suppliers
    @apiVersion 1.0.0

    @apiSuccessExample {json} Success-Response:
    HTTP/1.1 200
    {
        "data": [
            {
                "id": 1,
                "name": "supplier_name",
                "address": "supplier_address",
                "contact_phone": "0217676767"
            },
            {
                "id": 2,
                "name": "supplier_name",
                "address": "supplier_address",
                "contact_phone": "0217676767"
            }
        ],
        "status": "OK"
    }
    """
    suppliers = SupplierReport.get(args=request.args)
    return make_rest('OK', suppliers, 200, include=request.args.get('include', None))

@mod.route('/suppliers/<id>/', methods=['GET'])
@login_required
def get_supplier(id):
    """
    @api {get} reports/suppliers/:id Getting a supplier
    @apiName GetSingleSupplier
    @apiGroup Suppliers
    @apiVersion 1.0.0

    @apiParam {Number} id Supplier unique ID.

    @apiSuccessExample {json} Success-Response:
    HTTP/1.1 200
    {
      "data": {
            "id": 1,
            "name": "supplier_name",
            "address": "supplier_address",
            "contact_phone": "0217676767"
            },
      "status": "OK"
    }

    @apiErrorExample {json} Error-Response:
    HTTP/1.1 204
    {
      "data": "There is no supplier with this id",
      "status": "ERROR"
    }
    """
    supplier = SupplierReport.get(id=int(id), args=request.args)
    if not supplier:
        return make_rest('ERROR', "There is no supplier with this id", 204)

    return make_rest('OK', supplier, 200, include=request.args.get('include', None))

@mod.route('/suppliers/<id>/', methods=['PUT'])
@login_required
def update_supplier(id):
    """
    @api {put} reports/suppliers/:id Updating a supplier
    @apiName UpdateSingleSupplier
    @apiGroup Suppliers
    @apiVersion 1.0.0

    @apiParam {Number} id Supplier unique ID.
    @apiParam {String} name Supplier name.
    @apiParam {String} address Supplier address.
    @apiParam {String} contact_phone Supplier contact phone.

    @apiSuccessExample {json} Success-Response:
    HTTP/1.1 200
    {
      "data": {
            "id": 1,
            "name": "supplier_name",
            "address": "supplier_address",
            "contact_phone": "0217676767"
            },
      "status": "OK"
    }

    @apiErrorExample {json} Error-Response:
    HTTP/1.1 204
    {
      "data": "There is no supplier with this id",
      "status": "ERROR"
    }
    """
    supplier = SupplierReport.get(id=int(id), args=request.args)
    if not supplier:
        return make_rest('ERROR', 'There is no supplier report with this id', 204)

    form = SupplierReportForm(WtfFormData(request.json), obj=supplier)
    if form.validate():
        form.populate_obj(supplier)
        SupplierReport.update(supplier)

        return make_rest('OK', supplier, 200)

    return make_rest('ERROR', form.errors, 400)

@mod.route('/suppliers/<id>/', methods=['DELETE'])
@login_required
def delete_supplier(id):
    """
    @api {delete} reports/suppliers/:id Deleting a food
    @apiName DeleteSingleSupplier
    @apiGroup Suppliers
    @apiVersion 1.0.0

    @apiParam {Number} id Supplier unique ID.

    @apiSuccessExample {json} Success-Response:
    HTTP/1.1 200
    {
      "data": "Deleted id=2 supplier",
      "status": "OK"
    }

    @apiErrorExample {json} Error-Response:
    HTTP/1.1 204
    {
      "data": "There is no supplier with this id",
      "status": "ERROR"
    }
    """
    supplier = SupplierReport.get(id=int(id), args=request.args)
    if supplier:
        SupplierReport.remove(supplier)
        return make_rest('OK', 'Deleted id={0} supplier report'.format(id), 200)

    return make_rest('ERROR', 'There is no supplier report with this id', 204)


@mod.route('/dailies/', methods=['POST'])
@login_required
def create_daily():
    """
    @api {post} reports/dailies/ Creating a daily report
    @apiName CreateDailyReport
    @apiGroup Daily Reports
    @apiVersion 1.0.0

    @apiParam {String} description Daily Report description.
    @apiParam {Number} temperature Daily Report temperature.
    @apiParam {String} object_type Daily Report object type.
    @apiParam {Number} object_id Daily Report object ID.
    @apiParam {Boolean} leftout Daily Report if food left-out.
    @apiParam {String} leftout_desc Daily Report left-out description.
    @apiParam {Boolean} expired Daily Report if food expired.
    @apiParam {String} expired_desc Daily Report expiration description.
    @apiParam {Boolean} dirtyclothes Daily Report if clothes dirty.
    @apiParam {String} dirtyclothes_desc Daily Report if dirt clothes description.
    @apiParam {Boolean} waste Daily Report if waste was attended.
    @apiParam {String} waste_desc Daily Report waste description.
    @apiParam {Boolean} floor Daily Report if floor was clean.
    @apiParam {String} floor_desc Daily Report flood description.
    @apiParam {Boolean} board Daily Report if board was attended.
    @apiParam {String} board_desc Daily Report board description.
    @apiParam {Boolean} utensile Daily Report if utensils were clean.
    @apiParam {String} utensile_desc Daily Report utensils desciption.
    @apiParam {Boolean} equipment Daily Report if equipments were attended.
    @apiParam {String} equipment_desc Daily Report equipment description.
    @apiParam {Boolean} storage Daily Report if storage was attended.
    @apiParam {String} storage_desc Daily Report storage description.
    @apiParam {Boolean} food_prep Daily Report if food was prepared.
    @apiParam {String} food_prep_desc Daily Report food preparation description.
    @apiParam {Boolean} matt_ok Daily Report if materials were OK.
    @apiParam {String} matt_ok_desc Daily Report material description.

    @apiSuccessExample {json} Success-Response:
    HTTP/1.1 200
    {
        "data": {
        "id": 1,
        "description": "text",
        "temperature": 10,
        "object_type": "restaurant",
        "object_id": 1,
        "leftout": true,
        "leftout_desc": "leftout_desc",
        "expired": true,
        "expired_desc": "expired_desc",
        "dirtyclothes": true,
        "dirtyclothes_desc": "dirtyclothes_desc",
        "waste": true,
        "waste_desc": "waste_desc",
        "floor": true,
        "floor_desc": "floor_desc",
        "board": true,
        "board_desc": "board_desc",
        "utensile": true,
        "utensile_desc": "utensile_desc",
        "equipment": true,
        "equipment_desc": "equipment_desc",
        "storage": true,
        "storage_desc": "storage_desc",
        "food_prep": true,
        "food_prep_desc": "food_prep_desc",
        "matt_ok": true,
        "matt_ok_desc": "matt_ok_desc"
        },
        "status": "OK"
    }
    """
    form = DailyReportForm(data=request.json)
    if form.validate():
        daily = DailyReport()
        form.populate_obj(daily)
        DailyReport.add(daily)

        return make_rest('OK', daily, 200)

    return make_rest('ERROR', form.errors, 400)

@mod.route('/dailies/', methods=['GET'])
@login_required
def get_dailies():
    """
    @api {get} reports/dailies/ Getting all daily reports
    @apiName GetDailyReports
    @apiGroup Daily Reports
    @apiVersion 1.0.0

    @apiSuccessExample {json} Success-Response:
    HTTP/1.1 200
    {
        "data": [
            {
            "id": 1,
            "description": "text",
            "temperature": 10,
            "object_type": "restaurant",
            "object_id": 1,
            "leftout": true,
            "leftout_desc": "leftout_desc",
            "expired": true,
            "expired_desc": "expired_desc",
            "dirtyclothes": true,
            "dirtyclothes_desc": "dirtyclothes_desc",
            "waste": true,
            "waste_desc": "waste_desc",
            "floor": true,
            "floor_desc": "floor_desc",
            "board": true,
            "board_desc": "board_desc",
            "utensile": true,
            "utensile_desc": "utensile_desc",
            "equipment": true,
            "equipment_desc": "equipment_desc",
            "storage": true,
            "storage_desc": "storage_desc",
            "food_prep": true,
            "food_prep_desc": "food_prep_desc",
            "matt_ok": true,
            "matt_ok_desc": "matt_ok_desc"
          },{
            "id": 2,
            "description": "text",
            "temperature": 10,
            "object_type": "restaurant",
            "object_id": 1,
            "leftout": true,
            "leftout_desc": "leftout_desc",
            "expired": true,
            "expired_desc": "expired_desc",
            "dirtyclothes": true,
            "dirtyclothes_desc": "dirtyclothes_desc",
            "waste": true,
            "waste_desc": "waste_desc",
            "floor": true,
            "floor_desc": "floor_desc",
            "board": true,
            "board_desc": "board_desc",
            "utensile": true,
            "utensile_desc": "utensile_desc",
            "equipment": true,
            "equipment_desc": "equipment_desc",
            "storage": true,
            "storage_desc": "storage_desc",
            "food_prep": true,
            "food_prep_desc": "food_prep_desc",
            "matt_ok": true,
            "matt_ok_desc": "matt_ok_desc"
          }
        ],
        "status": "OK"
    }
    """
    dailies = DailyReport.get(args=request.args)
    return make_rest('OK', dailies, 200, include=request.args.get('include', None))

@mod.route('/dailies/<id>/', methods=['GET'])
@login_required
def get_daily(id):
    """
    @api {get} reports/dailies/:id Getting a daily report
    @apiName GetSingleDailyReport
    @apiGroup Daily Reports
    @apiVersion 1.0.0

    @apiParam {Number} id Daily Report unique ID.

    @apiSuccessExample {json} Success-Response:
    HTTP/1.1 200
    {
      "data": {
        "id": 1,
        "description": "text",
        "temperature": 10,
        "object_type": "restaurant",
        "object_id": 1,
        "leftout": true,
        "leftout_desc": "leftout_desc",
        "expired": true,
        "expired_desc": "expired_desc",
        "dirtyclothes": true,
        "dirtyclothes_desc": "dirtyclothes_desc",
        "waste": true,
        "waste_desc": "waste_desc",
        "floor": true,
        "floor_desc": "floor_desc",
        "board": true,
        "board_desc": "board_desc",
        "utensile": true,
        "utensile_desc": "utensile_desc",
        "equipment": true,
        "equipment_desc": "equipment_desc",
        "storage": true,
        "storage_desc": "storage_desc",
        "food_prep": true,
        "food_prep_desc": "food_prep_desc",
        "matt_ok": true,
        "matt_ok_desc": "matt_ok_desc"
      },
      "status": "OK"
    }

    @apiErrorExample {json} Error-Response:
    HTTP/1.1 204
    {
      "data": "There is no daily report with this id",
      "status": "ERROR"
    }
    """
    daily = DailyReport.get(id=int(id), args=request.args)
    if not daily:
        return make_rest('ERROR', "There is no daily report with this id", 204)

    return make_rest('OK', daily, 200, include=request.args.get('include', None))

@mod.route('/dailies/<id>/', methods=['PUT'])
@login_required
def update_daily(id):
    """
    @api {put} reports/dailies/:id Updating a daily report
    @apiName UpdateSingleDailyReport
    @apiGroup Daily Reports
    @apiVersion 1.0.0

    @apiParam {Number} id Daily Report unique ID.
    @apiParam {String} description Daily Report description.
    @apiParam {Number} temperature Daily Report temperature.
    @apiParam {String} object_type Daily Report object type.
    @apiParam {Number} object_id Daily Report object ID.
    @apiParam {Boolean} leftout Daily Report if food left-out.
    @apiParam {String} leftout_desc Daily Report left-out description.
    @apiParam {Boolean} expired Daily Report if food expired.
    @apiParam {String} expired_desc Daily Report expiration description.
    @apiParam {Boolean} dirtyclothes Daily Report if clothes dirty.
    @apiParam {String} dirtyclothes_desc Daily Report if dirt clothes description.
    @apiParam {Boolean} waste Daily Report if waste was attended.
    @apiParam {String} waste_desc Daily Report waste description.
    @apiParam {Boolean} floor Daily Report if floor was clean.
    @apiParam {String} floor_desc Daily Report flood description.
    @apiParam {Boolean} board Daily Report if board was attended.
    @apiParam {String} board_desc Daily Report board description.
    @apiParam {Boolean} utensile Daily Report if utensils were clean.
    @apiParam {String} utensile_desc Daily Report utensils desciption.
    @apiParam {Boolean} equipment Daily Report if equipments were attended.
    @apiParam {String} equipment_desc Daily Report equipment description.
    @apiParam {Boolean} storage Daily Report if storage was attended.
    @apiParam {String} storage_desc Daily Report storage description.
    @apiParam {Boolean} food_prep Daily Report if food was prepared.
    @apiParam {String} food_prep_desc Daily Report food preparation description.
    @apiParam {Boolean} matt_ok Daily Report if materials were OK.
    @apiParam {String} matt_ok_desc Daily Report material description.

    @apiSuccessExample {json} Success-Response:
    HTTP/1.1 200
    {
      "data": {
        "id": 1,
        "description": "text",
        "temperature": 10,
        "object_type": "restaurant",
        "object_id": 1,
        "leftout": true,
        "leftout_desc": "leftout_desc",
        "expired": true,
        "expired_desc": "expired_desc",
        "dirtyclothes": true,
        "dirtyclothes_desc": "dirtyclothes_desc",
        "waste": true,
        "waste_desc": "waste_desc",
        "floor": true,
        "floor_desc": "floor_desc",
        "board": true,
        "board_desc": "board_desc",
        "utensile": true,
        "utensile_desc": "utensile_desc",
        "equipment": true,
        "equipment_desc": "equipment_desc",
        "storage": true,
        "storage_desc": "storage_desc",
        "food_prep": true,
        "food_prep_desc": "food_prep_desc",
        "matt_ok": true,
        "matt_ok_desc": "matt_ok_desc"
      },
      "status": "OK"
    }

    @apiErrorExample {json} Error-Response:
    HTTP/1.1 204
    {
      "data": "There is no daily report with this id",
      "status": "ERROR"
    }
    """
    daily = DailyReport.get(id=int(id), args=request.args)
    if not daily:
        return make_rest('ERROR', 'There is no daily report with this id', 204)

    form = DailyReportForm(WtfFormData(request.json), obj=daily)
    if form.validate():
        form.populate_obj(daily)
        DailyReport.update(daily)

        return make_rest('OK', daily, 200)

    return make_rest('ERROR', form.errors, 400)

@mod.route('/dailies/<id>/', methods=['DELETE'])
@login_required
def delete_daily(id):
    """
    @api {delete} report/dailies/:id Deleting a daily report
    @apiName DeleteSingleDailyReport
    @apiGroup Daily Reports
    @apiVersion 1.0.0

    @apiParam {Number} id Daily Report unique ID.

    @apiSuccessExample {json} Success-Response:
    HTTP/1.1 200
    {
      "data": "Deleted id=2 daily report",
      "status": "OK"
    }

    @apiErrorExample {json} Error-Response:
    HTTP/1.1 204
    {
      "data": "There is no daily report with this id",
      "status": "ERROR"
    }
    """
    daily = DailyReport.get(id=int(id), args=request.args)
    if daily:
        DailyReport.remove(daily)
        return make_rest('OK', 'Deleted id={0} daily report'.format(id), 200)

    return make_rest('ERROR', 'There is not any daily report with this id', 204)


@mod.route('/heatings/', methods=['POST'])
@login_required
def create_heating():
    """
    @api {post} reports/heatings/ Creating a heating report
    @apiName CreateHeaintReport
    @apiGroup Heating Reports
    @apiVersion 1.0.0

    @apiParam {String} description Heating Report description.
    @apiParam {Number} temperature Heating Report temperature.
    @apiParam {String} object_type Heating Report object type.
    @apiParam {Number} object_id Heating Report object ID.
    @apiParam {String} temp_now Heating Report current temperature.
    @apiParam {String} temp_to Heating Report desired temperature.
    @apiParam {String} food_name Heating Report food name.

    @apiSuccessExample {json} Success-Response:
    HTTP/1.1 200
    {
        "data": {
            "id": 1,
            "description": "text",
            "temperature": 10,
            "object_type": "restaurant",
            "object_id": 1,
            "temp_now": "t1",
            "temp_to": "t2",
            "food_name": "t3"
        },
        "status": "OK"
    }
    """
    form = HeatingReportForm(data=request.json)
    if form.validate():
        heating = HeatingReport()
        form.populate_obj(heating)
        HeatingReport.add(heating)

        return make_rest('OK', heating, 200)

    return make_rest('ERROR', form.errors, 400)

@mod.route('/heatings/', methods=['GET'])
@login_required
def get_heatings():
    """
    @api {get} reports/heatings/ Getting all heating reports
    @apiName GetHeatingReports
    @apiGroup Heating Reports
    @apiVersion 1.0.0

    @apiSuccessExample {json} Success-Response:
    HTTP/1.1 200
    {
        "data": [
            {
            "id": 1,
            "description": "text",
            "temperature": 10,
            "object_type": "restaurant",
            "object_id": 1,
            "temp_now": "t1",
            "temp_to": "t2",
            "food_name": "t3"
            },{
            "id": 2,
            "description": "text",
            "temperature": 10,
            "object_type": "restaurant",
            "object_id": 1,
            "temp_now": "t1",
            "temp_to": "t2",
            "food_name": "t3"
            }
        ],
        "status": "OK"
    }
    """
    heatings = HeatingReport.get(args=request.args)
    return make_rest('OK', heatings, 200, include=request.args.get('include', None))

@mod.route('/heatings/<id>/', methods=['GET'])
@login_required
def get_heating(id):
    """
    @api {get} reports/heatings/:id Getting a heating report
    @apiName GetSingleHeatingReport
    @apiGroup Heating Reports
    @apiVersion 1.0.0

    @apiParam {Number} id Heating Report unique ID.

    @apiSuccessExample {json} Success-Response:
    HTTP/1.1 200
    {
      "data": {
            "id": 1,
            "description": "text",
            "temperature": 10,
            "object_type": "restaurant",
            "object_id": 1,
            "temp_now": "t1",
            "temp_to": "t2",
            "food_name": "t3"
      },
      "status": "OK"
    }

    @apiErrorExample {json} Error-Response:
    HTTP/1.1 204
    {
      "data": "There is no heating report with this id",
      "status": "ERROR"
    }
    """
    heating = HeatingReport.get(id=int(id), args=request.args)
    if not heating:
        return make_rest('ERROR', "There is no heating report with this id", 204)

    return make_rest('OK', heating, 200, include=request.args.get('include', None))

@mod.route('/heatings/<id>/', methods=['PUT'])
@login_required
def update_heating(id):
    """
    @api {put} reports/heatings/:id Updating a heating report
    @apiName UpdateSingleHeatingReport
    @apiGroup Heating Reports
    @apiVersion 1.0.0

    @apiParam {Number} id Heating Report unique ID.
    @apiParam {String} description Heating Report description.
    @apiParam {Number} temperature Heating Report temperature.
    @apiParam {String} object_type Heating Report object type.
    @apiParam {Number} object_id Heating Report object ID.
    @apiParam {String} temp_now Heating Report current temperature.
    @apiParam {String} temp_to Heating Report desired temperature.
    @apiParam {String} food_name Heating Report food name.

    @apiSuccessExample {json} Success-Response:
    HTTP/1.1 200
    {
      "data": {
            "id": 1,
            "description": "text",
            "temperature": 10,
            "object_type": "restaurant",
            "object_id": 1,
            "temp_now": "t1",
            "temp_to": "t2",
            "food_name": "t3"
      },
      "status": "OK"
    }

    @apiErrorExample {json} Error-Response:
    HTTP/1.1 204
    {
      "data": "There is no heating report with this id",
      "status": "ERROR"
    }
    """
    heating = HeatingReport.get(id=int(id), args=request.args)
    if not heating:
        return make_rest('ERROR', 'There is no heating report with this id', 204)

    form = HeatingReportForm(WtfFormData(request.json), obj=heating)
    if form.validate():
        form.populate_obj(heating)
        HeatingReport.update(heating)

        return make_rest('OK', heating, 200)

    return make_rest('ERROR', form.errors, 400)

@mod.route('/heatings/<id>/', methods=['DELETE'])
@login_required
def delete_heating(id):
    """
    @api {delete} reports/heatings/:id Deleting a heating report
    @apiName DeleteSingleHeatingReport
    @apiGroup Heating Reports
    @apiVersion 1.0.0

    @apiParam {Number} id Heating Report unique ID.

    @apiSuccessExample {json} Success-Response:
    HTTP/1.1 200
    {
      "data": "Deleted id=2 heating report",
      "status": "OK"
    }

    @apiErrorExample {json} Error-Response:
    HTTP/1.1 204
    {
      "data": "There is no heating report with this id",
      "status": "ERROR"
    }
    """
    heating = HeatingReport.get(id=int(id), args=request.args)
    if heating:
        HeatingReport.remove(heating)
        return make_rest('OK', 'Deleted id={0} heating report'.format(id), 200)
        
    return make_rest('ERROR', 'There is no heating report with this id', 204)


@mod.route('/chillings/', methods=['POST'])
@login_required
def create_chilling():
    """
    @api {post} reports/chillings/ Creating a chilling report
    @apiName CreateChillingReport
    @apiGroup Chilling Reports
    @apiVersion 1.0.0

    @apiParam {String} description Chilling Report description.
    @apiParam {String} food_name Chilling Report food name.
    @apiParam {String} method Chilling Report method.
    @apiParam {Number} object_id Chilling Report object ID.
    @apiParam {String} object_type Chilling Report object type.
    @apiParam {String} temp_now Chilling Report current temperature.
    @apiParam {String} temp_to Chilling Report desired temperature.
    @apiParam {String} type Chilling Reprt type.

    @apiSuccessExample {json} Success-Response:
    HTTP/1.1 200
    {
        "data":{
            "description": "text",
            "food_name": "t3",
            "id": 1,
            "method": "t4",
            "object_id": 1,
            "object_type": "restaurant",
            "temp_now": "t1",
            "temp_to": "t2",
            "type": "chilling"
        },
        "status": "OK"
    }
    """
    form = ChillingReportForm(data=request.json)
    if form.validate():
        chilling = ChillingReport()
        form.populate_obj(chilling)
        ChillingReport.add(chilling)

        return make_rest('OK', chilling, 200)

    return make_rest('ERROR', form.errors, 400)

@mod.route('/chillings/', methods=['GET'])
@login_required
def get_chillings():
    """
    @api {get} reports/chillings/ Getting all chilling reports
    @apiName GetChillingReports
    @apiGroup Chilling Reports
    @apiVersion 1.0.0

    @apiSuccessExample {json} Success-Response:
    HTTP/1.1 200
    {
        "data": [
            {
                "description": "text",
                "food_name": "t3",
                "id": 1,
                "method": "t4",
                "object_id": 1,
                "object_type": "restaurant",
                "temp_now": "t1",
                "temp_to": "t2",
                "type": "chilling"
            },
            {
                "description": "text",
                "food_name": "t3",
                "id": 2,
                "method": "t4",
                "object_id": 1,
                "object_type": "restaurant",
                "temp_now": "t1",
                "temp_to": "t2",
                "type": "chilling"
        }
        ],
        "status": "OK"
    }
    """
    chillings = ChillingReport.get(args=request.args)
    return make_rest('OK', chillings, 200, include=request.args.get('include', None))

@mod.route('/chillings/<id>/', methods=['GET'])
@login_required
def get_chilling(id):
    """
    @api {get} reports/chillings:id Getting a chilling report
    @apiName GetSingleChillingReport
    @apiGroup Chilling Reports
    @apiVersion 1.0.0

    @apiParam {Number} id Chilling Report unique ID.

    @apiSuccessExample {json} Success-Response:
    HTTP/1.1 200
    {
      "data": {
          "description": "text",
          "food_name": "t3",
          "id": 1,
          "method": "t4",
          "object_id": 1,
          "object_type": "restaurant",
          "temp_now": "t1",
          "temp_to": "t2",
          "type": "chilling"
      },
      "status": "OK"
    }

    @apiErrorExample {json} Error-Response:
    HTTP/1.1 204
    {
      "data": "There is no chilling report with this id",
      "status": "ERROR"
    }
    """
    chilling = ChillingReport.get(id=int(id), args=request.args)
    if not chilling:
        return make_rest('ERROR', "There is no chilling report with this id", 204)

    return make_rest('OK', chilling, 200, include=request.args.get('include', None))
    
@mod.route('/chillings/<id>/', methods=['PUT'])
@login_required
def update_chilling(id):
    """
    @api {put} reports/chillings/:id Updating a chilling report
    @apiName UpdateSingleChillingReport
    @apiGroup Chilling Reports
    @apiVersion 1.0.0

    @apiParam {Number} id Chilling Report unique ID.
    @apiParam {String} description Chilling Report description.
    @apiParam {String} food_name Chilling Report food name.
    @apiParam {String} method Chilling Report method.
    @apiParam {Number} object_id Chilling Report object ID.
    @apiParam {String} object_type Chilling Report object type.
    @apiParam {String} temp_now Chilling Report current temperature.
    @apiParam {String} temp_to Chilling Report desired temperature.
    @apiParam {String} type Chilling Reprt type.

    @apiSuccessExample {json} Success-Response:
    HTTP/1.1 200
    {
      "data": {
          "description": "text",
          "food_name": "t3",
          "id": 1,
          "method": "t4",
          "object_id": 1,
          "object_type": "restaurant",
          "temp_now": "t1",
          "temp_to": "t2",
          "type": "chilling"
      },
      "status": "OK"
    }

    @apiErrorExample {json} Error-Response:
    HTTP/1.1 204
    {
      "data": "There is no chilling report with this id",
      "status": "ERROR"
    }
    """
    if not chilling:
        return make_rest('ERROR', 'There is no chilling report with this id', 204)

    form = ChillingReportForm(WtfFormData(request.json), obj=chilling)
    if form.validate():
        form.populate_obj(chilling)
        ChillingReport.update(chilling)

        return make_rest('OK', chilling, 200)

    return make_rest('ERROR', form.errors, 400)

@mod.route('/chillings/<id>/', methods=['DELETE'])
@login_required
def delete_chilling(id):
    """
    @api {delete} reports/chillings/:id Deleting a chilling report
    @apiName DeleteSingleChillingReport
    @apiGroup Chilling Reports
    @apiVersion 1.0.0

    @apiParam {Number} id Chilling Report unique ID.

    @apiSuccessExample {json} Success-Response:
    HTTP/1.1 200
    {
      "data": "Deleted id=2 chilling report",
      "status": "OK"
    }

    @apiErrorExample {json} Error-Response:
    HTTP/1.1 204
    {
      "data": "There is no chilling report with this id",
      "status": "ERROR"
    }
    """
    if chilling:
        ChillingReport.remove(chilling)
        return make_rest('OK', 'Deleted id={0} chilling report'.format(id), 200)
        
    return make_rest('ERROR', 'There is not any chilling report with this id', 204)


@mod.route('/defrostings/', methods=['POST'])
@login_required
def create_defrosting():
    """
    @api {post} reports/defrostings Creating a defrosting report
    @apiName CreateDefrostingReport
    @apiGroup Defrosting Reports
    @apiVersion 1.0.0

    @apiParam {String} description Defrosting Report description.
    @apiParam {String} food_name Defrosting Report food name.
    @apiParam {String} method Defrosting method.
    @apiParam {Number} object_id Defrosting Report object ID.
    @apiParam {String} object_type Defrosting Report object type.
    @apiParam {String} temp_to Defrosting Report desired temperature.
    @apiParam {String} type Defrosting Report type.

    @apiSuccessExample {json} Success-Response:
    HTTP/1.1 200
    {
        "data": {
            "description": "text",
            "food_name": "t2",
            "id": 14,
            "method": "t3",
            "object_id": 1,
            "object_type": "restaurant",
            "temp_to": "t1",
            "type": "defrosting"
        },
        "status": "OK"
    }
    """
    form = DefrostingReportForm(data=request.json)
    if form.validate():
        defrosting = DefrostingReport()
        form.populate_obj(defrosting)
        DefrostingReport.add(defrosting)

        return make_rest('OK', defrosting, 200)

    return make_rest('ERROR', form.errors, 400)

@mod.route('/defrostings/', methods=['GET'])
@login_required
def get_defrostings():
    """
    @api {get} reports/defrostings/ Getting all defrosting reports
    @apiName GetDefrostingReports
    @apiGroup Defrosting Reports
    @apiVersion 1.0.0

    @apiSuccessExample {json} Success-Response:
    HTTP/1.1 200
    {
        "data": [
            {
                  "description": "text",
                  "food_name": "t2",
                  "id": 14,
                  "method": "t3",
                  "object_id": 1,
                  "object_type": "restaurant",
                  "temp_to": "t1",
                  "type": "defrosting"
              },
                    {
                  "description": "text",
                  "food_name": "t2",
                  "id": 15,
                  "method": "t3",
                  "object_id": 1,
                  "object_type": "restaurant",
                  "temp_to": "t1",
                  "type": "defrosting"
              }
        ],
        "status": "OK"
    }
    """
    defrostings = DefrostingReport.get(args=request.args)
    return make_rest('OK', defrostings, 200, include=request.args.get('include', None))

@mod.route('/defrostings/<id>/', methods=['GET'])
@login_required
def get_defrosting(id):
    """
    @api {get} reports/defrostings/:id Getting a defrosting report
    @apiName GetSingleDefrostingReport
    @apiGroup Defrosting Reports
    @apiVersion 1.0.0

    @apiParam {Number} id Defrosting Report unique ID.

    @apiSuccessExample {json} Success-Response:
    HTTP/1.1 200
    {
      "data": {
          "description": "text",
          "food_name": "t2",
          "id": 14,
          "method": "t3",
          "object_id": 1,
          "object_type": "restaurant",
          "temp_to": "t1",
          "type": "defrosting"
      },
      "status": "OK"
    }

    @apiErrorExample {json} Error-Response:
    HTTP/1.1 204
    {
      "data": "There is no defrosting report with this id",
      "status": "ERROR"
    }
    """
    defrosting = DefrostingReport.get(id=int(id), args=request.args)
    if not defrosting:
        return make_rest('ERROR', "There is no defrosting report with this id", 204)

    return make_rest('OK', defrosting, 200, include=request.args.get('include', None))
    
@mod.route('/defrostings/<id>/', methods=['PUT'])
@login_required
def update_defrosting(id):
    """
    @api {put} reports/defrostings/:id Updating a defrosting report
    @apiName UpdateSingleDefrostingReport
    @apiGroup Defrosting Reports
    @apiVersion 1.0.0

    @apiParam {Number} id Defrosting Report unique ID.
    @apiParam {String} description Defrosting Report description.
    @apiParam {String} food_name Defrosting Report food name.
    @apiParam {String} method Defrosting method.
    @apiParam {Number} object_id Defrosting Report object ID.
    @apiParam {String} object_type Defrosting Report object type.
    @apiParam {String} temp_to Defrosting Report desired temperature.
    @apiParam {String} type Defrosting Report type.
 
    @apiSuccessExample {json} Success-Response:
    HTTP/1.1 200
    {
      "data": {
          "description": "text",
          "food_name": "t2",
          "id": 14,
          "method": "t3",
          "object_id": 1,
          "object_type": "restaurant",
          "temp_to": "t1",
          "type": "defrosting"
      },
      "status": "OK"
    }

    @apiErrorExample {json} Error-Response:
    HTTP/1.1 204
    {
      "data": "There is no defrosting report with this id",
      "status": "ERROR"
    }
    """
    defrosting = DefrostingReport.get(id=int(id), args=request.args)
    if not defrosting:
        return make_rest('ERROR', 'There is no defrosting report with this id', 204)

    form = DefrostingReportForm(WtfFormData(request.json), obj=defrosting)
    if form.validate():
        form.populate_obj(defrosting)
        DefrostingReport.update(defrosting)

        return make_rest('OK', defrosting, 200)

    return make_rest('ERROR', form.errors, 400)

@mod.route('/defrostings/<id>/', methods=['DELETE'])
@login_required
def delete_defrosting(id):
    """
    @api {delete} reports/defrostings/:id Deleting a defrosting report
    @apiName DeleteSingleDefrostingReport
    @apiGroup Defrosting Reports
    @apiVersion 1.0.0

    @apiParam {Number} id Defrosting Report unique ID.

    @apiSuccessExample {json} Success-Response:
    HTTP/1.1 200
    {
      "data": "Deleted id=2 defrosting report",
      "status": "OK"
    }

    @apiErrorExample {json} Error-Response:
    HTTP/1.1 204
    {
      "data": "There is no defrosting report with this id",
      "status": "ERROR"
    }
    """
    defrosting = DefrostingReport.get(id=int(id), args=request.args)
    if defrosting:
        DefrostingReport.remove(defrosting)
        return make_rest('OK', 'Deleted id={0} defrosting report'.format(id), 200)

    return make_rest('ERROR', 'There is not any defrosting report with this id', 204)

@mod.route('/cleanings/', methods=['POST'])
@login_required
def create_cleaning():
    """
    @api {post} reports/cleanings/ Creating a cleaning report
    @apiName CreateCleaningReport
    @apiGroup Cleaning Reports
    @apiVersion 1.0.0

    @apiParam {String} description Cleaning Report description.
    @apiParam {String} field Cleaning Report field.
    @apiParam {Number} object_id Cleaning Report object ID.
    @apiParam {String} object_type Cleaning Report object type.
    @apiParam {String} periodic_time Cleaning Report schedule time.
    @apiParam {String} type Cleaning Report type.

    @apiSuccessExample {json} Success-Response:
    HTTP/1.1 200
    {
        "data": {
              "description": "text",
              "field": "t1",
              "id": 3,
              "object_id": 1,
              "object_type": "restaurant",
              "periodic_time": "t2",
              "type": "cleaning"
        },
        "status": "OK"
    }
    """
    form = CleaningReportForm(data=request.json)
    if form.validate():
        cleaning = CleaningReport()
        form.populate_obj(cleaning)
        CleaningReport.add(cleaning)

        return make_rest('OK', cleaning, 200)

    return make_rest('ERROR', form.errors, 400)

@mod.route('/cleanings/', methods=['GET'])
@login_required
def get_cleanings():
    """
    @api {get} reports/cleanings/ Getting all cleaning reports
    @apiName GetCleaningReports
    @apiGroup Cleaning Reports
    @apiVersion 1.0.0

    @apiSuccessExample {json} Success-Response:
    HTTP/1.1 200
    {
        "data": [
            {
              "description": "text",
              "field": "t1",
              "id": 3,
              "object_id": 1,
              "object_type": "restaurant",
              "periodic_time": "t2",
              "type": "cleaning"
            },
            {
              "description": "text",
              "field": "t1",
              "id": 4,
              "object_id": 1,
              "object_type": "restaurant",
              "periodic_time": "t2",
              "type": "cleaning"
            }
        ],
        "status": "OK"
    }
    """
    cleanings = CleaningReport.get(args=request.args)
    return make_rest('OK', cleanings, 200, include=request.args.get('include', None))

@mod.route('/cleanings/<id>/', methods=['GET'])
@login_required
def get_cleaning(id):
    """
    @api {get} reports/cleaning/:id Getting a cleaning report
    @apiName GetSingleCleaningReport
    @apiGroup Cleaning Reports
    @apiVersion 1.0.0

    @apiParam {Number} id Cleaning Report unique ID.

    @apiSuccessExample {json} Success-Response:
    HTTP/1.1 200
    {
      "data": {
          "description": "text",
          "field": "t1",
          "id": 3,
          "object_id": 1,
          "object_type": "restaurant",
          "periodic_time": "t2",
          "type": "cleaning"
      },
      "status": "OK"
    }

    @apiErrorExample {json} Error-Response:
    HTTP/1.1 204
    {
      "data": "There is no cleaning report with this id",
      "status": "ERROR"
    }
    """
    cleaning = CleaningReport.get(id=int(id), args=request.args)
    if not cleaning:
        return make_rest('ERROR', 'There is no cleaning report with this id', 204)

    return make_rest('OK', cleaning, 200, include=request.args.get('include', None))
    
@mod.route('/cleanings/<id>/', methods=['PUT'])
@login_required
def update_cleaning(id):
    """
    @api {put} foods/:id Updating a cleaning report
    @apiName UpdateSingleCleaningReport
    @apiGroup Cleaning Reports
    @apiVersion 1.0.0

    @apiParam {Number} id Cleaning Report unique ID.
    @apiParam {String} description Cleaning Report description.
    @apiParam {String} field Cleaning Report field.
    @apiParam {Number} object_id Cleaning Report object ID.
    @apiParam {String} object_type Cleaning Report object type.
    @apiParam {String} periodic_time Cleaning Report schedule time.
    @apiParam {String} type Cleaning Report type.


    @apiSuccessExample {json} Success-Response:
    HTTP/1.1 200
    {
      "data": {
          "description": "text",
          "field": "t1",
          "id": 3,
          "object_id": 1,
          "object_type": "restaurant",
          "periodic_time": "t2",
          "type": "cleaning"
      },
      "status": "OK"
    }

    @apiErrorExample {json} Error-Response:
    HTTP/1.1 204
    {
      "data": "There is no cleaning report with this id",
      "status": "ERROR"
    }
    """
    if not cleaning:
        return make_rest('ERROR', 'There is no cleaning report with this id', 204)

    form = CleaningReportForm(WtfFormData(request.json), obj=cleaning)
    if form.validate():
        form.populate_obj(cleaning)
        CleaningReport.update(cleaning)

        return make_rest('OK', cleaning, 200)

    return make_rest('ERROR', form.errors, 400)

@mod.route('/cleanings/<id>/', methods=['DELETE'])
@login_required
def delete_cleaning(id):
    """
    @api {delete} reports/cleanings/:id Deleting a cleaning report
    @apiName DeleteSingleCleaningReport
    @apiGroup Cleaning Reports
    @apiVersion 1.0.0

    @apiParam {Number} id Cleaning Report unique ID.

    @apiSuccessExample {json} Success-Response:
    HTTP/1.1 200
    {
      "data": "Deleted id=2 cleaning report",
      "status": "OK"
    }

    @apiErrorExample {json} Error-Response:
    HTTP/1.1 204
    {
      "data": "There is no cleaning report with this id",
      "status": "ERROR"
    }
    """
    if cleaning:
        CleaningReport.remove(cleaning)
        return make_rest('OK', 'Deleted id={0} cleaning report'.format(id), 200)
    
    return make_rest('ERROR', 'There is not any cleaning report with this id', 204)
