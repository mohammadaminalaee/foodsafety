from wtforms_alchemy import ModelForm
from .models import TemperatureReport, StaffReport, SupplierReport, \
    DailyReport, HeatingReport, ChillingReport, DefrostingReport, CleaningReport

from wtforms import IntegerField, BooleanField, StringField


class TemperatureReportForm(ModelForm):
    class Meta:
        model = TemperatureReport
        exclude = ['data']

    temperature = IntegerField()


class StaffReportForm(ModelForm):
    class Meta:
        model = StaffReport
        exclude = ['data']

    health = BooleanField()
    appearance = BooleanField()


class SupplierReportForm(ModelForm):
    class Meta:
        model = SupplierReport
        exclude = ['data']

    q1 = BooleanField()
    q2 = BooleanField()
    q3 = BooleanField()
    q4 = BooleanField()


class DailyReportForm(ModelForm):
    class Meta:
        model = DailyReport
        exclude = ['data']

    leftout = BooleanField()
    leftout_desc = StringField()
    expired = BooleanField()
    expired_desc = StringField()
    dirtyclothes = BooleanField()
    dirtyclothes_desc = StringField()
    waste = BooleanField()
    waste_desc = StringField()
    floor = BooleanField()
    floor_desc = StringField()
    board = BooleanField()
    board_desc = StringField()
    utensile = BooleanField()
    utensile_desc = StringField()
    equipment = BooleanField()
    equipment_desc = StringField()
    storage = BooleanField()
    storage_desc = StringField()
    food_prep = BooleanField()
    food_prep_desc = StringField()
    matt_ok = BooleanField()
    matt_ok_desc = StringField()


class HeatingReportForm(ModelForm):
    class Meta:
        model = HeatingReport
        exclude = ['data']

    temp_now = StringField()
    temp_to = StringField()
    food_name = StringField()


class ChillingReportForm(ModelForm):
    class Meta:
        model = ChillingReport
        exclude = ['data']

    temp_now = StringField()
    temp_to = StringField()
    food_name = StringField()
    method = StringField()


class DefrostingReportForm(ModelForm):
    class Meta:
        model = DefrostingReport
        exclude = ['data']

    temp_to = StringField()
    food_name = StringField()
    method = StringField()


class CleaningReportForm(ModelForm):
    class Meta:
        model = CleaningReport
        exclude = ['data']

    field = StringField()
    periodic_time = StringField()