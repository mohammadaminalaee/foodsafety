from flask import Blueprint
mod = Blueprint('report', __name__, url_prefix='/api/v1/reports')
from .views import *
