import os
from flask import Flask, render_template, url_for, current_app, send_from_directory, jsonify
from .base.db import db


def create_app(config):
    app = Flask(__name__)
    app.config.from_object(config)

    config_db(app)
    define_modules(app)
    config_login(app)
    config_error_pages(app)
    config_favicon(app)

    return app


def config_db(app):
    db.init_app(app)


def config_login(app):
    from .base.login_manager import login_manager
    login_manager.init_app(app)

    # login_manager.login_view = "auth.login"
    login_manager.login_message = "Logged in successfully"


def define_modules(app):
    for mod in app.config['MODULES']:
        module = __import__('api.modules.{mod}'.format(mod=mod[1]), fromlist='mod')
        app.register_blueprint(module.mod)
        print '&'*20, 'Registered Module ===>', mod[1], ' ,=>Description:', mod[0]

    print '&'*80


def config_error_pages(app):
    @app.errorhandler(404)
    def page_not_found(e):
        return render_template('errors/404.html'), 404

    @app.errorhandler(401)
    def custom_401(error):
        return jsonify({'data': 'Login is required', 'status': 'ERROR'})


def config_favicon(app):
    @app.route('/favicon.ico')
    def favicon():
        return send_from_directory(os.path.join(app.root_path, 'static'),
                                   'favicon.ico', mimetype='image/vnd.microsoft.icon')


def csrf_token(app):
    pass
